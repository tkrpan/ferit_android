package com.example.lv3;

import android.app.Activity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity implements EditFragment.TextChangeListener
{
    private static final String LOG_TAG = "LV3";

    private Button bSwitchFragments;
    public boolean mAreSwitched = false;
    private final String EDIT_FRAGMENT = "EDIT_FRAGMENT";
    private final String DISPLAY_FRAGMENT = "DISPLAY_FRAGMENT";
    private final String M_ARE_SWITCHED = "M_ARE_SWITCHED";
    private final String DF_TEXT_SIZE = "DF_TEXT_SIZE";
    private final String DF_TEXT_MESSAGE = "DF_TEXT_MESSAGE";
    private final String DF_TEXT_COLOR = "DF_TEXT_COLOR";
    private EditFragment editFragment;
    private DisplayFragment displayFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(LOG_TAG,"MainActivity onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getResources().getBoolean(R.bool.portraitMode))
        {
            Log.d(LOG_TAG,"portraitMode = true");
        }
        else
        {
            Log.d(LOG_TAG,"portraitMode = false");
        }
        setUpFragments(savedInstanceState);
        setUpUI();
    }

    private void setUpFragments(Bundle savedInstanceState)
    {
        Log.d(LOG_TAG,"MainActivity setUpFragments()");
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        if (savedInstanceState == null)
        {
            Log.d(LOG_TAG,"savedInstanceState = false");
            editFragment = new EditFragment();
            displayFragment = new DisplayFragment();
        }
        else
        {
            Log.d(LOG_TAG,"savedInstanceState = true");
            editFragment = (EditFragment) getFragmentManager().getFragment(savedInstanceState,EDIT_FRAGMENT);
            displayFragment = (DisplayFragment) getFragmentManager().getFragment(savedInstanceState,DISPLAY_FRAGMENT);
            mAreSwitched = savedInstanceState.getBoolean(M_ARE_SWITCHED);

            Log.d(LOG_TAG,"savedInstanceState.getInt(DF_TEXT_SIZE) = " + savedInstanceState.getInt(DF_TEXT_SIZE));
            Log.d(LOG_TAG,"savedInstanceState.getString(DF_TEXT_MESSAGE) = " + savedInstanceState.getString(DF_TEXT_MESSAGE));
            Log.d(LOG_TAG,"savedInstanceState.getInt(DF_TEXT_COLOR) = " + savedInstanceState.getInt(DF_TEXT_COLOR));

            displayFragment.currentTextSize = savedInstanceState.getInt(DF_TEXT_SIZE);
            displayFragment.currentTextMessage = savedInstanceState.getString(DF_TEXT_MESSAGE);
            displayFragment.currentTextColor = savedInstanceState.getInt(DF_TEXT_COLOR);
            /*
            displayFragment.setText(savedInstanceState.getInt(DF_TEXT_SIZE),
                    savedInstanceState.getString(DF_TEXT_MESSAGE),
                    savedInstanceState.getInt(DF_TEXT_COLOR));

            */
        }

        if(!mAreSwitched)
        {
            fragmentTransaction.replace(R.id.flPrimary,editFragment);
            fragmentTransaction.replace(R.id.flSecondary,displayFragment);
        }
        else
        {
            fragmentTransaction.replace(R.id.flPrimary,displayFragment);
            fragmentTransaction.replace(R.id.flSecondary,editFragment);
        }

        fragmentTransaction.commit();
    }

    private void setUpUI()
    {
        Log.d(LOG_TAG,"MainActivity setUpUI()");
        bSwitchFragments = findViewById(R.id.bSwitchFragments);
        bSwitchFragments.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d(LOG_TAG,"MainActivity onClick()");

                int dfTextSize = displayFragment.currentTextSize;
                String dfTextMessage = displayFragment.currentTextMessage;
                int dfTextColor = displayFragment.currentTextColor;

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.remove(editFragment);
                fragmentTransaction.remove(displayFragment);

                Fragment buf = recreateFragment(editFragment);
                editFragment = (EditFragment) buf;
                buf = recreateFragment(displayFragment);
                displayFragment = (DisplayFragment) buf;

                if(!mAreSwitched)
                {
                    fragmentTransaction.replace(R.id.flPrimary,displayFragment);
                    fragmentTransaction.replace(R.id.flSecondary,editFragment);
                }
                else
                {
                    fragmentTransaction.replace(R.id.flPrimary,editFragment);
                    fragmentTransaction.replace(R.id.flSecondary,displayFragment);
                }
                displayFragment.currentTextSize = dfTextSize;
                displayFragment.currentTextMessage = dfTextMessage;
                displayFragment.currentTextColor = dfTextColor;

                fragmentTransaction.commit();
                mAreSwitched = !mAreSwitched;
            }
        });
    }

    private Fragment recreateFragment(Fragment f)
    {
        try
        {
            Fragment.SavedState savedState = getFragmentManager().saveFragmentInstanceState(f);

            Fragment newInstance = f.getClass().newInstance();
            newInstance.setInitialSavedState(savedState);

            return newInstance;
        }
        catch (Exception e) //RecreationException, IllegalAccessException
        {
            throw new RuntimeException("Cannot recreate fragment " + f.getClass().getName(), e);
        }
    }

    @Override
    public void onTextChange(final int TextSize, final String TextMessage, final int TextColor)
    {
        Log.d(LOG_TAG,"MainActivity onTextChange()");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to change the text?").setTitle("Alert");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                // User clicked Yes button
                displayFragment.setText(TextSize,TextMessage,TextColor);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                // User cancelled the dialog
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        Log.d(LOG_TAG,"MainActivity onSaveInstanceState()");
        getFragmentManager().putFragment(outState, EDIT_FRAGMENT, editFragment);
        getFragmentManager().putFragment(outState, DISPLAY_FRAGMENT, displayFragment);
        outState.putBoolean(M_ARE_SWITCHED,mAreSwitched);

        outState.putInt(DF_TEXT_SIZE,displayFragment.currentTextSize);
        outState.putString(DF_TEXT_MESSAGE,displayFragment.currentTextMessage);
        outState.putInt(DF_TEXT_COLOR,displayFragment.currentTextColor);
    }

}
