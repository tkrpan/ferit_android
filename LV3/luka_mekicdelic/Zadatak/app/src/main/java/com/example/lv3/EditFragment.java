package com.example.lv3;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;

import android.app.Fragment; //Important - Alt + Enter imports androidx.fragment.app.Fragment instead which breaks things in MainActivity.java
import android.widget.Spinner;

public class EditFragment extends Fragment
{
    private int mTextSize = 10;
    private String mTextMessage = "...";
    private int mTextColor = Color.rgb(0,0,0);
    private EditText etTextMessage;
    private SeekBar sbTextSize;
    private Spinner sColor;
    private Button bChangeText;
    private TextChangeListener mTextChangeListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View Layout = inflater.inflate(R.layout.fragment_edit,null);
        setUpUI(Layout);
        return Layout;
    }

    private void setUpUI(View layout)
    {
        etTextMessage = layout.findViewById(R.id.etMessage);
        sbTextSize = layout.findViewById(R.id.sbTextSize);
        bChangeText = layout.findViewById(R.id.btnChangeText);
        sColor = layout.findViewById(R.id.sColor);

        sbTextSize.setProgress(mTextSize);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.array_color, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sColor.setAdapter(adapter);

        bChangeText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mTextMessage = etTextMessage.getText().toString();
                mTextSize = sbTextSize.getProgress();
                switch(sColor.getSelectedItem().toString())
                {
                    case "Black":
                        break; //Remains black as it was defined
                    case "Red":
                        mTextColor=Color.rgb(255,0,0);
                        break;
                    case "Green":
                        mTextColor=Color.rgb(0,255,0);
                        break;
                    case "Blue":
                        mTextColor=Color.rgb(0,0,255);
                        break;
                    case "Cyan":
                        mTextColor=Color.rgb(0,255,255);
                        break;
                    case "Magenta":
                        mTextColor=Color.rgb(255,0,255);
                        break;
                    case "Yellow":
                        mTextColor=Color.rgb(255,255,0);
                        break;
                }
                mTextChangeListener.onTextChange(mTextSize,mTextMessage,mTextColor);
            }
        });
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if(context instanceof TextChangeListener)
        {
            this.mTextChangeListener = (TextChangeListener) context;
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        this.mTextChangeListener = null;
    }

    public interface TextChangeListener
    {
        void onTextChange(int TextSize, String TextMessage, int TextColor);
    }
}
