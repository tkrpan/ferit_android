package com.example.lv3;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import android.app.Fragment; //Important - Alt + Enter imports androidx.fragment.app.Fragment instead which breaks things in MainActivity.java

public class DisplayFragment extends Fragment
{
    private static final String LOG_TAG = "LV3";

    TextView tvMessage;

    int currentTextSize = 0;
    String currentTextMessage = "";
    int currentTextColor = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View Layout = inflater.inflate(R.layout.fragment_display,null);
        tvMessage = Layout.findViewById(R.id.tvMessage);
        if(currentTextSize==0)
            currentTextSize = 20;
        if(currentTextMessage=="")
            currentTextMessage = getResources().getString(R.string.fragmentDisplay_tvMessage);
        if(currentTextColor==0)
            currentTextColor = Color.BLACK;
        //Log.d(LOG_TAG,"onCreateView() currentTextMessage = " + currentTextMessage);
        return Layout;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    public void setText(int TextSize, String TextMessage, int TextColor)
    {
        currentTextSize = TextSize;
        currentTextMessage = TextMessage;
        currentTextColor = TextColor;

        Log.d(LOG_TAG,"setText() currentTextMessage = " + currentTextMessage);

        tvMessage.setTextSize(TextSize);
        tvMessage.setText(TextMessage);
        tvMessage.setTextColor(TextColor);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d(LOG_TAG,"onViewCreated() currentTextMessage = " + currentTextMessage);

        tvMessage.setTextSize(currentTextSize);
        tvMessage.setText(currentTextMessage);
        tvMessage.setTextColor(currentTextColor);
    }
}