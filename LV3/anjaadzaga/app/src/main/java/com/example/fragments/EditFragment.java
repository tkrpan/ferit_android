package com.example.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import androidx.fragment.app.FragmentTransaction;

import static android.widget.ArrayAdapter.createFromResource;

public class EditFragment extends Fragment implements AdapterView.OnItemSelectedListener{
    private int mTextSize = 10;
    private String mTextMessage = "...";
    private EditText etTextMessage;
    private SeekBar sbTextSize;
    private Spinner spinnerColors;
    private TextColorChangeListener mTextColorChangeListener;
    private TextChangeListener mTextChangeListener;
    private String textColor = "";

    ArrayAdapter<CharSequence> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View Layout = inflater.inflate(R.layout.fragment_edit,null);
        setUpUI(Layout);
        return Layout;
    }
    private void setUpUI(View layout) {
        this.etTextMessage = (EditText) layout.findViewById(R.id.etMessage);
        this.sbTextSize = (SeekBar) layout.findViewById(R.id.sbTextSize);
        this.spinnerColors = (Spinner) layout.findViewById(R.id.spinnerColors);
        Button bChangeText = (Button) layout.findViewById(R.id.btnChangeText);
        Button bChangeColor = (Button) layout.findViewById(R.id.btnChangeTextColor);
        this.sbTextSize.setProgress(this.mTextSize);

        adapter = ArrayAdapter.createFromResource(this.getContext(), getResources().getIdentifier("spinnerColors", "array", "com.example.fragments"), android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerColors.setAdapter(adapter);
        spinnerColors.setOnItemSelectedListener(this);

        bChangeColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle("Alert Dialog");
                builder.setMessage("Click Yes to continue or No to cancel");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        mTextMessage = etTextMessage.getText().toString();
                        mTextSize = sbTextSize.getProgress();
                        textColor = spinnerColors.getSelectedItem().toString();

                        mTextColorChangeListener.onTextColorChange(textColor);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                    }
                });

                AlertDialog alertdialog = builder.create();
                alertdialog.show();
            }

        });

        bChangeText.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTextMessage = etTextMessage.getText().toString();
                        mTextSize = sbTextSize.getProgress();
                        mTextChangeListener.onTextChange(mTextSize,mTextMessage);
                    }
                }
        );
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof TextChangeListener)
        {
            this.mTextChangeListener = (TextChangeListener) context;
            this.mTextColorChangeListener = (TextColorChangeListener) context;
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        this.mTextChangeListener = null;
        this.mTextColorChangeListener = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public interface TextChangeListener
    {
        void onTextChange(int TextSize, String TextMessage);
    }

    public interface TextColorChangeListener
    {
        void onTextColorChange(String color);
    }






}
