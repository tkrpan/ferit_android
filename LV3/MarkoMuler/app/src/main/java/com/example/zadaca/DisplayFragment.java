package com.example.zadaca;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;




public class DisplayFragment extends android.app.Fragment {
    TextView tvMessage;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View Layout = inflater.inflate(R.layout.fragment_display,null);
        this.tvMessage = (TextView) Layout.findViewById(R.id.tvMessage);
        return Layout;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }
    public void setText(int TextSize, String TextMessage, String TextColor)
    {
        this.tvMessage.setText(TextMessage);
        this.tvMessage.setTextSize(TextSize);
        if (TextColor == "colorPrimary") this.tvMessage.setTextColor(this.getResources().getColor(R.color.colorPrimary));
        else if (TextColor == "colorPrimaryDark") this.tvMessage.setTextColor(this.getResources().getColor(R.color.colorPrimaryDark));
        else this.tvMessage.setTextColor(this.getResources().getColor(R.color.colorAccent));

    }
}



