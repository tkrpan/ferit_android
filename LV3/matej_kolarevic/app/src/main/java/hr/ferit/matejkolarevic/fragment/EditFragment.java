package hr.ferit.matejkolarevic.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;

public class EditFragment extends Fragment {
    private int mTextSize = 10;
    private String mTextMessage = "...";

    private EditText etTextMessage;
    private SeekBar sbTextSize;
    private Button btnChangeText;

    private TextChangeListener mTextChangeListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View Layout = inflater.inflate(R.layout.fragment_edit,null);
        setUpUI(Layout);
        return Layout;
    }

    private void setUpUI(View layout){
        this.etTextMessage = layout.findViewById(R.id.etMessage);
        this.sbTextSize = layout.findViewById(R.id.sbTextSize);
        this.btnChangeText = layout.findViewById(R.id.btnChangeText);
        this.sbTextSize.setProgress(this.mTextSize);
        this.btnChangeText.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        mTextMessage = etTextMessage.getText().toString();
                        mTextSize = sbTextSize.getProgress();
                        mTextChangeListener.onTextChange(mTextSize,mTextMessage);
                    }
                }
        );
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if(context instanceof TextChangeListener){
            this.mTextChangeListener = (TextChangeListener) context;
        }
    }

    @Override
    public void onDetach(){
        super.onDetach();
        this.mTextChangeListener = null;
    }

    public interface TextChangeListener{
        void onTextChange(int TextSize, String TextMessage);
    }
}
