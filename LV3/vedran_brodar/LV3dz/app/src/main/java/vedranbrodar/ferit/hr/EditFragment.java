package vedranbrodar.ferit.hr;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;



public class EditFragment extends android.app.Fragment {
    private int mTextSize = 10;
    private String mTextMessage = "...";
    private EditText etTextMessage;
    private SeekBar sbTextSize;
    private Button bChangeText;
    private TextChangeListener mTextChangeListener;
    private Spinner spinner;
    private String text;
    String [] values = {"White","Black", "Red"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View Layout = inflater.inflate(R.layout.fragment_edit,null);

        setUpUI(Layout);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, values);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(adapter);

        String text = spinner.getSelectedItem().toString();


        return Layout;

    }
    private void setUpUI(View layout) {
        this.spinner = layout.findViewById(R.id.spinner);
        this.etTextMessage = (EditText) layout.findViewById(R.id.etMessage);
        this.sbTextSize = (SeekBar) layout.findViewById(R.id.sbTextSize);
        this.bChangeText = (Button) layout.findViewById(R.id.btnChangeText);
        this.sbTextSize.setProgress(this.mTextSize);
        this.bChangeText.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        builder.setTitle("Alert Dialog");
                        builder.setMessage("Click Yes to proceed or No to cancel");
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                mTextMessage = etTextMessage.getText().toString();
                                mTextSize = sbTextSize.getProgress();
                                text = spinner.getSelectedItem().toString();
                                mTextChangeListener.onTextChange(mTextSize,mTextMessage, text);
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                            }
                        });

                        AlertDialog alertdialog = builder.create();
                        alertdialog.show();

                    }
                }
        );
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof TextChangeListener)
        {
            this.mTextChangeListener = (TextChangeListener) context;
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        this.mTextChangeListener = null;
    }
    public interface TextChangeListener
    {
        void onTextChange(int TextSize, String TextMessage, String TextColor);
    }
}


