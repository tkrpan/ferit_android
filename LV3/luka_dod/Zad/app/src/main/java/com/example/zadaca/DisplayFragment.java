package com.example.zadaca;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DisplayFragment extends android.app.Fragment{
    TextView screenMessage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View Layout = inflater.inflate(R.layout.activity_display_fragment,null);
        this.screenMessage = (TextView) Layout.findViewById(R.id.screenMessage);
        return Layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setText(int TextSize, String TextMessage, String TextColor) {
        this.screenMessage.setText(TextMessage);
        this.screenMessage.setTextSize(TextSize);
        if (TextColor == "Red") {
            this.screenMessage.setTextColor(this.getResources().getColor(R.color.red));
        }
        if (TextColor == "Yellow") {
            this.screenMessage.setTextColor(this.getResources().getColor(R.color.yellow));
        }
        if (TextColor == "Green") {
            this.screenMessage.setTextColor(this.getResources().getColor(R.color.green));
        }

    }
}
