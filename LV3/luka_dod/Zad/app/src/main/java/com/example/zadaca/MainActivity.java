package com.example.zadaca;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends Activity implements EditFragment.TextChangeListener{

    private  static final String LOG_TAG = "mbalat";
    private Button btnSwitchFragments;
    public boolean modeAreSwitched = false;
    private final String EDIT_FRAGMENT = "Edit_Fragment";
    private final String DISPLAY_FRAGMENT = "Display_Fragment";
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setUpFragments();
        this.setUpUI();
    }
    private void setUpUI() {
        this.btnSwitchFragments = (Button) findViewById(R.id.btnSwitchFragments);
        this.btnSwitchFragments.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction =
                                fragmentManager.beginTransaction();
                        if(modeAreSwitched)
                        {
                            fragmentTransaction.replace(R.id.framePrimary,new EditFragment(),EDIT_FRAGMENT);
                            fragmentTransaction.replace(R.id.frameSecondary,new DisplayFragment(),DISPLAY_FRAGMENT);
                        }
                        else
                        {
                            fragmentTransaction.replace(R.id.framePrimary,new DisplayFragment(),DISPLAY_FRAGMENT);
                            fragmentTransaction.replace(R.id.frameSecondary,new EditFragment(),EDIT_FRAGMENT);
                        }
                        fragmentTransaction.commit();
                        modeAreSwitched = !modeAreSwitched;
                    }
                }
        );
    }
    private void setUpFragments() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.framePrimary,new EditFragment(),this.EDIT_FRAGMENT);
        fragmentTransaction.add(R.id.frameSecondary,new DisplayFragment(),this.DISPLAY_FRAGMENT);
        fragmentTransaction.commit();
    }
    @Override
    public void onTextChange(int TextSize, String TextMessage, String TextColor) {
        Log.d(LOG_TAG,"In event handler");
        FragmentManager fragmentManager = getFragmentManager();
        DisplayFragment displayFragment = (DisplayFragment) fragmentManager.findFragmentByTag(this.DISPLAY_FRAGMENT);
        displayFragment.setText(TextSize,TextMessage, TextColor);
    }
}
