package com.example.zadaca;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;

public class EditFragment extends android.app.Fragment{
    private int modeTextSize = 10;
    private String modeTextMessage = "...";
    private EditText etTextMessage;
    private SeekBar barTextSize;
    private Button btnChangeText;
    private TextChangeListener modeTextChangeListener;
    private Spinner spinner;
    private String content;
    String [] values = {"Red","Yellow", "Green"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View Layout = inflater.inflate(R.layout.activity_edit_fragment,null);

        setUpUI(Layout);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, values);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(adapter);

        String content = spinner.getSelectedItem().toString();


        return Layout;

    }
    private void setUpUI(View layout) {
        this.spinner = layout.findViewById(R.id.spinner);
        this.etTextMessage = (EditText) layout.findViewById(R.id.enterMessage);
        this.barTextSize = (SeekBar) layout.findViewById(R.id.barTextSize);
        this.btnChangeText = (Button) layout.findViewById(R.id.btnChangeText);
        this.barTextSize.setProgress(this.modeTextSize);
        this.btnChangeText.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        builder.setTitle("Alert Dialog");
                        builder.setMessage("Click Yes to proceed or No to cancel");
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                modeTextMessage = etTextMessage.getText().toString();
                                modeTextSize = barTextSize.getProgress();
                                content = spinner.getSelectedItem().toString();
                                modeTextChangeListener.onTextChange(modeTextSize,modeTextMessage, content);
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                            }
                        });

                        AlertDialog alertdialog = builder.create();
                        alertdialog.show();

                    }
                }
        );
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof TextChangeListener)
        {
            this.modeTextChangeListener = (TextChangeListener) context;
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        this.modeTextChangeListener = null;
    }
    public interface TextChangeListener
    {
        void onTextChange(int TextSize, String TextMessage, String TextColor);
    }
}
