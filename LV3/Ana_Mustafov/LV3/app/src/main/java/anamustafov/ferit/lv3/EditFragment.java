package anamustafov.ferit.lv3;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;

public class EditFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private int mTextSize = 10;
    private String mTextMessage = "...";
    private EditText etTextMessage;
    private SeekBar sbTextSize;
    private Spinner spColors;
    private TextChangeListener mTextChangeListener;
    private TextColorChangeListener mTextColorChangeListener;
    private String textColor = "";
    ArrayAdapter<CharSequence> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View Layout = inflater.inflate(R.layout.fragment_edit,null);
        setUpUI(Layout);
        return Layout;
    }
    private void setUpUI(View layout) {
        this.etTextMessage  = (EditText) layout.findViewById(R.id.etMessage);
        this.sbTextSize     = (SeekBar) layout.findViewById(R.id.sbTextSize);
        this.spColors       = (Spinner) layout.findViewById(R.id.spColors);
        Button bChangeText  = (Button) layout.findViewById(R.id.btnChangeText);
        Button bChangeColor = (Button) layout.findViewById(R.id.btnChangeTextColor);
        this.sbTextSize.setProgress(this.mTextSize);

        adapter = ArrayAdapter.createFromResource(this.getContext(), getResources().getIdentifier("spColors", "array", "anamustafov.ferit.lv3") , android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spColors.setAdapter(adapter);
        spColors.setOnItemSelectedListener(this);

        bChangeColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textColor = spColors.getSelectedItem().toString();
                mTextColorChangeListener.onTextColorChange(textColor);

            }
        });

        bChangeText.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTextMessage = etTextMessage.getText().toString();
                        mTextSize = sbTextSize.getProgress();
                        mTextChangeListener.onTextChange(mTextSize,mTextMessage);
                    }
                }
        );
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof TextChangeListener)
        {
            this.mTextChangeListener = (TextChangeListener) context;
            this.mTextColorChangeListener = (TextColorChangeListener) context;
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        this.mTextChangeListener = null;
        this.mTextColorChangeListener = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public interface TextChangeListener
    {
        void onTextChange(int TextSize, String TextMessage);
    }

    public interface TextColorChangeListener
    {
        void onTextColorChange(String color);
    }

}