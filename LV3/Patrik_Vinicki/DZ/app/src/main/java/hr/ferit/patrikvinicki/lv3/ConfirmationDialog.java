package hr.ferit.patrikvinicki.lv3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

public class ConfirmationDialog extends DialogFragment {
    private confirmChange confirm;
    private String color;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        color = getArguments().getString("color");
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setTitle("Are you sure?")
                .setMessage("You are about to change the text color, do you still wish to continue?")
                .setPositiveButton(
                        "Do it", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                confirm.confirm(color);
                            }
                        })
                .setNegativeButton(
                        "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getActivity(),"Canceled",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                );
        return dialogBuilder.create();
    }
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof confirmChange)
        {
            this.confirm = (confirmChange) context;
        }
    }

    public interface confirmChange{
        void confirm(String color);
    }
}
