package hr.ferit.patrikvinicki.lv3;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends FragmentActivity implements EditFragment.TextChangeListener, EditFragment.TextColorChangeListener, ConfirmationDialog.confirmChange {
    private static final String LOG_TAG = "Bruno";
    private Button bSwitchFragments;
    public boolean mAreSwitched =false;
    private final String EDIT_FRAGMENT = "Edit_fragment";
    private final String DISPLAY_FRAGMENT = "Display_fragment";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setUpFragments();
        this.setUpUI();
    }
    private void setUpUI() {
        this.bSwitchFragments = (Button) findViewById(R.id.bSwitchFragments);
        this.bSwitchFragments.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        if(mAreSwitched)
                        {
                            fragmentTransaction.replace(R.id.flPrimary,new EditFragment(),EDIT_FRAGMENT);
                            fragmentTransaction.replace(R.id.flSecondary,new DisplayFragment(),DISPLAY_FRAGMENT);
                        }
                        else
                        {
                            fragmentTransaction.replace(R.id.flPrimary,new DisplayFragment(),DISPLAY_FRAGMENT);
                            fragmentTransaction.replace(R.id.flSecondary,new EditFragment(),EDIT_FRAGMENT);
                        }
                        fragmentTransaction.commit();
                        mAreSwitched = !mAreSwitched;
                    }
                }
        );
    }
    private void setUpFragments() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.flPrimary,new EditFragment(),this.EDIT_FRAGMENT);
        fragmentTransaction.add(R.id.flSecondary,new DisplayFragment(),this.DISPLAY_FRAGMENT);
        fragmentTransaction.commit();
    }
    @Override
    public void onTextChange(int TextSize, String TextMessage) {
        Log.d(LOG_TAG,"In event handler");
        FragmentManager fragmentManager = getSupportFragmentManager();
        DisplayFragment displayFragment = (DisplayFragment) fragmentManager.findFragmentByTag(this.DISPLAY_FRAGMENT);
        displayFragment.setText(TextSize,TextMessage);
    }
    @Override
    public void onTextColorChange(String color){
        ConfirmationDialog dialog = new ConfirmationDialog();
        Bundle bundle = new Bundle();
        bundle.putString("color", color);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "ConfirmationDialog");
    }

    @Override
    public void confirm(String color){
        FragmentManager fragmentManager = getSupportFragmentManager();
        DisplayFragment displayFragment = (DisplayFragment) fragmentManager.findFragmentByTag(this.DISPLAY_FRAGMENT);
        displayFragment.changeColor(color);
    }
}

