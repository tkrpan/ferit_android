package matejkristic.ferit.hr;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends Activity implements EditFrag.TextChangeListener {

    private static final String LOG_TAG = "Matej";
    private Button bSwitchFragments;
    public boolean mAreSwitched =false;
    private final String EDIT_FRAG = "Edit_frag";
    private final String DISPLAY_FRAGMENT = "Display_fragment";
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setUpFragments();
        this.setUpUI();
    }

    private void setUpUI() {
        this.bSwitchFragments = (Button) findViewById(R.id.bSwitchFragments);
        this.bSwitchFragments.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction =
                                fragmentManager.beginTransaction();
                        if(mAreSwitched)
                        {
                            fragmentTransaction.replace(R.id.flPrimary,new EditFrag(),EDIT_FRAG);
                            fragmentTransaction.replace(R.id.flSecondary,new DisplayFrag(),DISPLAY_FRAGMENT);
                        }
                        else
                        {
                            fragmentTransaction.replace(R.id.flPrimary,new DisplayFrag(),DISPLAY_FRAGMENT);
                            fragmentTransaction.replace(R.id.flSecondary,new EditFrag(),EDIT_FRAG);
                        }
                        fragmentTransaction.commit();
                        mAreSwitched = !mAreSwitched;
                    }
                }
        );
    }

    private void setUpFragments() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.flPrimary,new EditFrag(),this.EDIT_FRAG);
        fragmentTransaction.add(R.id.flSecondary,new DisplayFrag(),this.DISPLAY_FRAGMENT);
        fragmentTransaction.commit();
    }

    @Override
    public void onTextChange(int TextSize, String TextMessage, String TextColor) {
        Log.d(LOG_TAG,"In event handler");
        FragmentManager fragmentManager = getFragmentManager();
        DisplayFrag displayFragment = (DisplayFrag) fragmentManager.findFragmentByTag(this.DISPLAY_FRAGMENT);
        displayFragment.setText(TextSize,TextMessage, TextColor);
    }

}
