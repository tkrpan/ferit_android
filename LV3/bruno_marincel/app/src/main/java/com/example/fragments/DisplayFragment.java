package com.example.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;


public class DisplayFragment extends android.app.Fragment {
    TextView tvMessage;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View Layout = inflater.inflate(R.layout.fragment_display,null);
        this.tvMessage = (TextView) Layout.findViewById(R.id.tvMessage);
        return Layout;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }
    public void setText(int TextSize, String TextMessage, String TextColor)
    {
        this.tvMessage.setText(TextMessage);
        this.tvMessage.setTextSize(TextSize);
        if (TextColor == "White") { this.tvMessage.setTextColor(this.getResources().getColor(R.color.white)); }
        if (TextColor == "Black") { this.tvMessage.setTextColor(this.getResources().getColor(R.color.black)); }
        if (TextColor == "Midnight Blue") { this.tvMessage.setTextColor(this.getResources().getColor(R.color.blue)); }

    }
}