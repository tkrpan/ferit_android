package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class resultActivity extends AppCompatActivity {

    private TextView firstChoice,secondChoice,result,startingData;
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        setView();
    }
    private void setView(){
        firstChoice = findViewById(R.id.textView);
        secondChoice= findViewById(R.id.textView3);
        startingData= findViewById(R.id.textView2);
        result =findViewById(R.id.textView4);
        linearLayout = findViewById(R.id.result);

        int firstChoicePosition,secondChoicePosition;
        double startData,resultData;
        String [] conversionOptions;

        Intent startingIntent2 = getIntent();

        firstChoicePosition = Integer.parseInt(startingIntent2.getStringExtra(ConversionActivity.CHOICE1));
        secondChoicePosition = Integer.parseInt(startingIntent2.getStringExtra(ConversionActivity.CHOICE2));
        startData = startingIntent2.getDoubleExtra(ConversionActivity.INPUT_DATA,0);
        conversionOptions = getConversionOptions(startingIntent2.getStringExtra(ConversionActivity.CONV));
        resultData = calculate(startingIntent2.getStringExtra(ConversionActivity.CONV),firstChoicePosition,secondChoicePosition,startData);

        setBackground(startingIntent2.getStringExtra(ConversionActivity.CONV));
        setTitles( conversionOptions,firstChoicePosition,secondChoicePosition,startData,resultData);

    }

    private void  setBackground(String conversionType){
        if(conversionType.equals("temp"))linearLayout.setBackgroundColor(getResources().getColor(R.color.tempColor));
        else if(conversionType.equals("dist"))linearLayout.setBackgroundColor(getResources().getColor(R.color.distColor));
        else if(conversionType.equals("vol"))linearLayout.setBackgroundColor(getResources().getColor(R.color.volColor));
        else linearLayout.setBackgroundColor(getResources().getColor(R.color.speedColor));
    }
    private String[] getConversionOptions(String conversionType){
        if(conversionType.equals("temp"))return getResources().getStringArray(R.array.temperature);
        else if(conversionType.equals("dist"))return getResources().getStringArray(R.array.distance);
        else if(conversionType.equals("vol"))return getResources().getStringArray(R.array.volume);
        else return getResources().getStringArray(R.array.speed);
    }
    private void setTitles(String array[],int firstChoicePos,int secondChoicePos,Double startData,Double resultData){
        firstChoice.setText(array[firstChoicePos]);
        secondChoice.setText(array[secondChoicePos]);
        startingData.setText(String.format("%.2f",startData));
        result.setText(String.format("%.2e",resultData));

    }
    private double calculate(String conversionType,int firstChoicePos,int secondChoicePos,Double startData){
        if(conversionType.equals("temp")){
            if(firstChoicePos==0 &&secondChoicePos==0){
                return startData;
            }
            else if(firstChoicePos==0 &&secondChoicePos==1){
                return ((startData-32)*5)/9+273.15;
            }
            else if(firstChoicePos==0 &&secondChoicePos==2){
                return (startData-32)/1.8;
            }
            else if(firstChoicePos==1 &&secondChoicePos==1){
                return startData;
            }
            else if(firstChoicePos==1 &&secondChoicePos==0){
                return (startData - 273.15)*1.8+ 32.00;
            }
            else if(firstChoicePos==1 &&secondChoicePos==2){
                return startData - 273.15;
            }
            else if(firstChoicePos==2 &&secondChoicePos==2){
                return startData;
            }
            else if(firstChoicePos==2 &&secondChoicePos==0){
                return startData*1.8 + 32;
            }else if(firstChoicePos==2 &&secondChoicePos==1){
                return startData - 273.15;
            }
        }
        else if(conversionType.equals("dist")){
            Double distMulti [][]={{1.0,0.001,0.000001,0.03937007874,0.0010936132983377,0.000000062137119223733},
                    {1000.0,1.0,0.001,39.37007874,1.0936,0.00062137},
                    {1000000.0,1000.0,1.0,39370.08,1093.613,0.6213712},
                    {25.4,0.0254,0.0000254,1.0,0.02777778,0.00001578283},
                    {914.4,0.9144,0.0009144,36.0,1.0,0.0005681818},
                    {1609344.0,1609.344,1.609344,63360.0,1760.0,1.0}};
            return startData*distMulti[firstChoicePos][secondChoicePos];
        }
        else if(conversionType.equals("vol")){
            Double volMulti [][]={{1.0,1000000000.0,35.315,61024.0},
                                  {0.000000001,1.0,0.000000035315,0.000061024},
                                  {0.02831685, 28316846.592,1.0,1728.0},
                                  {0.000016387064,16387.064 ,0.0005787037,1.0}};
            return startData*volMulti[firstChoicePos][secondChoicePos];
        }
        else {
            Double speedMulti [][]={{1.0,3.6,2.236936},
                                    {0.2777778,1.0,0.6213712419},
                                    {0.44704,1.609344,1.0}};
            return startData*speedMulti[firstChoicePos][secondChoicePos];
        }
        return 0;
    }

}
