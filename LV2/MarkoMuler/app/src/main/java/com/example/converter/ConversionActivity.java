package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Array;

public class ConversionActivity extends AppCompatActivity implements View.OnClickListener {
    //public static int choice1,choice2;
    private Button convertButton;
    private EditText inputData;
    private Spinner firstSpinner,secondSpinner;
    private TextView mainTitle;
    private LinearLayout linearLayout;
    public static String CHOICE1,CHOICE2,CONV,LOG_TAG,INPUT_DATA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);
        setView();
    }
    private void setView(){

        convertButton = findViewById(R.id.convertButton);
        inputData = findViewById(R.id.inputData);
        mainTitle = findViewById(R.id.conversionTitle);
        firstSpinner = findViewById(R.id.firstSpinner);
        secondSpinner = findViewById(R.id.secondSpinner);
        linearLayout = findViewById(R.id.conversion);

        Intent startingIntent = getIntent();

        CONV = startingIntent.getStringExtra(MainActivity.CONVERSION);
        mainTitle.setText(getTitle(startingIntent.getStringExtra(MainActivity.CONVERSION)));
        setBackground(startingIntent.getStringExtra(MainActivity.CONVERSION));
        ArrayAdapter<CharSequence> adapter = createAdapter(startingIntent.getStringExtra(MainActivity.CONVERSION));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        firstSpinner.setAdapter(adapter);
        secondSpinner.setAdapter(adapter);

        firstSpinner.setOnItemSelectedListener(new ConversionActivity.spinnerChoice());
        secondSpinner.setOnItemSelectedListener(new ConversionActivity.spinnerChoice1());
        convertButton.setOnClickListener(this);

    }

    private String getTitle(String conversionType){
        if(conversionType.equals("temp"))return getResources().getString(R.string.temp)+" "+ getResources().getString(R.string.conversion);
        else if(conversionType.equals("dist"))return getResources().getString(R.string.dist)+" "+ getResources().getString(R.string.conversion);
        else if(conversionType.equals("vol"))return getResources().getString(R.string.vol)+" "+ getResources().getString(R.string.conversion);
        else return getResources().getString(R.string.speed)+" "+ getResources().getString(R.string.conversion);
    }
    public void  setBackground(String conversionType){
        if(conversionType.equals("temp"))linearLayout.setBackgroundColor(getResources().getColor(R.color.tempColor));
        else if(conversionType.equals("dist"))linearLayout.setBackgroundColor(getResources().getColor(R.color.distColor));
        else if(conversionType.equals("vol"))linearLayout.setBackgroundColor(getResources().getColor(R.color.volColor));
        else linearLayout.setBackgroundColor(getResources().getColor(R.color.speedColor));
    }
    private ArrayAdapter<CharSequence> createAdapter(String conversionType){
        if(conversionType.equals("temp"))return ArrayAdapter.createFromResource(this,R.array.temperature,android.R.layout.simple_spinner_item);
        else if(conversionType.equals("dist"))return ArrayAdapter.createFromResource(this,R.array.distance,android.R.layout.simple_spinner_item);
        else if(conversionType.equals("vol"))return ArrayAdapter.createFromResource(this,R.array.volume,android.R.layout.simple_spinner_item);
        else return ArrayAdapter.createFromResource(this,R.array.speed,android.R.layout.simple_spinner_item);

    }
    public class spinnerChoice implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {
            CHOICE1=Integer.toString(parent.getSelectedItemPosition());
        }

        public void onNothingSelected(AdapterView<?> parent) {
            // Another interface callback
        }
    }
    public class spinnerChoice1 implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {
            CHOICE2=Integer.toString(parent.getSelectedItemPosition());
        }

        public void onNothingSelected(AdapterView<?> parent) {
            // Another interface callback
        }
    }
    public void onClick(View v){
        if(!inputData.getText().toString().isEmpty()){
            Intent newIntent = new Intent(this,resultActivity.class);
            newIntent.putExtra(CHOICE1,CHOICE1);
            Log.d(LOG_TAG, CHOICE1);
            newIntent.putExtra(CHOICE2,CHOICE2);
            Log.d(LOG_TAG, CHOICE2);
            newIntent.putExtra(CONV,CONV);
            Log.d(LOG_TAG, CONV);
            newIntent.putExtra(INPUT_DATA,Double.parseDouble(inputData.getText().toString()));
            this.startActivity(newIntent);
        }
    }



}
