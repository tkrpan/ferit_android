package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button tempButton,distButton,volButton,speedButton;
    public static final String CONVERSION = "conversion";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();
    }

    private void setView(){

        tempButton = findViewById(R.id.temp);
        distButton = findViewById(R.id.dist);
        volButton  = findViewById(R.id.vol);
        speedButton= findViewById(R.id.speed);

        tempButton.setOnClickListener(this);
        distButton.setOnClickListener(this);
        volButton.setOnClickListener(this);
        speedButton.setOnClickListener(this);

    }
    @Override
    public void onClick(View v){

        switch (v.getId()){

            case R.id.temp:{
                Intent intent = new Intent(this,ConversionActivity.class);
                intent.putExtra(CONVERSION,"temp");
                this.startActivity(intent);
                break;
            }
            case R.id.dist:{
                Intent intent = new Intent(this,ConversionActivity.class);
                intent.putExtra(CONVERSION,"dist");
                this.startActivity(intent);
                break;
            }
            case R.id.vol:{
                Intent intent = new Intent(this,ConversionActivity.class);
                intent.putExtra(CONVERSION,"vol");
                this.startActivity(intent);
                break;
            }
            case R.id.speed:{
                Intent intent = new Intent(this,ConversionActivity.class);
                intent.putExtra(CONVERSION,"speed");
                this.startActivity(intent);
                break;
            }
            default:
                break;
        }
    }
}
