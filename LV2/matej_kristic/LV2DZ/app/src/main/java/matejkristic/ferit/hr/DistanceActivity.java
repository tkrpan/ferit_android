package matejkristic.ferit.hr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class DistanceActivity extends AppCompatActivity {

    private Button btnCalc;
    private EditText etKiloMeters;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);
        setTitle("Distance conversion");
        setView();
    }

    private void setView(){

        btnCalc = findViewById(R.id.Calc);
        etKiloMeters = findViewById(R.id.Kilometri);


        btnCalc.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view){
                        double kilometriBroj = Double.parseDouble(etKiloMeters.getText().toString());
                        String rezultat = Double.toString(getMiles(kilometriBroj));
                        Intent rezultatIntent = new Intent(getApplicationContext(), ResultActivity.class);
                        rezultatIntent.putExtra("rezultat", rezultat);
                        rezultatIntent.putExtra("name","Miles");
                        startActivity(rezultatIntent);


                    }
                }
        );

    }

    private double getMiles(double broj){
        return broj/1.609;
    }

}