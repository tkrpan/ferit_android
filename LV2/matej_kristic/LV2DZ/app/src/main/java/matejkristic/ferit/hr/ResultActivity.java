package matejkristic.ferit.hr;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    private String valueRes;
    private String valueName;
    private TextView tvName;
    private TextView tvResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        valueRes = getIntent().getStringExtra("rezultat");
        valueName = getIntent().getStringExtra("name");
        setView();
    }

    private void setView() {
        tvName = findViewById(R.id.Name);
        tvResult = findViewById(R.id.Result);
        tvName.setText(valueName);
        tvResult.setText(valueRes);

    }


}