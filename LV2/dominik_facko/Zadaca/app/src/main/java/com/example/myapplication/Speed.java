package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class Speed extends AppCompatActivity {

    Spinner spinner,spinner2;
    Button BtnConvert;
    EditText etSpeed;
    int pos1,pos2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed);


        etSpeed= findViewById(R.id.textViewSpeed);
        BtnConvert =findViewById(R.id.button);
        spinner=findViewById(R.id.spinner1);
        spinner2=findViewById(R.id.spinner2);

        ArrayAdapter<CharSequence> adapter= ArrayAdapter.createFromResource(this,R.array.fromSpeed,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<CharSequence> adapter2=ArrayAdapter.createFromResource(this,R.array.speedChoice,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner2.setAdapter(adapter2);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pos1=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pos2=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        BtnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etSpeed.getText().toString().matches("")){
                    Toast.makeText(getApplicationContext(),"put a number",Toast.LENGTH_SHORT).show();
                    return;
                }else{

                }


            }
        });
    }
}
