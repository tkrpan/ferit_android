package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class Distance extends AppCompatActivity {

    Spinner spinner,spinner2;
    Button BtnConvert;
    EditText toConvert;
    int pos,pos2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance1);

        toConvert= findViewById(R.id.toconvert);
        BtnConvert =findViewById(R.id.button);
        spinner=findViewById(R.id.spinner1);
        spinner2=findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(this,R.array.conversion,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<CharSequence> adapter2=ArrayAdapter.createFromResource(this,R.array.conversion2,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner2.setAdapter(adapter2);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pos=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pos2=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        BtnConvert.setOnClickListener(new View.OnClickListener() {
            Intent intent = new Intent(getApplicationContext(), Result.class);
            @Override
            public void onClick(View v) {
                if (toConvert.getText().toString().matches("")) {
                    Toast.makeText(getApplicationContext(),"put a number",Toast.LENGTH_SHORT).show();
                    return;
                } else {

                    double value = Double.parseDouble(toConvert.getText().toString());
                    double result = 0;

                    switch (pos) {
                        case 0:
                            value = value;
                            result=calc(value);
                            intent.putExtra("flavor","Meters to");
                            break;
                        case 1:
                            value= value/10;
                            result=calc(value);
                            intent.putExtra("flavor","DM to");
                            break;
                        case 2:
                            value = value / 100;
                            result=calc(value);
                            intent.putExtra("flavor","CM to");
                            break;
                        case 3:
                            value = value / 1000;
                            result=calc(value);
                            intent.putExtra("flavor","MM to");
                            break;
                    }

                    intent.putExtra("result", result);
                    startActivity(intent);
                }
            }

            private double calc(double meters) {
                if (pos2==0){
                    intent.putExtra("flavor2","feet");
                    return meters*3.28084;
                }else{
                    intent.putExtra("flavor2","inch");
                    return meters*39.3701;
                }
            }
        });

    }




}
