package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Result extends Activity {

    TextView Result,Flavor,Flavor2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Result=findViewById(R.id.resultText);
        Flavor=findViewById(R.id.flavorText);
        Flavor2=findViewById(R.id.flavorText2);
        Bundle bundle=getIntent().getExtras();

        if(bundle.containsKey("flavor2")){
            String flavor2=bundle.getString("flavor2");
            Flavor2.setText(flavor2);
            Flavor2.setVisibility(View.VISIBLE);
        }

        String flavor = bundle.getString("flavor");
        Flavor.setText(flavor);

        double res =bundle.getDouble("result");
        Result.setText(Double.toString(res));
        }
    }

