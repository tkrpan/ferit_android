package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Temp extends AppCompatActivity {

    Button kelvin,celsius;
    EditText unit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);



        kelvin= findViewById(R.id.ToKelvin);
        celsius=findViewById(R.id.ToC);
        unit=findViewById(R.id.tempperature);

        kelvin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (unit.getText().toString().matches("")) {
                Toast.makeText(getApplicationContext(),"not good",Toast.LENGTH_SHORT).show();
                return;
                } else {
                double value = Double.parseDouble(unit.getText().toString());
                double result=value+273.15;
                String flavor = "to kelvin";
                    Intent intent =new Intent(getApplicationContext(),Result.class);
                    intent.putExtra("flavor",flavor);
                    intent.putExtra("result",result);
                    startActivity(intent);
                }
            }
        });

        celsius.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (unit.getText().toString().matches("")) {
                    Toast.makeText(getApplicationContext(), "not good", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    double value = Double.parseDouble(unit.getText().toString());
                    double result = value - 273.15;
                    String flavor = "to celsius";
                    Intent intent = new Intent(getApplicationContext(), Result.class);
                    intent.putExtra("flavor", flavor);
                    intent.putExtra("result", result);
                    startActivity(intent);
                }


            }
        });
    }
}
