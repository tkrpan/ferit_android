package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button Dist,Temp,Vol,Speed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();
    }


    private void setView(){

        Dist=findViewById(R.id.buttonDist);
        Temp=findViewById(R.id.buttonTemp);
        Vol=findViewById(R.id.buttonVol);
        Speed=findViewById(R.id.buttonSpeed);



        Dist.setOnClickListener(this);
        Temp.setOnClickListener(this);
        Vol.setOnClickListener(this);
        Speed.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case(R.id.buttonDist):
                Intent intent  = new Intent (this,Distance.class);
                startActivity(intent);
                break;
            case(R.id.buttonTemp):
                intent = new Intent(this, Temp.class);
                startActivity(intent);
                break;
            case(R.id.buttonVol):
                break;
            case(R.id.buttonSpeed):
                intent = new Intent(this, Speed.class);
                startActivity(intent);
                break;
        }

    }
}
