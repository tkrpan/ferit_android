package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button energy, mass, angle, power;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI(){
        energy = findViewById(R.id.energy);
        mass = findViewById(R.id.mass);
        angle = findViewById(R.id.angle);
        power = findViewById(R.id.power);
    }

    public void page1(View v){
        Intent intent = new Intent(MainActivity.this, EnergyActivity.class);
        startActivity(intent);
    }

    public void page2(View v){
        Intent intent = new Intent(MainActivity.this, MassActivity.class);
        startActivity(intent);
    }

    public void page3(View v){
        Intent intent = new Intent(MainActivity.this, AngleActivity.class);
        startActivity(intent);
    }

    public void page4(View v){
        Intent intent = new Intent(MainActivity.this, PowerActivity.class);
        startActivity(intent);
    }
}
