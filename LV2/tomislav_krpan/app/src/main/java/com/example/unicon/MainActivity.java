package com.example.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonTemp;
    private Button buttonDist;
    private Button buttonVol;
    private Button buttonSpeed;

    private static final String LOG_TAG = "unicon";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(LOG_TAG, "MainActivity onCreate");

        setView();
    }

    private void setView() {
        Log.d(LOG_TAG, "MainActivity setView");
        buttonTemp = findViewById(R.id.buttonTemp);
        buttonDist = findViewById(R.id.buttonDist);
        buttonVol = findViewById(R.id.buttonVol);
        buttonSpeed = findViewById(R.id.buttonSpeed);

        buttonTemp.setOnClickListener(this);
        buttonDist.setOnClickListener(this);
        buttonVol.setOnClickListener(this);
        buttonSpeed.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Log.d(LOG_TAG, "MainActivity onClick");

        switch (view.getId()){

            case R.id.buttonTemp:
                Log.d(LOG_TAG, "MainActivity onClick buttonTemp");
                break;

            case R.id.buttonDist:
                Log.d(LOG_TAG, "MainActivity onClick buttonDist");
                Intent intent = new Intent(this, DistActivity.class);
                startActivity(intent);
                break;

            case R.id.buttonVol:
                Log.d(LOG_TAG, "MainActivity onClick buttonVol");
                break;

            case R.id.buttonSpeed:
                Log.d(LOG_TAG, "MainActivity onClick buttonSpeed");
                break;

            default:
                break;
        }
    }


    @Override
    protected void onPause() {
        Log.d(LOG_TAG, "MainActivity onPause");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d(LOG_TAG, "MainActivity onDestroy");
        super.onDestroy();
    }
}
