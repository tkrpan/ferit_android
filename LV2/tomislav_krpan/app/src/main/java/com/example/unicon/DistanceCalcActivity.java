package com.example.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class DistanceCalcActivity extends AppCompatActivity {

    private static final String LOG_TAG = "unicon";

    private TextView textViewValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance_calc);

        Log.d(LOG_TAG, "DistanceCalcActivity onCreate");

        String value = getDataFromIntent();

        setView(value);
    }

    private String getDataFromIntent(){
        Log.d(LOG_TAG, "DistanceCalcActivity getDataFromIntent");
        Intent intent = getIntent();
        return intent.getStringExtra(DistActivity.DIST_VALUE);
    }

    private void setView(String value) {
        Log.d(LOG_TAG, "DistanceCalcActivity setView");
        textViewValue = findViewById(R.id.textViewValue);

        textViewValue.setText(value);
    }
}
