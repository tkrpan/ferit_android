package com.example.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class DistActivity extends AppCompatActivity {

    private static final String LOG_TAG = "unicon";
    public static final String DIST_VALUE = "DIST_VALUE";

    private Button button;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dist);

        Log.d(LOG_TAG, "DistActivity onCreate");
        setView();
    }

    private void setView() {
        Log.d(LOG_TAG, "DistActivity setView");
        editText = findViewById(R.id.editText);
        button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "DistActivity onClick");
                String value = editText.getText().toString();
                Intent intent = new Intent(DistActivity.this,
                        DistanceCalcActivity.class);
                intent.putExtra(DIST_VALUE, value);
                startActivity(intent);
            }
        });
    }
}
