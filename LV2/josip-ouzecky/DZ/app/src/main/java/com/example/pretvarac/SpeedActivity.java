package com.example.pretvarac;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SpeedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed);
        ArrayAdapter<CharSequence> adapter =ArrayAdapter.createFromResource(this, R.array.speed_array,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spin = findViewById(R.id.spinner);
        spin.setAdapter(adapter);

        Button btnConvert = findViewById(R.id.btnConvert);
        final EditText txtEntry = findViewById(R.id.txtEntry);

        btnConvert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Code for conversion here.
            }
        });

        final int[] pos = {0};
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent, View view, int position, long id) {
                pos[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });




        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                if (txtEntry.getText().toString().length() > 0){
                    String textValue = txtEntry.getText().toString();
                    double lastValue = Double.parseDouble(textValue);
                    double km_h_to_m_s, m_s_to_km_h;

                    if(pos[0] ==0){
                        km_h_to_m_s = lastValue / 3.6;
                        double rounded = Math.round(km_h_to_m_s * 100) / 100.00;
                        Intent intent =  new Intent(SpeedActivity.this, newactivity.class);
                        intent.putExtra("result", rounded);
                        startActivityForResult(intent, 1);

                    }else if(pos[0] ==1){
                        m_s_to_km_h = lastValue * 3.6;
                        double rounded = Math.round(m_s_to_km_h * 100) / 100.00;
                        Intent intent =  new Intent(SpeedActivity.this, newactivity.class);
                        intent.putExtra("result", rounded);
                        startActivityForResult(intent, 1);
                    }
                }  else{
                    Toast.makeText(getApplicationContext(),"Input something!",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}