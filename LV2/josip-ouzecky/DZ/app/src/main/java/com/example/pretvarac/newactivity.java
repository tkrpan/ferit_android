package com.example.pretvarac;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class newactivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newactivity);

        setTitle("Activity Result");

        Intent intent = getIntent();
        double result = intent.getDoubleExtra("result", 0);

        TextView  textViewResult = findViewById(R.id.textViewResult);
        textViewResult.setText("Result: " + result);

    }
}
