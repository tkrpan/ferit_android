package anamustafov.ferit.lv2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

public  class MainActivity extends AppCompatActivity implements View.OnClickListener {

    CardView cardTemperature, cardDistance, cardWeight, cardLiquid;
    Intent intent;
    Bundle bundle;
    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }

    private void initializeUI(){
        cardTemperature = (CardView) findViewById(R.id.tempCardView);
        cardDistance    = (CardView) findViewById(R.id.distanceCardView);
        cardWeight      = (CardView) findViewById(R.id.weightCardView);
        cardLiquid      = (CardView) findViewById(R.id.liquidCardView);

        this.cardTemperature.setOnClickListener(this);
        this.cardDistance.setOnClickListener(this);
        this.cardWeight.setOnClickListener(this);
        this.cardLiquid.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.tempCardView:
                bundle = new Bundle();
                bundle.putInt("metricSpinnerItems", R.array.temp_units_metric);
                bundle.putInt("imperialSpinnerItems", R.array.temp_units_imperial);
                bundle.putString("title", getResources().getString(R.string.tempConversionTitle));
                bundle.putString("conversion", "temperature");
                bundle.putInt("bgColor", (getResources().getColor(R.color.tempCardColor)));

                intent = new Intent(getApplicationContext(), ConversionActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.distanceCardView:
                bundle = new Bundle();
                bundle.putInt("metricSpinnerItems", R.array.distance_units_metric);
                bundle.putInt("imperialSpinnerItems", R.array.distance_units_imperial);
                bundle.putString("title", getResources().getString(R.string.distanceConversionTitle));
                bundle.putString("conversion", "distance");
                bundle.putInt("bgColor", (getResources().getColor(R.color.distanceCardColor)));

                intent = new Intent(MainActivity.this, ConversionActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.weightCardView:
                bundle = new Bundle();
                bundle.putInt("metricSpinnerItems", R.array.weight_unit_metric);
                bundle.putInt("imperialSpinnerItems", R.array.weight_unit_imperial);
                bundle.putString("title", getResources().getString(R.string.weightConversionTitle));
                bundle.putString("conversion", "weight");
                bundle.putInt("bgColor", (getResources().getColor(R.color.weightCardColor)));

                intent = new Intent(getApplicationContext(), ConversionActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.liquidCardView:
                bundle = new Bundle();

                bundle.putInt("metricSpinnerItems", R.array.liquid_unit_metric);
                bundle.putInt("imperialSpinnerItems", R.array.liquid_unit_imperial);
                bundle.putString("title", getResources().getString(R.string.liquidConversionTitle));
                bundle.putString("conversion", "liquid");
                bundle.putInt("bgColor", (getResources().getColor(R.color.liquidCardColor)));

                intent = new Intent(getApplicationContext(), ConversionActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }
}


