package anamustafov.ferit.lv2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ShowResultActivity extends AppCompatActivity {

    TextView userInputUnit, userInputValue, convertToUnit, convertToValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        initializeUI();
    }

    private void initializeUI(){
        userInputUnit   = (TextView) findViewById(R.id.tvUserInputUnit);
        userInputValue  = (TextView) findViewById(R.id.tvUserInputValue);
        convertToUnit   = (TextView) findViewById(R.id.tvConvertToUnit);
        convertToValue  = (TextView) findViewById(R.id.tvConverToValue);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();

        userInputUnit.setText(bundle.getString("userInput"));
        convertToUnit.setText(bundle.getString("convertTo"));
        userInputValue.setText(bundle.getString("userInputValue"));
        convertToValue.setText(bundle.getString("result"));

    }
}
