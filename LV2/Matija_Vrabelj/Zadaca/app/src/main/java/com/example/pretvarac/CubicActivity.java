package com.example.pretvarac;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CubicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cubic);
        ArrayAdapter<CharSequence> adapter =ArrayAdapter.createFromResource(this, R.array.cubic_array,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spin = findViewById(R.id.spinner);
        spin.setAdapter(adapter);

        Button btnConvert = findViewById(R.id.btnConvert);
        final EditText txtEntry = findViewById(R.id.txtEntry);

        btnConvert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Code for conversion here.
            }
        });

        final int[] pos = {0};
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent, View view, int position, long id) {
                pos[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                if (txtEntry.getText().toString().length() > 0){
                    String textValue = txtEntry.getText().toString();
                    double lastValue = Double.parseDouble(textValue);
                    double HP_to_kWh, kWh_to_HP;

                    if(pos[0] ==0){
                        HP_to_kWh = lastValue * 0.74;
                        Intent intent =  new Intent(CubicActivity.this, newactivity.class);
                        intent.putExtra("result", HP_to_kWh);
                        startActivityForResult(intent, 1);

                    }else if(pos[0] ==1){
                        kWh_to_HP = lastValue * 1.34;
                        Intent intent =  new Intent(CubicActivity.this, newactivity.class);
                        intent.putExtra("result", kWh_to_HP);
                        startActivityForResult(intent, 1);
                    }
                }  else{
                    Toast.makeText(getApplicationContext(),"Input something!",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}