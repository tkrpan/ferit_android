package com.example.lv2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private static final String LOG_TAG = "LV2";
    public static final String CONVERSION_TYPE = "CONVERSION_TYPE";

    private Button buttonTemp;
    private Button buttonVol;
    private Button buttonDist;
    private Button buttonSpeed;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(LOG_TAG,"MainActivity onCreate");

        setView();
    }

    private void setView()
    {
        Log.d(LOG_TAG,"MainActivity setView");

        buttonTemp = findViewById(R.id.buttonTemp);
        buttonVol = findViewById(R.id.buttonVol);
        buttonDist = findViewById(R.id.buttonDist);
        buttonSpeed = findViewById(R.id.buttonSpeed);

        buttonTemp.setOnClickListener(this);
        buttonVol.setOnClickListener(this);
        buttonDist.setOnClickListener(this);
        buttonSpeed.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        Log.d(LOG_TAG,"MainActivity onClick");

        String conversionType = "";

        switch(view.getId())
        {
            case R.id.buttonTemp:
                Log.d(LOG_TAG,"MainActivity onClick buttonTemp");
                conversionType="Temp";
                break;

            case R.id.buttonDist:
                Log.d(LOG_TAG,"MainActivity onClick buttonDist");
                conversionType="Dist";
                break;

            case R.id.buttonVol:
                Log.d(LOG_TAG,"MainActivity onClick buttonVol");
                conversionType="Vol";
                break;

            case R.id.buttonSpeed:
                Log.d(LOG_TAG,"MainActivity onClick buttonSpeed");
                conversionType="Speed";
                break;

            default:
                break;
        }
        Intent intent = new Intent(this, CalcActivity.class);
        intent.putExtra(CONVERSION_TYPE, conversionType);
        startActivity(intent);
    }

    @Override
    public void onPause()
    {
        Log.d(LOG_TAG,"MainActivity onPause");
        super.onPause();
    }

    @Override
    public void onDestroy()
    {
        Log.d(LOG_TAG,"MainActivity onDestroy");
        super.onDestroy();
    }
}
