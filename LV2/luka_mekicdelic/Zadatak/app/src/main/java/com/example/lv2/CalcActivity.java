package com.example.lv2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class CalcActivity extends AppCompatActivity
{
    private static final String LOG_TAG = "LV2";
    public static final String INPUT_VALUE = "INPUT_VALUE";
    public static final String INPUT_UNIT = "INPUT_UNIT";
    public static final String RESULT_VALUE = "RESULT_VALUE";
    public static final String RESULT_UNIT = "RESULT_UNIT";

    private TextView textViewTitle;
    private EditText editTextValue;
    private Spinner spinnerUnitFrom;
    private Spinner spinnerUnitTo;
    private Button buttonCalculate;

    private String conversionType;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc);

        Log.d(LOG_TAG,"CalcActivity onCreate");

        conversionType = getDataFromIntent();

        setView();
    }

    private String getDataFromIntent()
    {
        Log.d(LOG_TAG,"CalcActivity getDataFromIntent");
        Intent intent = getIntent();
        return intent.getStringExtra(MainActivity.CONVERSION_TYPE);
    }

    private void setView()
    {
        Log.d(LOG_TAG,"CalcActivity setView");

        textViewTitle = findViewById(R.id.textViewTitle);
        editTextValue = findViewById(R.id.editTextValue);
        spinnerUnitFrom = findViewById(R.id.spinnerUnitFrom);
        spinnerUnitTo = findViewById(R.id.spinnerUnitTo);
        buttonCalculate = findViewById(R.id.buttonCalculate);

        int conversionArray = 0;
        switch(conversionType)
        {
            case "Temp":
                Log.d(LOG_TAG,"CalcActivity got \"Temp\" Extra");
                textViewTitle.setText("TEMPERATURE CONVERSION");
                editTextValue.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                conversionArray = R.array.array_temp;
                break;
            case "Dist":
                Log.d(LOG_TAG,"CalcActivity got \"Dist\" Extra");
                textViewTitle.setText("DISTANCE CONVERSION");
                conversionArray = R.array.array_dist;
                break;
            case "Vol":
                Log.d(LOG_TAG,"CalcActivity got \"Vol\" Extra");
                textViewTitle.setText("VOLUME CONVERSION");
                conversionArray = R.array.array_vol;
                break;
            case "Speed":
                Log.d(LOG_TAG,"CalcActivity got \"Speed\" Extra");
                textViewTitle.setText("SPEED CONVERSION");
                conversionArray = R.array.array_speed;
                break;

            default:
                Log.d(LOG_TAG,"CalcActivity got no Extras!!!");
                break;
        }

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, conversionArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUnitFrom.setAdapter(adapter);
        spinnerUnitTo.setAdapter(adapter);

        spinnerUnitTo.setSelection(1);

        //For getting last selected item in OnItemSelectedListener
        spinnerUnitFrom.setTag(spinnerUnitFrom.getSelectedItemPosition());
        spinnerUnitTo.setTag(spinnerUnitTo.getSelectedItemPosition());

        spinnerUnitFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
            {
                int lastPos = (int)spinnerUnitFrom.getTag();

                Log.d(LOG_TAG,"spinnerUnitFrom lastPos: " + lastPos + " pos: " + pos);
                if(pos==spinnerUnitTo.getSelectedItemPosition())
                    spinnerUnitTo.setSelection(lastPos);
                spinnerUnitFrom.setTag(pos);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
                //Do nothing
            }
        });
        spinnerUnitTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
            {
                int lastPos = (int)spinnerUnitTo.getTag();

                Log.d(LOG_TAG,"spinnerUnitTo lastPos: " + lastPos + " pos: " + pos);
                if(pos==spinnerUnitFrom.getSelectedItemPosition())
                    spinnerUnitFrom.setSelection(lastPos);
                spinnerUnitTo.setTag(pos);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
                //Do nothing
            }
        });

        editTextValue.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE))
                {
                    Log.d(LOG_TAG,"Keyboard \"Done\" pressed");

                    calculate();
                }
                return false;
            }
        });
        buttonCalculate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d(LOG_TAG,"CalcActivity onClick");

                calculate();
            }
        });
    }

    private void calculate()
    {
        String inputValueString = editTextValue.getText().toString();
        String inputUnitString = spinnerUnitFrom.getSelectedItem().toString();
        String resultValueString = "";
        String resultUnitString = spinnerUnitTo.getSelectedItem().toString();

        double inputValue = 0;
        if(!inputValueString.equals(""))
            inputValue = Double.parseDouble(inputValueString);
        else
            inputValueString = "0";
        double resultValue = inputValue; //In case user selects the same from and to units

        String unitFrom = spinnerUnitFrom.getSelectedItem().toString();
        String unitTo = spinnerUnitTo.getSelectedItem().toString();

        double conversionMultiplier = 1; //For all calculations except temperature

        switch(conversionType)
        {
            case "Temp":
                if(unitFrom.equals("°C") && unitTo.equals("°F"))
                    resultValue = inputValue * 9 / 5 + 32;
                else if(unitFrom.equals("°F") && unitTo.equals("°C"))
                    resultValue = (inputValue - 32) * ((double)5 / 9);
                break;
            case "Dist":
                switch(unitFrom)
                {
                    case "mm":
                        switch(unitTo)
                        {
                            case "mm":
                                break;
                            case "cm":
                                conversionMultiplier = 0.1;
                                break;
                            case "m":
                                conversionMultiplier = 0.001;
                                break;
                            case "km":
                                conversionMultiplier = 0.000001;
                                break;
                            case "in":
                                conversionMultiplier = 1/25.4;
                                break;
                            case "ft":
                                conversionMultiplier = (double)1/305;
                                break;
                            case "yd":
                                conversionMultiplier = (double)1/914;
                                break;
                            case "mi":
                                conversionMultiplier = 1/1.609e+6;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "cm":
                        switch(unitTo)
                        {
                            case "mm":
                                conversionMultiplier = 10;
                                break;
                            case "cm":
                                break;
                            case "m":
                                conversionMultiplier = 0.01;
                                break;
                            case "km":
                                conversionMultiplier = 0.00001;
                                break;
                            case "in":
                                conversionMultiplier = 1/2.54;
                                break;
                            case "ft":
                                conversionMultiplier = 1/30.5;
                                break;
                            case "yd":
                                conversionMultiplier = 1/91.4;
                                break;
                            case "mi":
                                conversionMultiplier = 1/1.609e+5;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "m":
                        switch(unitTo)
                        {
                            case "mm":
                                conversionMultiplier = 1000;
                                break;
                            case "cm":
                                conversionMultiplier = 100;
                                break;
                            case "m":
                                break;
                            case "km":
                                conversionMultiplier = 0.001;
                                break;
                            case "in":
                                conversionMultiplier = 1/0.0254;
                                break;
                            case "ft":
                                conversionMultiplier = 1/0.305;
                                break;
                            case "yd":
                                conversionMultiplier = 1/0.914;
                                break;
                            case "mi":
                                conversionMultiplier = 1/1.609e+3;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "km":
                        switch(unitTo)
                        {
                            case "mm":
                                conversionMultiplier = 1000000;
                                break;
                            case "cm":
                                conversionMultiplier = 100000;
                                break;
                            case "m":
                                conversionMultiplier = 1000;
                                break;
                            case "km":
                                break;
                            case "in":
                                conversionMultiplier = 1/0.0000254;
                                break;
                            case "ft":
                                conversionMultiplier = 1/0.000305;
                                break;
                            case "yd":
                                conversionMultiplier = 1/0.000914;
                                break;
                            case "mi":
                                conversionMultiplier = 1/1.609;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "in":
                        switch(unitTo)
                        {
                            case "mm":
                                conversionMultiplier = 25.4;
                                break;
                            case "cm":
                                conversionMultiplier = 2.54;
                                break;
                            case "m":
                                conversionMultiplier = 0.0254;
                                break;
                            case "km":
                                conversionMultiplier = 0.0000254;
                                break;
                            case "in":
                                break;
                            case "ft":
                                conversionMultiplier = 1.0/12;
                                break;
                            case "yd":
                                conversionMultiplier = 1.0/36;
                                break;
                            case "mi":
                                conversionMultiplier = 1.0/63360;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "ft":
                        switch(unitTo)
                        {
                            case "mm":
                                conversionMultiplier = 305;
                                break;
                            case "cm":
                                conversionMultiplier = 30.5;
                                break;
                            case "m":
                                conversionMultiplier = 0.305;
                                break;
                            case "km":
                                conversionMultiplier = 0.000305;
                                break;
                            case "in":
                                conversionMultiplier = 12;
                                break;
                            case "ft":
                                break;
                            case "yd":
                                conversionMultiplier = 1.0/3;
                                break;
                            case "mi":
                                conversionMultiplier = 1.0/5280;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "yd":
                        switch(unitTo)
                        {
                            case "mm":
                                conversionMultiplier = 914;
                                break;
                            case "cm":
                                conversionMultiplier = 91.4;
                                break;
                            case "m":
                                conversionMultiplier = 0.914;
                                break;
                            case "km":
                                conversionMultiplier = 0.000914;
                                break;
                            case "in":
                                conversionMultiplier = 36;
                                break;
                            case "ft":
                                conversionMultiplier = 3;
                                break;
                            case "yd":
                                break;
                            case "mi":
                                conversionMultiplier = 1.0/1760;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "mi":
                        switch(unitTo)
                        {
                            case "mm":
                                conversionMultiplier = 1.609e+6;
                                break;
                            case "cm":
                                conversionMultiplier = 1.609e+5;
                                break;
                            case "m":
                                conversionMultiplier = 1.609e+3;
                                break;
                            case "km":
                                conversionMultiplier = 1.609;
                                break;
                            case "in":
                                conversionMultiplier = 63360;
                                break;
                            case "ft":
                                conversionMultiplier = 5280;
                                break;
                            case "yd":
                                conversionMultiplier = 1760;
                                break;
                            case "mi":
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;

                    default:
                        Log.d(LOG_TAG,"WARNING: No valid unitFrom found!");
                        break;
                }
                break;
            case "Vol":
                switch(unitFrom)
                {
                    case "mm3":
                        switch(unitTo)
                        {
                            case "mm3":
                                break;
                            case "m3":
                                conversionMultiplier = 0.000000001;
                                break;
                            case "ml":
                                conversionMultiplier = 0.001;
                                break;
                            case "l":
                                conversionMultiplier = 0.000001;
                                break;
                            case "pt":
                                conversionMultiplier = 1.0/568261;
                                break;
                            case "gal":
                                conversionMultiplier = 1.0/4.546e+6;
                                break;
                            case "bbl":
                                conversionMultiplier = 0.0000000063;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "m3":
                        switch(unitTo)
                        {
                            case "mm3":
                                conversionMultiplier = 1e+9;
                                break;
                            case "m3":
                                break;
                            case "ml":
                                conversionMultiplier = 1e+6;
                                break;
                            case "l":
                                conversionMultiplier = 1000;
                                break;
                            case "pt":
                                conversionMultiplier = 1760;
                                break;
                            case "gal":
                                conversionMultiplier = 220;
                                break;
                            case "bbl":
                                conversionMultiplier = 6.29;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "ml":
                        switch(unitTo)
                        {
                            case "mm3":
                                conversionMultiplier = 1000;
                                break;
                            case "m3":
                                conversionMultiplier = 1e-6;
                                break;
                            case "ml":
                                break;
                            case "l":
                                conversionMultiplier = 0.001;
                                break;
                            case "pt":
                                conversionMultiplier = 0.00175975;
                                break;
                            case "gal":
                                conversionMultiplier = 0.000219969;
                                break;
                            case "bbl":
                                conversionMultiplier = 6.28981e-6;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "l":
                        switch(unitTo)
                        {
                            case "mm3":
                                conversionMultiplier = 1000000;
                                break;
                            case "m3":
                                conversionMultiplier = 0.001;
                                break;
                            case "ml":
                                conversionMultiplier = 1000;
                                break;
                            case "l":
                                break;
                            case "pt":
                                conversionMultiplier = 1.75975;
                                break;
                            case "gal":
                                conversionMultiplier = 0.219969;
                                break;
                            case "bbl":
                                conversionMultiplier = 0.00628981;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "pt":
                        switch(unitTo)
                        {
                            case "mm3":
                                conversionMultiplier = 568261;
                                break;
                            case "m3":
                                conversionMultiplier = 0.000568261;
                                break;
                            case "ml":
                                conversionMultiplier = 568.261;
                                break;
                            case "l":
                                conversionMultiplier = 0.568261;
                                break;
                            case "pt":
                                break;
                            case "gal":
                                conversionMultiplier = 0.125;
                                break;
                            case "bbl":
                                conversionMultiplier = 0.00297619;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "gal":
                        switch(unitTo)
                        {
                            case "mm3":
                                conversionMultiplier = 4.546e+6;
                                break;
                            case "m3":
                                conversionMultiplier = 0.00454609;
                                break;
                            case "ml":
                                conversionMultiplier = 4546.09;
                                break;
                            case "l":
                                conversionMultiplier = 4.54609;
                                break;
                            case "pt":
                                conversionMultiplier = 8;
                                break;
                            case "gal":
                                break;
                            case "bbl":
                                conversionMultiplier = 0.031746031745;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "bbl":
                        switch(unitTo)
                        {
                            case "mm3":
                                conversionMultiplier = 1.59e+8;
                                break;
                            case "m3":
                                conversionMultiplier = 0.158987;
                                break;
                            case "ml":
                                conversionMultiplier = 158987;
                                break;
                            case "l":
                                conversionMultiplier = 158.987;
                                break;
                            case "pt":
                                conversionMultiplier = 279.779;
                                break;
                            case "gal":
                                conversionMultiplier = 34.9723;
                                break;
                            case "bbl":
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;

                    default:
                        Log.d(LOG_TAG,"WARNING: No valid unitFrom found!");
                        break;
                }
                break;
            case "Speed":
                switch(unitFrom)
                {
                    case "m/s":
                        switch(unitTo)
                        {
                            case "m/s":
                                break;
                            case "km/h":
                                conversionMultiplier = 3.6;
                                break;
                            case "ft/s":
                                conversionMultiplier = 3.28084;
                                break;
                            case "mph":
                                conversionMultiplier = 2.23694;
                                break;
                            case "knot":
                                conversionMultiplier = 1.94384;
                                break;
                            case "Ma":
                                conversionMultiplier = 0.00291545;
                                break;
                            case "c":
                                conversionMultiplier = 1/299792458.0;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "km/h":
                        switch(unitTo)
                        {
                            case "m/s":
                                conversionMultiplier = 1/3.6;
                                break;
                            case "km/h":
                                break;
                            case "ft/s":
                                conversionMultiplier = 1/1.097;
                                break;
                            case "mph":
                                conversionMultiplier = 1/1.609;
                                break;
                            case "knot":
                                conversionMultiplier = 1/1.852;
                                break;
                            case "Ma":
                                conversionMultiplier = 1/1235.0;
                                break;
                            case "c":
                                conversionMultiplier = 1.079e-9;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "ft/s":
                        switch(unitTo)
                        {
                            case "m/s":
                                conversionMultiplier = 0.3048;
                                break;
                            case "km/h":
                                conversionMultiplier = 1.09728;
                                break;
                            case "ft/s":
                                break;
                            case "mph":
                                conversionMultiplier = 1/1.467;
                                break;
                            case "knot":
                                conversionMultiplier = 1/1.688;
                                break;
                            case "Ma":
                                conversionMultiplier = 1/1235.0;
                                break;
                            case "c":
                                conversionMultiplier = 1.0167e-9;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "mph":
                        switch(unitTo)
                        {
                            case "m/s":
                                conversionMultiplier = 0.44704;
                                break;
                            case "km/h":
                                conversionMultiplier = 1.60934;
                                break;
                            case "ft/s":
                                conversionMultiplier = 1.46667;
                                break;
                            case "mph":
                                break;
                            case "knot":
                                conversionMultiplier = 1/1.151;
                                break;
                            case "Ma":
                                conversionMultiplier = 1/767.0;
                                break;
                            case "c":
                                conversionMultiplier = 1/670616629.0;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "knot":
                        switch(unitTo)
                        {
                            case "m/s":
                                conversionMultiplier = 0.514444;
                                break;
                            case "km/h":
                                conversionMultiplier = 1.852;
                                break;
                            case "ft/s":
                                conversionMultiplier = 1.68781;
                                break;
                            case "mph":
                                conversionMultiplier = 1.15078;
                                break;
                            case "knot":
                                break;
                            case "Ma":
                                conversionMultiplier = 0.00149984;
                                break;
                            case "c":
                                conversionMultiplier = 1/582749918.0;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "Ma":
                        switch(unitTo)
                        {
                            case "m/s":
                                conversionMultiplier = 343;
                                break;
                            case "km/h":
                                conversionMultiplier = 1234.8;
                                break;
                            case "ft/s":
                                conversionMultiplier = 1125.33;
                                break;
                            case "mph":
                                conversionMultiplier = 767.269;
                                break;
                            case "knot":
                                conversionMultiplier = 666.739;
                                break;
                            case "Ma":
                                break;
                            case "c":
                                conversionMultiplier = 1/874030.0;
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;
                    case "c":
                        switch(unitTo)
                        {
                            case "m/s":
                                conversionMultiplier = 299792458;
                                break;
                            case "km/h":
                                conversionMultiplier = 1.07925285e+10;
                                break;
                            case "ft/s":
                                conversionMultiplier = 983571056;
                                break;
                            case "mph":
                                conversionMultiplier = 6.706e+8;
                                break;
                            case "knot":
                                conversionMultiplier = 5.827e+8;
                                break;
                            case "Ma":
                                conversionMultiplier = 874030;
                                break;
                            case "c":
                                break;

                            default:
                                Log.d(LOG_TAG,"WARNING: No valid unitTo found!");
                                break;
                        }
                        break;

                    default:
                        Log.d(LOG_TAG,"WARNING: No valid unitFrom found!");
                        break;
                }
                break;

            default:
                Log.d(LOG_TAG,"WARNING: No valid conversionType found!");
                break;
        }

        if(!conversionType.equals("Temp"))
            resultValue = inputValue * conversionMultiplier;

        resultValueString = String.valueOf(resultValue);

        Intent intent = new Intent(CalcActivity.this, ResultActivity.class);
        intent.putExtra(INPUT_VALUE,inputValueString);
        intent.putExtra(INPUT_UNIT,inputUnitString);
        intent.putExtra(RESULT_VALUE,resultValueString);
        intent.putExtra(RESULT_UNIT,resultUnitString);
        startActivity(intent);
    }
}
