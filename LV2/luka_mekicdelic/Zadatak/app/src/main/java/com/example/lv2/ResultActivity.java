package com.example.lv2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity
{
    private static final String LOG_TAG = "LV2";

    private TextView textViewInputValue;
    private TextView textViewInputUnit;
    private TextView textViewResultValue;
    private TextView textViewResultUnit;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Log.d(LOG_TAG,"ResultActivity onCreate");

        Intent intent = getIntent();
        String inputValueString = intent.getStringExtra(CalcActivity.INPUT_VALUE);
        String inputUnitString = intent.getStringExtra(CalcActivity.INPUT_UNIT);
        String resultValueString = intent.getStringExtra(CalcActivity.RESULT_VALUE);
        String resultUnitString = intent.getStringExtra(CalcActivity.RESULT_UNIT);

        setView(inputValueString, inputUnitString, resultValueString, resultUnitString);
    }

    private void setView(String inputValueString, String inputUnitString, String resultValueString, String resultUnitString)
    {
        Log.d(LOG_TAG,"ResultActivity setView");

        textViewInputValue = findViewById(R.id.textViewInputValue);
        textViewInputUnit = findViewById(R.id.textViewInputUnit);
        textViewResultValue = findViewById(R.id.textViewResultValue);
        textViewResultUnit = findViewById(R.id.textViewResultUnit);

        textViewInputValue.setText(inputValueString);
        textViewInputUnit.setText(inputUnitString);
        textViewResultValue.setText(resultValueString);
        textViewResultUnit.setText(resultUnitString);
    }
}
