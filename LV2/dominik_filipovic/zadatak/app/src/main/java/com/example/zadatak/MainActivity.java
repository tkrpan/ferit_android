package com.example.zadatak;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button temperature,distance,power,speed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        temperature = findViewById(R.id.temperature);
        distance = findViewById(R.id.distance);
        power = findViewById(R.id.cube);
        speed = findViewById(R.id.speed);


    }


    public void Temperature(View v){
        Intent intent = new Intent(MainActivity.this, Temperature.class);
        startActivity(intent);
    }

    public void Distance(View v){
        Intent intent = new Intent(MainActivity.this, Distance.class);
        startActivity(intent);
    }
    public void Power(View v){
        Intent intent = new Intent(MainActivity.this, Power.class);
        startActivity(intent);
    }
    public void Speed(View v){
        Intent intent = new Intent(MainActivity.this, Speed.class);
        startActivity(intent);
    }
}