package com.example.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class PowerActivity extends AppCompatActivity implements View.OnClickListener {
    Spinner spinnerPowerUnitsFrom;
    Spinner spinnerPowerUnitsTo;
    TextView textViewResult;
    EditText editTextEnterValue;
    Button buttonConvert;
    String selectedPowerUnitFrom, selectedPowerUnitTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_power);
        setUpUI();
    }

    public void setUpUI() {
        this.spinnerPowerUnitsFrom = (Spinner) findViewById(R.id.spinnerPowerUnitsFrom);
        this.spinnerPowerUnitsTo = (Spinner) findViewById(R.id.spinnerPowerUnitsTo);
        this.editTextEnterValue = (EditText) findViewById(R.id.editTextEnterValue);
        this.textViewResult = (TextView) findViewById(R.id.textViewResult);
        this.buttonConvert = (Button) findViewById(R.id.buttonConvert);

        ArrayAdapter<CharSequence> powerUnitsAdapterFrom = ArrayAdapter.createFromResource(this, R.array.powerUnitsFrom, android.R.layout.simple_spinner_item);
        powerUnitsAdapterFrom.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        ArrayAdapter<CharSequence> powerUnitsAdapterTo = ArrayAdapter.createFromResource(this, R.array.powerUnitsTo, android.R.layout.simple_spinner_item);
        powerUnitsAdapterTo.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        this.spinnerPowerUnitsFrom.setAdapter(powerUnitsAdapterFrom);
        this.spinnerPowerUnitsTo.setAdapter(powerUnitsAdapterTo);

        this.spinnerPowerUnitsFrom.setOnItemSelectedListener(new PowerActivity.PowerUnitFrom());
        this.spinnerPowerUnitsTo.setOnItemSelectedListener(new PowerActivity.PowerUnitTo());
        this.buttonConvert.setOnClickListener(this);
    }

    class PowerUnitFrom implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            selectedPowerUnitFrom = parent.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    class PowerUnitTo implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            selectedPowerUnitTo = parent.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    @Override
    public void onClick(View v) {
        if(!this.editTextEnterValue.getText().toString().isEmpty()) {
            String resultValue = "0";
            double enterValue = Double.parseDouble(this.editTextEnterValue.getText().toString());

            if(selectedPowerUnitFrom.equals("Kilowatts") && selectedPowerUnitTo.equals("Horsepower"))
                resultValue = Double.toString(enterValue  / 0.745699872) + " hp";
            else if(selectedPowerUnitFrom.equals("Horsepower") && selectedPowerUnitTo.equals("Kilowatts"))
                resultValue = Double.toString(enterValue  * 0.745699872) + " kw";
            else
                resultValue = Double.toString(enterValue);

            textViewResult.setText(resultValue);
            textViewResult.setVisibility(View.VISIBLE);
        }
    }
}
