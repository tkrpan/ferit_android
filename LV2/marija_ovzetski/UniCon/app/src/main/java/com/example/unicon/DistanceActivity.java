package com.example.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class DistanceActivity extends AppCompatActivity implements View.OnClickListener {
    Spinner spinnerDistanceUnitsFrom;
    Spinner spinnerDistanceUnitsTo;
    TextView textViewResult;
    EditText editTextEnterValue;
    Button buttonConvert;
    String selectedDistanceUnitFrom, selectedDistanceUnitTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);
        setUpUI();
    }

    public void setUpUI() {
        this.spinnerDistanceUnitsFrom = (Spinner) findViewById(R.id.spinnerDistanceUnitsFrom);
        this.spinnerDistanceUnitsTo = (Spinner) findViewById(R.id.spinnerDistanceUnitsTo);
        this.editTextEnterValue = (EditText) findViewById(R.id.editTextEnterValue);
        this.textViewResult = (TextView) findViewById(R.id.textViewResult);
        this.buttonConvert = (Button) findViewById(R.id.buttonConvert);

        ArrayAdapter<CharSequence> distanceUnitsAdapterFrom = ArrayAdapter.createFromResource(this, R.array.distanceUnitsFrom, android.R.layout.simple_spinner_item);
        distanceUnitsAdapterFrom.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        ArrayAdapter<CharSequence> distanceUnitsAdapterTo = ArrayAdapter.createFromResource(this, R.array.distanceUnitsTo, android.R.layout.simple_spinner_item);
        distanceUnitsAdapterTo.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        this.spinnerDistanceUnitsFrom.setAdapter(distanceUnitsAdapterFrom);
        this.spinnerDistanceUnitsTo.setAdapter(distanceUnitsAdapterTo);

        this.spinnerDistanceUnitsFrom.setOnItemSelectedListener(new DistanceActivity.DistanceUnitFrom());
        this.spinnerDistanceUnitsTo.setOnItemSelectedListener(new DistanceActivity.DistanceUnitTo());
        this.buttonConvert.setOnClickListener(this);
    }

    class DistanceUnitFrom implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            selectedDistanceUnitFrom = parent.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    class DistanceUnitTo implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            selectedDistanceUnitTo = parent.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    @Override
    public void onClick(View v) {
        if(!this.editTextEnterValue.getText().toString().isEmpty()) {
            String resultValue = "0";
            double enterValue = Double.parseDouble(this.editTextEnterValue.getText().toString());

            if(selectedDistanceUnitFrom.equals("Centimeters") && selectedDistanceUnitTo.equals("Meters"))
                resultValue = Double.toString(enterValue / 100) + " m";
            else if(selectedDistanceUnitFrom.equals("Centimeters") && selectedDistanceUnitTo.equals("Kilometers"))
                resultValue = Double.toString(enterValue / 10000) + " °m";
            else if(selectedDistanceUnitFrom.equals("Centimeters") && selectedDistanceUnitTo.equals("Inches"))
                resultValue = Double.toString(enterValue * 0.39370) + " \"";
            else if(selectedDistanceUnitFrom.equals("Meters") && selectedDistanceUnitTo.equals("Centimeters"))
                resultValue = Double.toString(enterValue * 100) + " cm";
            else if(selectedDistanceUnitFrom.equals("Meters") && selectedDistanceUnitTo.equals("Kilometers"))
                resultValue = Double.toString(enterValue / 1000) + " km";
            else if(selectedDistanceUnitFrom.equals("Meters") && selectedDistanceUnitTo.equals("Inches"))
                resultValue = Double.toString(enterValue * 39.370) + " \"";
            else if(selectedDistanceUnitFrom.equals("Kilometers") && selectedDistanceUnitTo.equals("Centimeters"))
                resultValue = Double.toString(enterValue * 10000) + " cm";
            else if(selectedDistanceUnitFrom.equals("Kilometers") && selectedDistanceUnitTo.equals("Meters"))
                resultValue = Double.toString(enterValue * 1000) + " m";
            else if(selectedDistanceUnitFrom.equals("Kilometers") && selectedDistanceUnitTo.equals("Inches"))
                resultValue = Double.toString(enterValue * 39370) + " \"";
            else if(selectedDistanceUnitFrom.equals("Inches") && selectedDistanceUnitTo.equals("Centimeters"))
                resultValue = Double.toString(enterValue / 0.39370) + " cm";
            else if(selectedDistanceUnitFrom.equals("Inches") && selectedDistanceUnitTo.equals("Meters"))
                resultValue = Double.toString(enterValue / 39.370) + " m";
            else if(selectedDistanceUnitFrom.equals("Inches") && selectedDistanceUnitTo.equals("Kilometers"))
                resultValue = Double.toString(enterValue / 39370) + " km";
            else
                resultValue = Double.toString(enterValue);

            textViewResult.setText(resultValue);
            textViewResult.setVisibility(View.VISIBLE);
        }
    }
}

