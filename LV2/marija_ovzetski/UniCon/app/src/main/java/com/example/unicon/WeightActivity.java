package com.example.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class WeightActivity extends AppCompatActivity implements View.OnClickListener {
    Spinner spinnerWeightUnitsFrom;
    Spinner spinnerWeightUnitsTo;
    TextView textViewResult;
    EditText editTextEnterValue;
    Button buttonConvert;
    String selectedWeightUnitFrom, selectedWeightUnitTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight);
        setUpUI();
    }

    public void setUpUI() {
        this.spinnerWeightUnitsFrom = (Spinner) findViewById(R.id.spinnerWeightUnitsFrom);
        this.spinnerWeightUnitsTo = (Spinner) findViewById(R.id.spinnerWeightUnitsTo);
        this.editTextEnterValue = (EditText) findViewById(R.id.editTextEnterValue);
        this.textViewResult = (TextView) findViewById(R.id.textViewResult);
        this.buttonConvert = (Button) findViewById(R.id.buttonConvert);

        ArrayAdapter<CharSequence> weightUnitsAdapterFrom = ArrayAdapter.createFromResource(this, R.array.weightUnitsFrom, android.R.layout.simple_spinner_item);
        weightUnitsAdapterFrom.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        ArrayAdapter<CharSequence> weightUnitsAdapterTo = ArrayAdapter.createFromResource(this, R.array.weightUnitsTo, android.R.layout.simple_spinner_item);
        weightUnitsAdapterTo.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        this.spinnerWeightUnitsFrom.setAdapter(weightUnitsAdapterFrom);
        this.spinnerWeightUnitsTo.setAdapter(weightUnitsAdapterTo);

        this.spinnerWeightUnitsFrom.setOnItemSelectedListener(new WeightActivity.WeightUnitFrom());
        this.spinnerWeightUnitsTo.setOnItemSelectedListener(new WeightActivity.WeightUnitTo());
        this.buttonConvert.setOnClickListener(this);
    }

    class WeightUnitFrom implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            selectedWeightUnitFrom = parent.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    class WeightUnitTo implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            selectedWeightUnitTo = parent.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    @Override
    public void onClick(View v) {
        if(!this.editTextEnterValue.getText().toString().isEmpty()) {
            String resultValue = "0";
            double enterValue = Double.parseDouble(this.editTextEnterValue.getText().toString());

            if(selectedWeightUnitFrom.equals("Milligrams") && selectedWeightUnitTo.equals("Grams"))
                resultValue = Double.toString(enterValue / 1000) + " g";
            else if(selectedWeightUnitFrom.equals("Milligrams") && selectedWeightUnitTo.equals("Kilograms"))
                resultValue = Double.toString(enterValue / 1000000) + " kg";
            else if(selectedWeightUnitFrom.equals("Milligrams") && selectedWeightUnitTo.equals("Pounds"))
                resultValue = Double.toString(enterValue * 0.0000022046) + " lb";
            else if(selectedWeightUnitFrom.equals("Grams") && selectedWeightUnitTo.equals("Milligrams"))
                resultValue = Double.toString(enterValue * 1000) + " mg";
            else if(selectedWeightUnitFrom.equals("Grams") && selectedWeightUnitTo.equals("Kilograms"))
                resultValue = Double.toString(enterValue * 1000000) + " kg";
            else if(selectedWeightUnitFrom.equals("Grams") && selectedWeightUnitTo.equals("Pounds"))
                resultValue = Double.toString(enterValue / 453.59237) + " lb";
            else if(selectedWeightUnitFrom.equals("Kilograms") && selectedWeightUnitTo.equals("Milligrams"))
                resultValue = Double.toString(enterValue * 1000000) + " mg";
            else if(selectedWeightUnitFrom.equals("Kilograms") && selectedWeightUnitTo.equals("Grams"))
                resultValue = Double.toString(enterValue * 1000) + " g";
            else if(selectedWeightUnitFrom.equals("Kilograms") && selectedWeightUnitTo.equals("Pounds"))
                resultValue = Double.toString(enterValue / 0.45359237) + " lb";
            else if(selectedWeightUnitFrom.equals("Pounds") && selectedWeightUnitTo.equals("Milligrams"))
                resultValue = Double.toString(enterValue / 0.0000022046) + " mg";
            else if(selectedWeightUnitFrom.equals("Pounds") && selectedWeightUnitTo.equals("Grams"))
                resultValue = Double.toString(enterValue * 453.59237) + " g";
            else if(selectedWeightUnitFrom.equals("Pounds") && selectedWeightUnitTo.equals("Kilograms"))
                resultValue = Double.toString(enterValue * 0.45359237) + " kg";
            else
                resultValue = Double.toString(enterValue);

            textViewResult.setText(resultValue);
            textViewResult.setVisibility(View.VISIBLE);
        }
    }
}
