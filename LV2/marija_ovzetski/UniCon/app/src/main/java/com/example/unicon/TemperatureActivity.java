package com.example.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class TemperatureActivity extends AppCompatActivity implements View.OnClickListener{
    Spinner spinnerTemperatureScaleFrom;
    Spinner spinnerTemperatureScaleTo;
    TextView textViewResult;
    EditText editTextEnterValue;
    Button buttonConvert;
    String selectedTempScaleFrom, selectedTempScaleTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);
        setUpUI();
    }

    public void setUpUI() {
        this.spinnerTemperatureScaleFrom = (Spinner) findViewById(R.id.spinnerTemperatureScaleFrom);
        this.spinnerTemperatureScaleTo = (Spinner) findViewById(R.id.spinnerTemperatureScaleTo);
        this.editTextEnterValue = (EditText) findViewById(R.id.editTextEnterValue);
        this.textViewResult = (TextView) findViewById(R.id.textViewResult);
        this.buttonConvert = (Button) findViewById(R.id.buttonConvert);

        ArrayAdapter<CharSequence> temperatureScaleAdapterFrom = ArrayAdapter.createFromResource(this, R.array.tempScalesFrom, android.R.layout.simple_spinner_item);
        temperatureScaleAdapterFrom.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        ArrayAdapter<CharSequence> temperatureScaleAdapterTo = ArrayAdapter.createFromResource(this, R.array.tempScalesTo, android.R.layout.simple_spinner_item);
        temperatureScaleAdapterTo.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        this.spinnerTemperatureScaleFrom.setAdapter(temperatureScaleAdapterFrom);
        this.spinnerTemperatureScaleTo.setAdapter(temperatureScaleAdapterTo);

        this.spinnerTemperatureScaleFrom.setOnItemSelectedListener(new TemperatureScaleFrom());
        this.spinnerTemperatureScaleTo.setOnItemSelectedListener(new TemperatureScaleTo());
        this.buttonConvert.setOnClickListener(this);

    }

    class TemperatureScaleFrom implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            selectedTempScaleFrom = parent.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    class TemperatureScaleTo implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            selectedTempScaleTo = parent.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }


    @Override
    public void onClick(View v) {
        if(!this.editTextEnterValue.getText().toString().isEmpty()) {
            String resultValue = "0";
            double enterValue = Double.parseDouble(this.editTextEnterValue.getText().toString());

            if(selectedTempScaleFrom.equals("Celsius") && selectedTempScaleTo.equals("Fahrenheit"))
                resultValue = Double.toString(enterValue * 1.8 + 32) + " °F";
            else if(selectedTempScaleFrom.equals("Celsius") && selectedTempScaleTo.equals("Kelvin"))
                resultValue = Double.toString(enterValue + 273.15) + " °K";
            else if(selectedTempScaleFrom.equals("Fahrenheit") && selectedTempScaleTo.equals("Celsius"))
                resultValue = Double.toString((enterValue - 32)/1.8) + " °C";
            else if(selectedTempScaleFrom.equals("Fahrenheit") && selectedTempScaleTo.equals("Kelvin"))
                resultValue = Double.toString((enterValue - 32)/1.8 + 273.15) + " °K";
            else if(selectedTempScaleFrom.equals("Kelvin") && selectedTempScaleTo.equals("Celsius"))
                resultValue = Double.toString(enterValue - 273.15) + " °C";
            else if(selectedTempScaleFrom.equals("Kelvin") && selectedTempScaleTo.equals("Fahrenheit"))
                resultValue = Double.toString((enterValue - 273.15) * 1.8 + 32) + " °F";
            else
                resultValue = Double.toString(enterValue);

            textViewResult.setText(resultValue);
            textViewResult.setVisibility(View.VISIBLE);
        }
    }
}
