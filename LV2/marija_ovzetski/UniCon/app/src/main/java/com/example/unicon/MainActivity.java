package com.example.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView imageButtonTemperatureConverter;
    ImageView imageButtonDistanceConverter;
    ImageView imageButtonWeightConverter;
    ImageView imageButtonStrengthConverter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setUpUI();
    }
    public void setUpUI() {
        this.imageButtonTemperatureConverter = (ImageView) findViewById(R.id.imageButtonTemperatureConverter);
        this.imageButtonDistanceConverter = (ImageView) findViewById(R.id.imageButtonDistanceConverter);
        this.imageButtonWeightConverter = (ImageView) findViewById(R.id.imageButtonWeightConverter);
        this.imageButtonStrengthConverter = (ImageView) findViewById(R.id.imageButtonStrengthConverter);

        this.imageButtonTemperatureConverter.setOnClickListener(this);
        this.imageButtonDistanceConverter.setOnClickListener(this);
        this.imageButtonWeightConverter.setOnClickListener(this);
        this.imageButtonStrengthConverter.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case(R.id.imageButtonTemperatureConverter): {
                Intent startTemperatureActivity = new Intent();
                startTemperatureActivity.setClass(getApplicationContext(), TemperatureActivity.class);
                this.startActivity(startTemperatureActivity);
                break;
            }
            case(R.id.imageButtonDistanceConverter): {
                Intent startDistanceActivity = new Intent();
                startDistanceActivity.setClass(getApplicationContext(), DistanceActivity.class);
                this.startActivity(startDistanceActivity);
                break;
            }
            case(R.id.imageButtonWeightConverter): {
                Intent startWeightActivity = new Intent();
                startWeightActivity.setClass(getApplicationContext(), WeightActivity.class);
                this.startActivity(startWeightActivity);
                break;
            }
            case(R.id.imageButtonStrengthConverter): {
                Intent startPowerActivity = new Intent();
                startPowerActivity.setClass(getApplicationContext(), PowerActivity.class);
                this.startActivity(startPowerActivity);
                break;
            }
        }
    }
}
