package com.example.pretvarac;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class
MainActivity extends AppCompatActivity {

    Button temperature,distance,cubic,speed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        temperature = findViewById(R.id.temperature);
        distance = findViewById(R.id.distance);
        cubic = findViewById(R.id.cubic);
        speed = findViewById(R.id.speed);
    }

    public void movepage1(View v){
        Intent intent = new Intent(MainActivity.this, TemperatureActivity.class);
        startActivity(intent);
    }

    public void movepage2(View v){
        Intent intent = new Intent(MainActivity.this, DistanceActivity.class);
        startActivity(intent);
    }
    public void movepage3(View v){
        Intent intent = new Intent(MainActivity.this, CubicActivity.class);
        startActivity(intent);
    }
    public void movepage4(View v){
        Intent intent = new Intent(MainActivity.this, SpeedActivity.class);
        startActivity(intent);
    }
}
