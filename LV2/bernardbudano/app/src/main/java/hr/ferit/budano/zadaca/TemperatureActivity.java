package hr.ferit.budano.zadaca;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TemperatureActivity extends AppCompatActivity {

    private EditText celsiusInput;
    private Button btnConvertTemperature;

    private Intent explicitIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);
        setTitle("Temperature conversion");
        findViewsById();
        convert();
    }

    private void findViewsById(){
        // EditText
        this.celsiusInput = findViewById(R.id.celsiusInput);

        // Button
        this.btnConvertTemperature = findViewById(R.id.btnConvertTemperature);
    }

    private void convert(){
        this.btnConvertTemperature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double celsius = Double.parseDouble(celsiusInput.getText().toString());
                Double kelvin = celsius + 273.15;

                explicitIntent = new Intent(getApplicationContext(), ResultActivity.class);
                explicitIntent.putExtra("txtResult", String.valueOf(kelvin));
                explicitIntent.putExtra("txtUnit", "Kelvin");
                startActivity(explicitIntent);
            }
        });
    }
}
