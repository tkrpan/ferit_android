package hr.ferit.budano.zadaca;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MassActivity extends AppCompatActivity {

    private EditText kilogramsInput;
    private Button btnConvertMass;

    private Intent explicitIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mass);
        setTitle("Mass conversion");
        findViewsById();
        convert();
    }

    private void findViewsById(){
        // EditText
        this.kilogramsInput = findViewById(R.id.kilogramsInput);

        // Button
        this.btnConvertMass = findViewById(R.id.btnConvertMass);
    }

    private void convert(){
        this.btnConvertMass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double kilograms = Double.parseDouble(kilogramsInput.getText().toString());
                Double pounds = kilograms * 2.20462262;

                explicitIntent = new Intent(getApplicationContext(), ResultActivity.class);
                explicitIntent.putExtra("txtResult", String.valueOf(pounds));
                explicitIntent.putExtra("txtUnit", "pounds");
                startActivity(explicitIntent);
                //poundsResult.setText(String.valueOf(pounds));
            }
        });
    }
}
