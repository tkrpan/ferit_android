package hr.ferit.budano.zadaca;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnLength;
    private Button btnTime;
    private Button btnMass;
    private Button btnTemperature;

    private Intent explicitIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Main Menu");
        findViewsById();
        setOnClickListeners();
    }

    private void findViewsById(){
        btnLength = findViewById(R.id.length);
        btnTime = findViewById(R.id.time);
        btnMass = findViewById(R.id.mass);
        btnTemperature = findViewById(R.id.temperature);
    }

    private void setOnClickListeners(){
        // set on click listener on btnLength
        btnLength.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        explicitIntent = new Intent(getApplicationContext(), LengthActivity.class);
                        startActivity(explicitIntent);
                    }
                }
        );

        // set on click listener on btnTime
        btnTime.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        explicitIntent = new Intent(getApplicationContext(), TimeActivity.class);
                        startActivity(explicitIntent);
                    }
                }
        );

        // set on click listener on btnMass
        btnMass.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        explicitIntent = new Intent(getApplicationContext(), MassActivity.class);
                        startActivity(explicitIntent);
                    }
                }
        );

        // set on click listener on btnTemperature
        btnTemperature.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        explicitIntent = new Intent(getApplicationContext(), TemperatureActivity.class);
                        startActivity(explicitIntent);
                    }
                }
        );
    }


}
