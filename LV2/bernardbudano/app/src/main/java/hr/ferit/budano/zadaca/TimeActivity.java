package hr.ferit.budano.zadaca;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TimeActivity extends AppCompatActivity {

    private EditText daysInput;
    private Button btnConvertTime;

    private Intent explicitIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);
        setTitle("Time conversion");
        findViewsById();
        convert();
    }

    private void findViewsById(){
        // EditText
        this.daysInput = findViewById(R.id.daysInput);

        // Button
        this.btnConvertTime = findViewById(R.id.btnConvertTime);
    }

    private void convert(){
        this.btnConvertTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double days = Double.parseDouble(daysInput.getText().toString());
                Double minutes = days * 24 * 60;

                explicitIntent = new Intent(getApplicationContext(), ResultActivity.class);
                explicitIntent.putExtra("txtResult", String.valueOf(minutes));
                explicitIntent.putExtra("txtUnit", "minutes");
                startActivity(explicitIntent);
            }
        });
    }
}
