package hr.ferit.budano.zadaca;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LengthActivity extends AppCompatActivity {

    private EditText centimetersInput;
    private Button btnConvertLength;

    private Intent explicitIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_length);
        setTitle("Length conversion");
        findViewsById();
        convert();
    }

    private void findViewsById(){
        // EditText
        this.centimetersInput = findViewById(R.id.centimetersInput);

        // Button
        this.btnConvertLength = findViewById(R.id.btnConvertLength);
    }

    private void convert(){
        this.btnConvertLength.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double centimeters = Double.parseDouble(centimetersInput.getText().toString());
                Double inches = centimeters * 0.393700787;

                explicitIntent = new Intent(getApplicationContext(), ResultActivity.class);
                explicitIntent.putExtra("txtResult", String.valueOf(inches));
                explicitIntent.putExtra("txtUnit", "inches");
                startActivity(explicitIntent);
            }
        });
    }

}
