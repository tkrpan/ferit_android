package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SpeedActivity extends AppCompatActivity {

    private Button btnCalc;
    private EditText etKmh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed);
        setTitle("Calculate speed");
        setView();
    }

    private void setView(){

        btnCalc = findViewById(R.id.Calc);
        etKmh = findViewById(R.id.Kmh);

        btnCalc.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view){
                        double kmhBroj = Double.parseDouble(etKmh.getText().toString());
                        String rezultat = Double.toString(getMilesPerHour(kmhBroj));
                        Intent rezultatIntent = new Intent(getApplicationContext(), ResultActivity.class);
                        rezultatIntent.putExtra("rezultat", rezultat);
                        rezultatIntent.putExtra("name","Miles per hour");
                        startActivity(rezultatIntent);
                    }
                }
        );
    }

    private double getMilesPerHour(double broj){
        return broj/1.609;
    }
}
