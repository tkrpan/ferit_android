package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TemperatureActivity extends AppCompatActivity {

    private Button btnCalc;
    private EditText etCelz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);
        setTitle("Calculate temperature");
        setView();
    }

    private void setView(){

        btnCalc = findViewById(R.id.Calc);
        etCelz = findViewById(R.id.TempCelz);

        btnCalc.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view){
                        double celzijeviBroj = Double.parseDouble(etCelz.getText().toString());
                        String rezultat = Double.toString(getFarenheit(celzijeviBroj));
                        Intent rezultatIntent = new Intent(getApplicationContext(), ResultActivity.class);
                        rezultatIntent.putExtra("rezultat", rezultat);
                        rezultatIntent.putExtra("name","Farenheit");
                        startActivity(rezultatIntent);
                    }
                }
        );
    }
    private double getFarenheit(double number){
        return (number*((double)9/5))+32;
    }
}
