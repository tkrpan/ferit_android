package com.example.converter;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MassActivity extends AppCompatActivity {

    private Button btnCalc;
    private EditText etKilogrami;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mass);
        setTitle("Calculate mass");
        setView();
    }

    private void setView() {

        btnCalc = findViewById(R.id.Calc);
        etKilogrami = findViewById(R.id.Kg);

        btnCalc.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view){
                        double kilogramiBroj = Double.parseDouble(etKilogrami.getText().toString());
                        String rezultat = Double.toString(getFunte(kilogramiBroj));
                        Intent rezultatIntent = new Intent(getApplicationContext(), ResultActivity.class);
                        rezultatIntent.putExtra("rezultat", rezultat);
                        rezultatIntent.putExtra("name","Mass");
                        startActivity(rezultatIntent);
                    }
                }
        );
    }
    private double getFunte(double broj){
        return broj*2.205;
    }
}
