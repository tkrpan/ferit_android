package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class VolActivity extends AppCompatActivity {

    public static final String DIST_VALUE = "DIST_VALUE";

    private Button button;
    private EditText editText;
    private Spinner spin;


    String[] distanceValue={"M^3 to DM^3","DM^3 to M^3"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dist);

        button = findViewById(R.id.button);
        editText = findViewById(R.id.editText);

        spin = findViewById(R.id.spinner);

        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, distanceValue);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapter);

        final int[] opt = {0};
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent, View view, int position, long id) {
                opt[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });


        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String val = editText.getText().toString();
                double value = Double.parseDouble(val);
                double mToDm, dmToM;
                String result;


                if(opt[0] == 0){
                    mToDm = value * 1000;
                    result = String.valueOf(mToDm);
                    Intent intent = new Intent(VolActivity.this, DistanceCalcActivity.class);
                    intent.putExtra(DIST_VALUE, result);
                    startActivity(intent);
                }else if(opt[0] == 1){
                    dmToM = value / 1000;
                    result = String.valueOf(dmToM);
                    Intent intent = new Intent(VolActivity.this, DistanceCalcActivity.class);
                    intent.putExtra(DIST_VALUE, result);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),"Something went wrong!", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

}