package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonTemp, buttonVol, buttonDist, buttonSpeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setView();
    }

    private void setView(){

        buttonTemp = findViewById(R.id.buttonTemp);
        buttonDist = findViewById(R.id.buttonDist);
        buttonVol = findViewById(R.id.buttonVol);
        buttonSpeed = findViewById(R.id.buttonSpeed);

        buttonTemp.setOnClickListener(this);
        buttonDist.setOnClickListener(this);
        buttonVol.setOnClickListener(this);
        buttonSpeed.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        Intent intent;

        switch(view.getId()){

            case R.id.buttonTemp:
                intent = new Intent(this, TempActivity.class);
                startActivity(intent);
                break;

            case R.id.buttonDist:
                intent = new Intent(this, DistActivity.class);
                startActivity(intent);
                break;

            case R.id.buttonVol:
                intent = new Intent(this, VolActivity.class);
                startActivity(intent);
                break;

            case R.id.buttonSpeed:
                intent = new Intent(this, SpeedActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
