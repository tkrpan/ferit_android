package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class DistanceCalcActivity extends AppCompatActivity {

    private TextView textViewValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance_calc);

        String value = getDataFromIntent();

        setView(value);
    }

    private String getDataFromIntent() {
        Intent intent = getIntent();
        return intent.getStringExtra("DIST_VALUE");
    }

    private void setView(String value) {

        textViewValue = findViewById(R.id.textViewValue);

        textViewValue.setText(value);
    }

}
