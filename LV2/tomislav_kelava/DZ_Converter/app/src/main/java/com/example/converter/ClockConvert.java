package com.example.converter;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
public class ClockConvert extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.clock_activity);
        ArrayAdapter<CharSequence> adapter =ArrayAdapter.createFromResource(this, R.array.clock_array,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spin = findViewById(R.id.spinner);
        spin.setAdapter(adapter);

        Button btnConvert = findViewById(R.id.btnConvert);
        final EditText txtEntry = findViewById(R.id.txtEntry);

        btnConvert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Code for conversion here.
            }
        });

        final int[] pos = {0};
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent, View view, int position, long id) {
                pos[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        final TextView result = findViewById(R.id.result);



        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                if (txtEntry.getText().toString().length() > 0){
                    String textValue = txtEntry.getText().toString();
                    double lastValue = Double.parseDouble(textValue);
                    double min_to_h, h_to_min;

                    if(pos[0] ==0){
                        min_to_h = lastValue / 60;
                        result.setText("Hours: " + min_to_h );

                    }else if(pos[0] ==1){
                        h_to_min = lastValue * 60;
                        result.setText("Minutes: " + h_to_min);
                    }
                }  else{
                    result.setText("No input! Please input number.");
                }

            }
        });
    }
}
