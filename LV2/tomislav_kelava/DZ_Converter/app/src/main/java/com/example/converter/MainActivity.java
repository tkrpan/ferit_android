package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button clock,currency,speed,temperature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        clock = findViewById(R.id.clock);
        currency = findViewById(R.id.currency);
        speed = findViewById(R.id.speed);
        temperature = findViewById(R.id.temperature);
    }

    public void movepage1(View v){
        Intent intent = new Intent(MainActivity.this, ClockConvert.class);
        startActivity(intent);
    }

    public void movepage2(View v){
        Intent intent = new Intent(MainActivity.this, CurrencyConvert.class);
        startActivity(intent);
    }
    public void movepage3(View v){
        Intent intent = new Intent(MainActivity.this, SpeedConvert.class);
        startActivity(intent);
    }
    public void movepage4(View v){
        Intent intent = new Intent(MainActivity.this, TemperatureConvert.class);
        startActivity(intent);
    }
}

