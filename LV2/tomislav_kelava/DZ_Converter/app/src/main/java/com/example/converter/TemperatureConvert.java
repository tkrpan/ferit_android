package com.example.converter;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
public class TemperatureConvert extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.temperature_activity);
        ArrayAdapter<CharSequence> adapter =ArrayAdapter.createFromResource(this, R.array.temperature_array,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spin = findViewById(R.id.spinner);
        spin.setAdapter(adapter);

        Button btnConvert = findViewById(R.id.btnConvert);
        final EditText txtEntry = findViewById(R.id.txtEntry);

        btnConvert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Code for conversion here.
            }
        });

        final int[] pos = {0};
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent, View view, int position, long id) {
                pos[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        final TextView result = findViewById(R.id.result);



        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                if (txtEntry.getText().toString().length() > 0){
                    String textValue = txtEntry.getText().toString();
                    double lastValue = Double.parseDouble(textValue);
                    double c_to_f, f_to_c;

                    if(pos[0] ==0){
                        f_to_c = (lastValue/5)*9+32;
                        result.setText("Fahrenheit: " + f_to_c);

                    }else if(pos[0] ==1){
                        c_to_f = (lastValue-32)*5/9;
                        result.setText("Celsius: " + c_to_f);
                    }
                }  else{
                    result.setText("No input! Please input number.");
                }

            }
        });
    }
}
