package com.example.example2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class UdaljenostActivity extends AppCompatActivity {

    private Button btnCalc;
    private EditText etKiloMeters;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_udaljenost);
        setTitle("Preračunavanje udaljenost");
        setView();
    }

    private void setView(){

        btnCalc = findViewById(R.id.Calc);
        etKiloMeters = findViewById(R.id.Kilometri);


        btnCalc.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view){
                        double kilometriBroj = Double.parseDouble(etKiloMeters.getText().toString());
                        String rezultat = Double.toString(getMiles(kilometriBroj));
                        Intent rezultatIntent = new Intent(getApplicationContext(), RezultatActivity.class);
                        rezultatIntent.putExtra("rezultat", rezultat);
                        rezultatIntent.putExtra("name","Milje");
                        startActivity(rezultatIntent);


                    }
                }
        );

    }

    private double getMiles(double broj){
        return broj/1.609;
    }

}
