package com.example.unitconverter;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class ResultActivity extends AppCompatActivity{
    private TextView txtResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        findViewsById();

        String result = getIntent().getStringExtra("txtResult");


        txtResult.setText(result + " ");
    }

    private void findViewsById(){
        // TextView
        this.txtResult = findViewById(R.id.txtResult);
    }
}
