package com.example.unitconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;


public class MainActivity extends AppCompatActivity {

    Button temperature,distance,weight,area;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        temperature = findViewById(R.id.temperature);
        distance = findViewById(R.id.distance);
        area = findViewById(R.id.area);
        weight = findViewById(R.id.weight);
    }

    public void temperaturePage(View v){
        Intent intent = new Intent(MainActivity.this, TemperatureActivity.class);
        startActivity(intent);
    }

    public void distancePage(View v){
        Intent intent = new Intent(MainActivity.this, DistanceActivity.class);
        startActivity(intent);
    }
    public void weightPage(View v){
        Intent intent = new Intent(MainActivity.this, WeightActivity.class);
        startActivity(intent);
    }
    public void areaPage(View v){
        Intent intent = new Intent(MainActivity.this, AreaActivity.class);
        startActivity(intent);
    }

}
