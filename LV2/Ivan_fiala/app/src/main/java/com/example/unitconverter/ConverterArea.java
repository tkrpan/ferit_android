package com.example.unitconverter;

public class ConverterArea {


    public enum Unit {
        HECTARE,
        ACRE,
        SQUAREMETER,
        SQUAREKILOMETER,
        SQUAREYARD,
        SQUAREMILE,
        SQUAREINCH,
        SQUARECENTIMETER,
        SQUAREFOOT,
        SQUAREMILLIMETER;


        // Helper method to convert text to one of the above constants
        public static Unit fromString(String text) {
            if (text != null) {
                for (Unit unit : Unit.values()) {
                    if (text.equalsIgnoreCase(unit.toString())) {
                        return unit;
                    }
                }
            }

            throw new IllegalArgumentException("Cannot find a value for " + text);
        }
    }


    // What can I multiply by to get me from my fromUnit to my toUnit?
    private final double multiplier;

    public ConverterArea(Unit from, Unit to) {
        double constant = 1;
        // Set the multiplier, else if fromUnit = toUnit, then it is 1
        switch (from) {
            case SQUAREINCH:
                if (to == Unit.SQUARECENTIMETER) {
                    constant = 6.4516;
                } else if (to == Unit.SQUAREFOOT) {
                    constant = 0.0069444444;
                } else if (to == Unit.SQUAREYARD) {
                    constant = 0.0007716049;
                } else if (to == Unit.SQUAREMETER) {
                    constant = 0.00064516;
                } else if (to == Unit.SQUAREMILE) {
                    constant = 2.490974868e-10;
                } else if (to == Unit.SQUAREKILOMETER) {
                    constant = 6.4516e-10;
                } else if (to == Unit.ACRE){
                    constant = 1.594225079e-7;
                } else if (to == Unit.HECTARE) {
                    constant = 6.4516e-8;
                } else if (to == Unit.SQUAREMILLIMETER){
                    constant = 645.16;
                }
                break;
            case SQUARECENTIMETER:
                if (to == Unit.SQUAREINCH) {
                    constant = 0.15500031;
                } else if (to == Unit.SQUAREFOOT) {
                    constant = 0.001076391;
                } else if (to == Unit.SQUAREYARD) {
                    constant = 0.000119599;
                } else if (to == Unit.SQUAREMETER) {
                    constant = 0.0001;
                } else if (to == Unit.SQUAREMILE) {
                    constant = 3.861018768e-11;
                } else if (to == Unit.SQUAREKILOMETER) {
                    constant = 1.e-10;
                } else if (to == Unit.ACRE){
                    constant = 2.471053814e-8;
                } else if (to == Unit.HECTARE) {
                    constant = 1.e-8;
                } else if (to == Unit.SQUAREMILLIMETER){
                    constant = 100;
                }
                break;
            case SQUAREFOOT:
                if (to == Unit.SQUAREINCH) {
                    constant = 144;
                } else if (to == Unit.SQUARECENTIMETER) {
                    constant = 929.0304;
                } else if (to == Unit.SQUAREYARD) {
                    constant = 0.1111111111;
                } else if (to == Unit.SQUAREMETER) {
                    constant = 0.09290304;
                } else if (to == Unit.SQUAREMILE) {
                    constant = 3.58700381e-8;
                } else if (to == Unit.SQUAREKILOMETER) {
                    constant = 9.290304E-8;
                } else if (to == Unit.ACRE){
                    constant = 0.0000229568;
                } else if (to == Unit.HECTARE) {
                    constant = 0.0000092903;
                } else if (to == Unit.SQUAREMILLIMETER){
                    constant = 92903.04;
                }
                break;
            case SQUAREYARD:
                if (to == Unit.SQUAREINCH) {
                    constant = 1296;
                } else if (to == Unit.SQUARECENTIMETER) {
                    constant = 8361.2736;
                } else if (to == Unit.SQUAREFOOT) {
                    constant = 9;
                } else if (to == Unit.SQUAREMETER) {
                    constant = 0.83612736;
                } else if (to == Unit.SQUAREMILE) {
                    constant = 3.228303429E-7;
                } else if (to == Unit.SQUAREKILOMETER) {
                    constant = 8.3612736E-7;
                } else if (to == Unit.ACRE){
                    constant = 0.0002066116;
                } else if (to == Unit.HECTARE) {
                    constant = 0.0000836127;
                } else if (to == Unit.SQUAREMILLIMETER){
                    constant = 836127.36;
                }
                break;
            case SQUAREMETER:
                if (to == Unit.SQUAREINCH) {
                    constant = 1550.0031;
                } else if (to == Unit.SQUARECENTIMETER) {
                    constant = 10000;
                } else if (to == Unit.SQUAREFOOT) {
                    constant = 10.763910417;
                } else if (to == Unit.SQUAREYARD) {
                    constant = 1.1959900463;
                } else if (to == Unit.SQUAREMILE) {
                    constant = 3.861018768E-7;
                } else if (to == Unit.SQUAREKILOMETER) {
                    constant = 0.000001;
                } else if (to == Unit.ACRE){
                    constant = 0.0002471054;
                } else if (to == Unit.HECTARE) {
                    constant = 0.0001;
                } else if (to == Unit.SQUAREMILLIMETER){
                    constant = 1000000;
                }
                break;
            case SQUAREMILE:
                if (to == Unit.SQUAREINCH) {
                    constant = 4014492529L;
                } else if (to == Unit.SQUARECENTIMETER) {
                    constant = 25899900000L;
                } else if (to == Unit.SQUAREFOOT) {
                    constant = 27878420.34;
                } else if (to == Unit.SQUAREYARD) {
                    constant = 3097602.26;
                } else if (to == Unit.SQUAREMETER) {
                    constant = 2589990;
                } else if (to == Unit.SQUAREKILOMETER) {
                    constant = 2.58999;
                } else if (to == Unit.ACRE){
                    constant = 640.00046695;
                } else if (to == Unit.HECTARE) {
                    constant = 258.999;
                } else if (to == Unit.SQUAREMILLIMETER){
                    constant = 2589990000000L;
                }
                break;
            case SQUAREKILOMETER:
                if (to == Unit.SQUAREINCH) {
                    constant = 1550003100;
                } else if (to == Unit.SQUARECENTIMETER) {
                    constant = 10000000000L;
                } else if (to == Unit.SQUAREFOOT) {
                    constant = 10763910.417;
                } else if (to == Unit.SQUAREYARD) {
                    constant = 1195990.0463;
                } else if (to == Unit.SQUAREMETER) {
                    constant = 1000000;
                } else if (to == Unit.SQUAREMILE) {
                    constant = 0.3861018768;
                } else if (to == Unit.ACRE){
                    constant = 247.10538147;
                } else if (to == Unit.HECTARE) {
                    constant = 100;
                } else if (to == Unit.SQUAREMILLIMETER){
                    constant = 1000000000000L;
                }
                break;
            case ACRE:
                if (to == Unit.SQUAREINCH) {
                    constant = 6272640;
                } else if (to == Unit.SQUARECENTIMETER) {
                    constant = 40468564.224;
                } else if (to == Unit.SQUAREFOOT) {
                    constant = 43560;
                } else if (to == Unit.SQUAREYARD) {
                    constant = 4840;
                } else if (to == Unit.SQUAREMETER) {
                    constant = 4046.8564224;
                } else if (to == Unit.SQUAREMILE) {
                    constant = 0.0015624989;
                } else if (to == Unit.SQUAREKILOMETER){
                    constant = 0.0040468564;
                } else if (to == Unit.HECTARE) {
                    constant = 0.4046856422;
                } else if (to == Unit.SQUAREMILLIMETER){
                    constant = 4046856422.4;
                }
                break;

            case HECTARE:
                if (to == Unit.SQUAREINCH) {
                    constant = 15500031;
                } else if (to == Unit.SQUARECENTIMETER) {
                    constant = 100000000;
                } else if (to == Unit.SQUAREFOOT) {
                    constant = 107639.10417;
                } else if (to == Unit.SQUAREYARD) {
                    constant = 11959.900463;
                } else if (to == Unit.SQUAREMETER) {
                    constant = 10000;
                } else if (to == Unit.SQUAREMILE) {
                    constant = 0.0038610188;
                } else if (to == Unit.SQUAREKILOMETER){
                    constant = 0.01;
                } else if (to == Unit.ACRE) {
                    constant = 2.4710538147;
                } else if (to == Unit.SQUAREMILLIMETER){
                    constant = 10000000000L;
                }
                break;

            case SQUAREMILLIMETER:
                if (to == Unit.SQUAREINCH) {
                    constant = 0.0015500031;
                } else if (to == Unit.SQUARECENTIMETER) {
                    constant = 0.01;
                } else if (to == Unit.SQUAREFOOT) {
                    constant = 0.0000107639;
                } else if (to == Unit.SQUAREYARD) {
                    constant = 0.000001196;
                } else if (to == Unit.SQUAREMETER) {
                    constant = 0.000001;
                } else if (to == Unit.SQUAREMILE) {
                    constant = 3.861018768E-13;
                } else if (to == Unit.SQUAREKILOMETER){
                    constant = 1.E-12;
                } else if (to == Unit.ACRE) {
                    constant = 2.471053814E-10;
                } else if (to == Unit.HECTARE){
                    constant = 9.999999999E-11;
                }
                break;
        }

        multiplier = constant;
    }

    // Convert the unit!
    public double convert(double input) {
        return input * multiplier;
    }

}