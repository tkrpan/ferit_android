package com.example.unitconverter;

public class ConverterTemperature {

    public enum Unit {
        CELSIUS,
        KELVIN,
        FAHRENHEIT;

        // Helper method to convert text to one of the above constants
        public static Unit fromString(String text) {
            if (text != null) {
                for (Unit unit : Unit.values()) {
                    if (text.equalsIgnoreCase(unit.toString())) {
                        return unit;
                    }
                }
            }

            throw new IllegalArgumentException("Cannot find a value for " + text);
        }
    }


    // What can I multiply by to get me from my fromUnit to my toUnit?
    private final double multiplier;
    private final double term;

    public ConverterTemperature(Unit from, Unit to) {
        double constant = 1;
        double addends = 0;
        // Set the multiplier, else if fromUnit = toUnit, then it is 1
        switch (from) {
            case KELVIN:
                if (to == Unit.CELSIUS) {
                    constant = 1;
                    addends = -273.15;
                } else if (to == Unit.FAHRENHEIT) {
                    constant = 1.8;
                    addends = -459.67;

                }
                break;
            case CELSIUS:
                if (to == Unit.KELVIN) {
                    constant = 1;
                    addends = 273.15;
                } else if (to == Unit.FAHRENHEIT) {
                    constant = 1.8;
                    addends = 32;
                }
                break;
            case FAHRENHEIT:
                if (to == Unit.CELSIUS) {
                    constant = 0.5555555556;
                    addends = -32;

                } else if (to == Unit.KELVIN) {
                    constant = 0.5555555556;
                    addends = 459.67;
                }
                break;

        }

        multiplier = constant;
        term = addends;
    }

    // Convert the unit!
    public double convert(double input, Unit from) {
        if (from == Unit.FAHRENHEIT){
            return  (input + term)*multiplier;
        }
        else {
            return input * multiplier + (term);
        }
    }

}
