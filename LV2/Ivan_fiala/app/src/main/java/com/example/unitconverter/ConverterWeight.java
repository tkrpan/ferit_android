package com.example.unitconverter;

public class ConverterWeight {

    public enum Unit {
        OUNCES,
        POUNDS,
        KILOGRAMS,
        GRAMS,
        DALTON,
        TONNE;

        // Helper method to convert text to one of the above constants
        public static Unit fromString(String text) {
            if (text != null) {
                for (Unit unit : Unit.values()) {
                    if (text.equalsIgnoreCase(unit.toString())) {
                        return unit;
                    }
                }
            }

            throw new IllegalArgumentException("Cannot find a value for " + text);
        }
    }


    // What can I multiply by to get me from my fromUnit to my toUnit?
    private final double multiplier;

    public ConverterWeight(Unit from, Unit to) {
        double constant = 1;
        // Set the multiplier, else if fromUnit = toUnit, then it is 1
        switch (from) {
            case OUNCES:
                if (to == Unit.POUNDS) {
                    constant = 0.0625;
                } else if (to == Unit.KILOGRAMS) {
                    constant = 0.0283495;
                } else if (to == Unit.GRAMS) {
                    constant = 28.3495;
                } else if (to == Unit.TONNE) {
                    constant = 0.0000283495;
                } else if (to == Unit.DALTON) {
                    constant = 1.70724563E+25;
                }
                break;
            case GRAMS:
                if (to == Unit.OUNCES) {
                    constant = 0.0352739907;
                } else if (to == Unit.POUNDS) {
                    constant = 0.0022046244;
                } else if (to == Unit.KILOGRAMS) {
                    constant = 0.001;
                } else if (to == Unit.TONNE) {
                    constant = 0.000001;
                } else if (to == Unit.DALTON) {
                    constant = 6.022136652E+23;
                }
                break;
            case DALTON:
                if (to == Unit.OUNCES) {
                    constant = 5.85738796E-26;
                } else if (to == Unit.GRAMS) {
                    constant = 1.660540199E-24;
                } else if (to == Unit.POUNDS) {
                    constant = 3.660867475E-27;
                } else if (to == Unit.KILOGRAMS) {
                    constant = 1.660540199E-27;
                } else if (to == Unit.TONNE) {
                    constant = 1.660540199E-30;
                }
                break;
            case POUNDS:
                if (to == Unit.OUNCES) {
                    constant = 16;
                } else if (to == Unit.GRAMS) {
                    constant = 453.592;
                } else if (to == Unit.DALTON) {
                    constant = 32.731593008E+26;
                } else if (to == Unit.KILOGRAMS) {
                    constant = 0.453592;
                } else if (to == Unit.TONNE) {
                    constant = 0.000453592;
                }
                break;
            case KILOGRAMS:
                if (to == Unit.OUNCES) {
                    constant = 35.273990723;
                } else if (to == Unit.GRAMS) {
                    constant = 1000;
                } else if (to == Unit.DALTON) {
                    constant = 6.022136652E+26;
                } else if (to == Unit.POUNDS) {
                    constant = 2.2046244202;
                } else if (to == Unit.TONNE) {
                    constant = 0.001;
                }
                break;
            case TONNE:
                if (to == Unit.OUNCES) {
                    constant = 35273.990723;
                } else if (to == Unit.GRAMS) {
                    constant = 1000000;
                } else if (to == Unit.DALTON) {
                    constant = 6.022136652E+29;
                } else if (to == Unit.POUNDS) {
                    constant = 2204.6244202;
                } else if (to == Unit.KILOGRAMS) {
                    constant = 1000;
                }
                break;
        }
        multiplier = constant;
    }

    // Convert the unit!
    public double convert(double input) {
        return input * multiplier;
    }

}
