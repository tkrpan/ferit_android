package com.example.converter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MassActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mass);
        ArrayAdapter<CharSequence> adapter =ArrayAdapter.createFromResource(this, R.array.mass_array,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spin = findViewById(R.id.spinner);
        spin.setAdapter(adapter);

        Button btnConvert = findViewById(R.id.btnConvert);
        final EditText txtEntry = findViewById(R.id.txtEntry);

        btnConvert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Code for conversion here.
            }
        });

        final int[] pos = {0};
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent, View view, int position, long id) {
                pos[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        final TextView result = findViewById(R.id.result);



        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                if (txtEntry.getText().toString().length() > 0){
                    String textValue = txtEntry.getText().toString();
                    double lastValue = Double.parseDouble(textValue);
                    double kg_to_N, N_to_kg;

                    if(pos[0] ==0){
                        kg_to_N = lastValue * 9.81;
                        Intent intent =  new Intent(MassActivity.this, ResultActivity.class);
                        intent.putExtra("result", kg_to_N);
                        startActivityForResult(intent, 1);

                    }else if(pos[0] ==1){
                        N_to_kg = lastValue * 0.101971621;
                        Intent intent =  new Intent(MassActivity.this, ResultActivity.class);
                        intent.putExtra("result", N_to_kg);
                        startActivityForResult(intent, 1);
                    }
                }  else{
                    Toast.makeText(getApplicationContext(),"Input something!",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
