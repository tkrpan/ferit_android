package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnMasa;
    private Button btnUdaljenost;
    private Button btnBrzina;
    private Button btnTemperatura;
    private Intent explicitIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        btnMasa = findViewById(R.id.masa);
        btnBrzina = findViewById(R.id.brzina);
        btnUdaljenost = findViewById(R.id.udaljenost);
        btnTemperatura = findViewById(R.id.temperatura);

        btnTemperatura.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view){
                        explicitIntent = new Intent(getApplicationContext(), TemperatureActivity.class);
                        startActivity(explicitIntent);
                    }
                }
        );

        btnMasa.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view){
                        explicitIntent = new Intent(getApplicationContext(), MassActivity.class);
                        startActivity(explicitIntent);
                    }
                }
        );

        btnUdaljenost.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view){
                        explicitIntent = new Intent(getApplicationContext(), DistanceActivity.class);
                        startActivity(explicitIntent);
                    }
                }
        );

        btnBrzina.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view){
                        explicitIntent = new Intent(getApplicationContext(), SpeedActivity.class);
                        startActivity(explicitIntent);
                    }
                }
        );
    }
}
