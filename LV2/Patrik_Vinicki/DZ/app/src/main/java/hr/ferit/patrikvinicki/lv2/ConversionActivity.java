package hr.ferit.patrikvinicki.lv2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ConversionActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private static Double RESULT;
    private static Double USER_INPUT;
    private static String CONVERT_FROM;
    private static String CONVERT_TO;
    private static String CONVERSION;

    Bundle bundle;
    Intent intent;
    Spinner spInput;
    Spinner spConvertTo;
    EditText edUserInput;
    TextView title;
    LinearLayout layout;
    Button btnConvert, btnSwitchValues;

    ArrayAdapter<CharSequence> adapterMetric;
    ArrayAdapter<CharSequence> adapterImperial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);
        initializeUI();
    }

    private void initializeUI() {
        bundle = new Bundle();
        bundle = getIntent().getExtras();

        layout          = (LinearLayout) findViewById(R.id.linearLayoutConversion);
        title           = (TextView) findViewById(R.id.tvTitle);
        spInput         = (Spinner) findViewById(R.id.spInputUnit);
        spConvertTo     = (Spinner) findViewById(R.id.spConvertToUnit);
        edUserInput     = (EditText) findViewById(R.id.edInputValue);
        btnConvert      = (Button) findViewById(R.id.btnConvert);
        btnSwitchValues = (Button) findViewById(R.id.btnSwitchValues);

        layout.setBackgroundColor(bundle.getInt("bgColor"));

        title.setText(bundle.getString("title"));

        adapterMetric = ArrayAdapter.createFromResource(this, bundle.getInt("metricSpinnerItems"), android.R.layout.simple_spinner_item);
        adapterMetric.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapterImperial = ArrayAdapter.createFromResource(this, bundle.getInt("imperialSpinnerItems"), android.R.layout.simple_spinner_item);
        adapterImperial.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spInput.setAdapter(adapterMetric);
        spInput.setOnItemSelectedListener(this);

        spConvertTo.setAdapter(adapterImperial);
        spConvertTo.setOnItemSelectedListener(this);

        btnConvert.setOnClickListener(this);
        btnSwitchValues.setOnClickListener(this);

        CONVERSION = bundle.getString("conversion");
        //edUserInput.setOnKeyListener(new View.OnKeyListener() {
        //  @Override
        //  public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        //      if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)){
        //           convert();
        //           return true;
        //       }
        //        return false;
        //     }
        // });
    }

    @Override
    public void onClick(View view) {
        CONVERT_FROM   = spInput.getSelectedItem().toString();
        CONVERT_TO     = spConvertTo.getSelectedItem().toString();

        switch(view.getId()) {

            case R.id.btnConvert:
                USER_INPUT = Double.parseDouble(edUserInput.getText().toString());

                switch (CONVERSION) {
                    case "temperature":
                        intent = new Intent(getApplicationContext(), ShowResultActivity.class);

                        if (CONVERT_TO.equals("Fahrenheit")) {
                            bundle = new Bundle();
                            RESULT = (((USER_INPUT * 2) - (USER_INPUT * (1 / 10))) + 32);
                            RESULT = round(RESULT, 2);
                            bundle.putString("userInput", CONVERT_FROM);
                            bundle.putString("convertTo", CONVERT_TO);
                            bundle.putString("userInputValue", USER_INPUT.toString());
                            bundle.putString("result", RESULT.toString());
                        } else {
                            bundle = new Bundle();
                            RESULT = ((USER_INPUT / 2) + (USER_INPUT * (1/10)) - 32);
                            RESULT = round(RESULT, 2);
                            bundle.putString("userInput", CONVERT_FROM);
                            bundle.putString("convertTo", CONVERT_TO);
                            bundle.putString("userInputValue", USER_INPUT.toString());
                            bundle.putString("result", RESULT.toString());
                        }

                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;

                    case "distance":
                        intent = new Intent(getApplicationContext(), ShowResultActivity.class);
                        bundle = new Bundle();

                        switch(CONVERT_TO) {
                            case "meters":
                                RESULT = USER_INPUT / 3.28;
                                RESULT = round(RESULT, 2);
                                break;

                            case "foot":
                                RESULT = USER_INPUT * 3.28;
                                RESULT = round(RESULT, 2);
                                break;
                        }
                        bundle.putString("userInput", CONVERT_FROM);
                        bundle.putString("convertTo", CONVERT_TO);
                        bundle.putString("userInputValue", USER_INPUT.toString());
                        bundle.putString("result", RESULT.toString());

                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;

                    case "weight":
                        intent = new Intent(getApplicationContext(), ShowResultActivity.class);
                        bundle = new Bundle();

                        switch(CONVERT_TO) {
                            case "lbs":
                                RESULT = (USER_INPUT * 2.2);
                                RESULT = round(RESULT, 2);
                                break;

                            case "kilogram":
                                RESULT = (USER_INPUT / 2.2);
                                RESULT = round(RESULT, 2);
                                break;
                        }

                        bundle.putString("userInput", CONVERT_FROM);
                        bundle.putString("convertTo", CONVERT_TO);
                        bundle.putString("userInputValue", USER_INPUT.toString());
                        bundle.putString("result", RESULT.toString());

                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;


                    case "liquid":
                        intent = new Intent(getApplicationContext(), ShowResultActivity.class);
                        bundle = new Bundle();

                        switch (CONVERT_TO){
                            case "ounce":
                                RESULT = USER_INPUT * 29.57;
                                RESULT = round(RESULT, 2);
                                break;

                            case "milliliter":
                                RESULT = USER_INPUT / 29.57;
                                RESULT = round(RESULT, 2);
                                break;
                        }
                        bundle.putString("userInput", CONVERT_FROM);
                        bundle.putString("convertTo", CONVERT_TO);
                        bundle.putString("userInputValue", USER_INPUT.toString());
                        bundle.putString("result", RESULT.toString());

                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                }

                break;


            case R.id.btnSwitchValues:
                String value;

                value = CONVERT_FROM;
                CONVERT_FROM = CONVERT_TO;
                CONVERT_TO = value;

                if(spInput.getAdapter().equals(adapterMetric)){
                    spInput.setAdapter(adapterImperial);
                    spConvertTo.setAdapter(adapterMetric);
                } else {
                    spInput.setAdapter(adapterMetric);
                    spConvertTo.setAdapter(adapterImperial);
                }

                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
        switch(view.getId()){
            case R.id.spInputUnit:
                CONVERT_FROM = spInput.getSelectedItem().toString();
                break;

            case R.id.spConvertToUnit:
                CONVERT_TO = spConvertTo.getSelectedItem().toString();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
