package hr.ferit.matejkolarevic.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class DistanceActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Button button;
    private EditText editText;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);

        spinner = findViewById(R.id.spinnerDistance);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.distance_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        setView();
    }

    private void setView(){
        this.editText =findViewById(R.id.editText);
        this.button = findViewById(R.id.buttonCalculateDistance);

        this.button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(editText.getText().toString().isEmpty() || editText.getText().toString() == "."){
                    Toast.makeText(getApplicationContext(), "You must enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        Double value = Double.parseDouble(editText.getText().toString());
                        if (spinner.getSelectedItemPosition() == 0) {
                            value *= 2.54;
                        } else {
                            value /= 2.54;
                        }
                        Intent intent = new Intent(DistanceActivity.this,
                                ResultActivity.class);
                        intent.putExtra("value", value);
                        startActivity(intent);
                    } catch (Exception e){
                        Toast.makeText(getApplicationContext(), "Input error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
