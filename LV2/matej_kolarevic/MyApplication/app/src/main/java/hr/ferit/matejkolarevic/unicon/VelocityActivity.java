package hr.ferit.matejkolarevic.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class VelocityActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Button button;
    private EditText inputVelocity;
    private Spinner spinner1, spinner2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_velocity);

        spinner1 = findViewById(R.id.spinnerVelocity1);
        spinner2 = findViewById(R.id.spinnerVelocity2);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.velocity_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner1.setOnItemSelectedListener(this);
        spinner2.setAdapter(adapter);
        spinner2.setOnItemSelectedListener(this);

        setView();
    }

    private void setView(){
        this.inputVelocity = findViewById(R.id.inputVelocity);
        this.button = findViewById(R.id.buttonCalculateVelocity);

        this.button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(inputVelocity.getText().toString().isEmpty() || inputVelocity.getText().toString() == "."){
                    Toast.makeText(getApplicationContext(), "You must enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        Double value = Double.parseDouble(inputVelocity.getText().toString());
                        if (spinner1.getSelectedItemPosition() == 0) {
                            //do nothing
                        } else if (spinner1.getSelectedItemPosition() == 1){
                            value /= 3.6;
                        } else if (spinner1.getSelectedItemPosition() == 2){
                            value *= 0.44704;
                        } else {
                            value /= 1.9438;
                        }
                        if(spinner2.getSelectedItemPosition() == 0) {
                            //do nothing
                        } else if(spinner2.getSelectedItemPosition() == 1) {
                            value *= 3.6;
                        } else if (spinner2.getSelectedItemPosition() == 2){
                            value /= 0.44704;
                        } else {
                            value *= 1.9438;
                        }
                        Intent intent = new Intent(VelocityActivity.this,
                                ResultActivity.class);
                        intent.putExtra("value", value);
                        startActivity(intent);
                    } catch (Exception e){
                        Toast.makeText(getApplicationContext(), "Input error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
