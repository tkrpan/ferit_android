package hr.ferit.matejkolarevic.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class TemperatureActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Button button;
    private EditText inputTemperature;
    private Spinner spinner1, spinner2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);

        spinner1 = findViewById(R.id.spinnerTemp1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.temperature_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner1.setOnItemSelectedListener(this);

        spinner2 = findViewById(R.id.spinnerTemp2);
        spinner2.setAdapter(adapter);
        spinner2.setOnItemSelectedListener(this);

        setView();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void setView(){
        this.inputTemperature =findViewById(R.id.inputTemperature);
        this.button = findViewById(R.id.buttonCalculateTemperature);

        this.button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(inputTemperature.getText().toString().isEmpty() || inputTemperature.getText().toString() == "."){
                    Toast.makeText(getApplicationContext(), "You must enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        boolean isValid = true;
                        Double value = Double.parseDouble(inputTemperature.getText().toString());
                        if (spinner1.getSelectedItemPosition() == 0) {
                            if(Double.parseDouble(inputTemperature.getText().toString()) < -273.14)
                                isValid = false;
                        } else if (spinner1.getSelectedItemPosition() == 1){
                            if(Double.parseDouble(inputTemperature.getText().toString()) < 0) {
                                isValid = false;
                            } else {
                                value -= 273.15;
                            }
                        } else {
                            if(Double.parseDouble(inputTemperature.getText().toString()) < -459.66)
                                isValid = false;
                            else
                                value = (value-32)/1.8;
                        }
                        if (!isValid) {
                            Toast.makeText(getApplicationContext(), "Incorrect temperature value", Toast.LENGTH_SHORT).show();
                        } else {
                            if(spinner2.getSelectedItemPosition() == 0) {
                                //do nothing
                            } else if(spinner2.getSelectedItemPosition() == 1) {
                                value += 273.15;
                            } else {
                                value = value*1.8 + 32;
                            }
                            Intent intent = new Intent(TemperatureActivity.this,
                                    ResultActivity.class);
                            intent.putExtra("value", value);
                            startActivity(intent);
                        }
                    } catch (Exception e){
                        Toast.makeText(getApplicationContext(), "Input error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
