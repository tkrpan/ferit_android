package hr.ferit.matejkolarevic.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class VolumeActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Button button;
    private EditText inputVolume;
    private Spinner spinner1, spinner2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume);

        spinner1 = findViewById(R.id.spinnerVolume1);
        spinner2 = findViewById(R.id.spinnerVolume2);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.volume_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner1.setOnItemSelectedListener(this);
        spinner2.setAdapter(adapter);
        spinner2.setOnItemSelectedListener(this);

        setView();
    }

    private void setView() {
        this.inputVolume = findViewById(R.id.inputVolume);
        this.button = findViewById(R.id.buttonCalculateVolume);

        this.button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(inputVolume.getText().toString().isEmpty() || inputVolume.getText().toString() == "."){
                    Toast.makeText(getApplicationContext(), "You must enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        Double value = Double.parseDouble(inputVolume.getText().toString());
                        if (spinner1.getSelectedItemPosition() == 0) {
                            //do nothing
                        } else if (spinner1.getSelectedItemPosition() == 1){
                            value *= 1000;
                        } else if (spinner1.getSelectedItemPosition() == 2){
                            value *= 10000;
                        } else {
                            value *= 1000000;
                        }
                        if(spinner2.getSelectedItemPosition() == 0) {
                            //do nothing
                        } else if(spinner2.getSelectedItemPosition() == 1) {
                            value /= 1000;
                        } else if (spinner2.getSelectedItemPosition() == 2){
                            value /= 10000;
                        } else {
                            value /= 1000000;
                        }
                        Intent intent = new Intent(VolumeActivity.this,
                                ResultActivity.class);
                        intent.putExtra("value", value);
                        startActivity(intent);
                    } catch (Exception e){
                        Toast.makeText(getApplicationContext(), "Input error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
