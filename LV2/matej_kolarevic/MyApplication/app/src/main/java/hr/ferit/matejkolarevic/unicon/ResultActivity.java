package hr.ferit.matejkolarevic.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    private TextView textView_Result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        setTitle("Result");

        Intent intent = getIntent();

        Double value = intent.getDoubleExtra("value", 0);
        textView_Result = (TextView) findViewById(R.id.textView_Result);

        textView_Result.setText("" + value);
    }
}
