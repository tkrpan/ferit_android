package hr.ferit.matejkolarevic.unicon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button_temp;
    private Button button_volume;
    private Button button_distance;
    private Button button_velocity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();
    }

    private void setView(){
        button_temp = findViewById(R.id.button_temp);
        button_distance = findViewById(R.id.button_distance);
        button_volume = findViewById(R.id.button_volume);
        button_velocity = findViewById(R.id.button_velocity);

        button_temp.setOnClickListener(this);
        button_distance.setOnClickListener(this);
        button_volume.setOnClickListener(this);
        button_velocity.setOnClickListener(this);
    }
    @Override
    public void onClick(View view){
        switch ((view.getId())){
            case R.id.button_temp: {
                Intent intent = new Intent(this, TemperatureActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.button_distance: {
                Intent intent = new Intent(this, DistanceActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.button_volume: {
                Intent intent = new Intent(this, VolumeActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.button_velocity: {
                Intent intent = new Intent(this, VelocityActivity.class);
                startActivity(intent);
                break;
            }
            default:
                break;
        }
    }
}
