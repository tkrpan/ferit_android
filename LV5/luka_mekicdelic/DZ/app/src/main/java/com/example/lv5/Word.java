package com.example.lv5;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

/**
 * A basic class representing an entity that is a row in a one-column database table.
 *
 * @ Entity - You must annotate the class as an entity and supply a table name if not class name.
 * @ PrimaryKey - You must identify the primary key.
 * @ ColumnInfo - You must supply the column name if it is different from the variable name.
 *
 * See the documentation for the full rich set of annotations.
 * https://developer.android.com/topic/libraries/architecture/room.html
 */

@Entity(tableName = "word_table")
public class Word
{
    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    //@ColumnInfo(name = "word") //Specify the name of the column in the table if you want it to be different from the name of the member variable
    private String word;

    public Word(@NonNull String word)
    {
        this.word = word;
    }

    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @NonNull
    public String getWord()
    {
        return this.word;
    }
}
