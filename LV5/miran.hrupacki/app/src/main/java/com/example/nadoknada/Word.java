package com.example.nadoknada;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "word_table")
public class Word {

    public void setId(int id) {
        this.id = id;
    }

    public void setWord(@NonNull String word) {
        this.word = word;
    }

    public void setmWord(String mWord) {
        this.mWord = mWord;
    }

    @PrimaryKey(autoGenerate = true)
     private int id;


     @NonNull
     private String word;

    private String mWord;

    public Word(@NonNull String word) {this.mWord = word;}

    public int getId() {
        return id;
    }

    public String getmWord() {
        return mWord;
    }

    public String getWord(){return this.mWord;}

}
