package com.example.roomwordsample;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.WordViewHolder> {

    class WordViewHolder extends RecyclerView.ViewHolder{
        private final TextView wordItemview;

        private WordViewHolder(View itemView){
            super(itemView);
            wordItemview=itemView.findViewById(R.id.textView);
        }
    }

    private final LayoutInflater mInflatter;
    private List<Word> mWords;

    WordListAdapter(Context context){ mInflatter= LayoutInflater.from(context);
    }
    @Override
    public WordViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView=mInflatter.inflate(R.layout.recyclerview_item,parent,false);
        return new WordViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WordViewHolder holder, int position){
        if(mWords!=null){
            Word current = mWords.get(position);
            holder.wordItemview.setText(current.getWord());
        }else{
            holder.wordItemview.setText("no Word");
        }
    }

    void setWords(List<Word> words){
        mWords=words;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount(){
        if(mWords!=null){
            return mWords.size();
        }
        else return 0;
    }
}
