package com.example.lv5;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;


public class new_word extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_word);
        final View mEditWordView = findViewById(R.id.edit_word);

        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            private final Object EXTRA_REPLY = "";

            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (!TextUtils.isEmpty(mEditWordView.getText())) {
                    String word = mEditWordView.getText().toString();
                    replyIntent.putExtra((String) EXTRA_REPLY, word);
                    setResult(RESULT_OK, replyIntent);
                } else {
                    setResult(RESULT_CANCELED, replyIntent);
                }
                finish();
            }
        });
    }
}
