package com.example.lv5;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
public class MainActivity extends AppCompatActivity {


    public class MainActivity<wordview> extends AppCompatActivity {

        public static final int NEW_WORD_ACTIVITY_REQUEST_CODE = 1;

        private wordview wordview;

       

        public <FloatingActionButton> void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            RecyclerView recyclerView = findViewById(R.id.recyclerview);
            final WordListAdapter adapter = new WordListAdapter(this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            wordview = new ViewModelProvider(this).get(WordViewModel.class);

            wordview.getAllWords().observe(this, new Observer<List<Word>>() {
                @Override
                public void onChanged(@Nullable final List<Word> words) {
                    // Update the cached copy of the words in the adapter.
                    adapter.setWords(words);
                }
            });

            FloatingActionButton fab = findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, NewWordActivity.class);
                    startActivityForResult(intent, NEW_WORD_ACTIVITY_REQUEST_CODE);
                }
            });
        }

        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
                Word word = new Word(data.getStringExtra(NewWordActivity.EXTRA_REPLY));
                wordview.wait(word);
            } else {
                Toast.makeText(
                        getApplicationContext(),
                        R.string.savednt,
                        Toast.LENGTH_LONG).show();
            }
        }
    }

