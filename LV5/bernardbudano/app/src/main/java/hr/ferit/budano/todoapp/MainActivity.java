package hr.ferit.budano.todoapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import hr.ferit.budano.todoapp.database.Todo;
import hr.ferit.budano.todoapp.database.TodoHelper;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "MainActivity";
    private TodoHelper todoHelper;
    private ListView listView;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        todoHelper = new TodoHelper(this);
        listView = (ListView) findViewById(R.id.list_todo);

        updateUserInterface();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(LOG_TAG, "onOptionsItemSelected: " + item.toString());
        switch(item.getItemId()) {
            case R.id.action_add_task:
                final EditText etTask = new EditText(this);
                final AlertDialog alertDialog = new AlertDialog.Builder(this)
                        .setTitle("New TODO item")
                        .setMessage("Create a new TODO item: ")
                        .setView(etTask)
                        .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogue, int which) {
                                String task = String.valueOf(etTask.getText());
                                SQLiteDatabase db = todoHelper.getWritableDatabase();
                                ContentValues values = new ContentValues();
                                values.put(Todo.TodoEntry.COL_TODO_NAME, task);
                                db.insertWithOnConflict(Todo.TodoEntry.TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                                db.close();
                                updateUserInterface();
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create();
                alertDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateUserInterface() {
        Log.d(LOG_TAG, "updateUserInterface");
        ArrayList<String> taskList = new ArrayList<>();
        SQLiteDatabase db = todoHelper.getReadableDatabase();
        Cursor cursor = db.query(Todo.TodoEntry.TABLE,
                new String[] {Todo.TodoEntry.COL_TODO_NAME}, null, null, null, null, null);

        while (cursor.moveToNext()) {
            int index = cursor.getColumnIndex(Todo.TodoEntry.COL_TODO_NAME);
            taskList.add(cursor.getString(index));
        }

        if (adapter == null) {
            adapter = new ArrayAdapter<String>(this, R.layout.item_todo, R.id.task_title, taskList);
            listView.setAdapter(adapter);

        } else {
            adapter.clear();
            adapter.addAll(taskList);
            adapter.notifyDataSetChanged();
        }

        cursor.close();
        db.close();
    }



    public void deleteTask(View view) {
        Log.d(LOG_TAG, "deleteTask: " + view.toString());
        View parent = (View) view.getParent();
        TextView taskTextView = (TextView) parent.findViewById(R.id.task_title);
        String task = String.valueOf(taskTextView.getText());
        SQLiteDatabase db = todoHelper.getWritableDatabase();
        db.delete(Todo.TodoEntry.TABLE, Todo.TodoEntry.COL_TODO_NAME + " = ?", new String[] {task});
        db.close();
        updateUserInterface();
    }
}
