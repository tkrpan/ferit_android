package hr.ferit.budano.todoapp.database;

import android.provider.BaseColumns;

/**
 * Created by vivekraja07 on 9/3/17.
 */

public class Todo {

    public static final String DB_NAME = "todo_list";
    public static final int DB_VERSION = 1;

    public class TodoEntry implements BaseColumns {
        public static final String TABLE = "todo";
        public static final String COL_TODO_NAME = "name";
    }

}
