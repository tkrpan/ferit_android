package hr.ferit.budano.todoapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by vivekraja07 on 9/3/17.
 */

public class TodoHelper extends SQLiteOpenHelper {

    public TodoHelper(Context context) {
        super(context, Todo.DB_NAME, null, Todo.DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "Create Table " + Todo.TodoEntry.TABLE + " ( " +
                Todo.TodoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Todo.TodoEntry.COL_TODO_NAME + " TEXT NOT NULL);";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Todo.TodoEntry.TABLE);
        onCreate(db);
    }
}