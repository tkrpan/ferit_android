package com.example.halloffame;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listView;

    String names []={"Mario Ančić","Luka Modrić","Mirko Filipović"};
    String births []={"30 March 1984","9 September 1985","10 September 1974"};
    String descs []={"Mario Ančić (Croatian pronunciation: [mâːrio âːntʃitɕ];[3][4] born 30 March 1984)" +
            " is a Croatian former professional tennis player who currently works as an investment banker" +
            " in New York City.","Luka Modrić (Croatian pronunciation: [lûːka mǒːdritɕ];[4][5] born 9 September 1985)" +
            " is a Croatian professional footballer who plays as a midfielder for Spanish club Real Madrid and captains" +
            " the Croatia national team.","Mirko Filipović (Croatian pronunciation: [mîrko fǐːlipɔːʋitɕ]; born 10 September 1974)," +
            " known by the nickname Cro Cop, is a retired Croatian professional mixed martial artist, kickboxer and amateur boxer."};
    int images [] = {R.drawable.mario_ancic,R.drawable.luka_modric,R.drawable.mirko_filipovic};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView=findViewById(R.id.listView);
        Adaptiza adapter = new Adaptiza(this,names,births,descs,images);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position ==  0) {
                    Toast.makeText(MainActivity.this, "I am not sure people realize how big it is to be at the top of the pyramid.", Toast.LENGTH_SHORT).show();
                }
                if (position ==  1) {
                    Toast.makeText(MainActivity.this, "Simply a MAGICIAN", Toast.LENGTH_SHORT).show();
                }
                if (position ==  2) {
                    Toast.makeText(MainActivity.this, "Right Leg Hospital, Left Leg Cemetery...", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    class Adaptiza extends ArrayAdapter <String>{

        Context context;

        String rTit [];
        String rbirt [];
        String rDesc [];
        int rImg [];

        Adaptiza(Context c,String title[],String birth[],String desc[],int image[]){
            super(c,R.layout.row,R.id.textView1,title);
            this.context = c;
            this.rTit = title;
            this.rbirt = birth;
            this.rDesc = desc;
            this.rImg = image;
        }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @Nullable ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row,parent,false);
            ImageView images = row.findViewById(R.id.image);
            TextView myTitle = row.findViewById(R.id.textView1);
            TextView myDescription = row.findViewById(R.id.textView3);
            TextView myBirth = row.findViewById(R.id.textView2);
            images.setImageResource(rImg[position]);
            myTitle.setText(rTit[position]);
            myDescription.setText(rDesc[position]);
            myBirth.setText(rbirt[position]);
            return row;
        }
    }
}
