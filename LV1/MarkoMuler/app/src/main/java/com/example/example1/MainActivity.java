package com.example.example1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Button;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private TextView result,definition,description;
    private EditText meters,kilograms;
    private Button calculate;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();
        calculation();
    }

    private void setView() {
        meters = findViewById(R.id.meters);
        kilograms = findViewById(R.id.kilograms);
        result = findViewById(R.id.result);
        definition = findViewById(R.id.definition);
        description = findViewById(R.id.description);
        image = findViewById(R.id.image);
        calculate = findViewById(R.id.calculate);

    }
    private void calculation() {
        this.calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double height = Double.parseDouble(meters.getText().toString());
                double weight = Double.parseDouble(kilograms.getText().toString());
                double res = weight / (height * height);
                result.setText(String.format("BMI: %.2f", res));
                result.setVisibility(View.VISIBLE);


                if (res < 18.5) {
                    definition.setText(R.string.uw);
                } else if (res >= 18.5 && res < 23) {
                    definition.setText(R.string.healty);
                } else if (res >= 23 && res < 27.5) {
                    definition.setText(R.string.ow);
                } else if (res >= 27.5) {
                    definition.setText(R.string.ob);
                }
                definition.setVisibility(View.VISIBLE);


                if (res < 18.5) {
                    description.setText(getString(R.string.underweight));
                } else if (res >= 18.5 && res < 23) {
                    description.setText(R.string.healty);
                } else if (res >= 23 && res < 27.5) {
                    description.setText(R.string.overweight);
                } else if (res >= 27.5) {
                    description.setText(R.string.obese);
                }
                description.setVisibility(View.VISIBLE);


                if (res < 18.5) {  image.setBackgroundResource(R.drawable.underweight); }
                else if (res >= 18.5 && res < 23) { image.setBackgroundResource(R.drawable.normalweight); }
                else if (res >= 23 && res < 27.5) { image.setBackgroundResource(R.drawable.overweight); }
                else if (res >= 27.5) { image.setBackgroundResource(R.drawable.obese); }
                image.setVisibility(View.VISIBLE);
            }
        });
    }

}
