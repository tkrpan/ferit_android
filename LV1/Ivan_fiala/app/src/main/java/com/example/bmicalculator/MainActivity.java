package com.example.bmicalculator;

import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText height;
    private EditText weight;
    private TextView result;
    private TextView resultDesc;
    private ImageView resultImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        height = (EditText) findViewById(R.id.height);
        weight = (EditText) findViewById(R.id.weight);
        result = (TextView) findViewById(R.id.result);
        resultDesc =(TextView) findViewById(R.id.resultDesc);
        resultImg =(ImageView) findViewById(R.id.resultImg);
    }

    public void calculateBMI(View v) {
        String heightStr = height.getText().toString();
        String weightStr = weight.getText().toString();

        if (heightStr != null && !"".equals(heightStr)
                && weightStr != null  &&  !"".equals(weightStr)) {
            float heightValue = Float.parseFloat(heightStr) / 100;
            float weightValue = Float.parseFloat(weightStr);

            float bmi = weightValue / (heightValue * heightValue);

            displayBMI(bmi);
        }
    }

    private void displayBMI(float bmi) {
        String bmiLabel = "";
        String bmiDesc = "";

        if (Float.compare(bmi, 18.5f) < 0) {
            bmiLabel = getString(R.string.underweight);
            bmiDesc = getString(R.string.descUnderweight);
            resultImg.setBackgroundResource(R.drawable.underweight);
        } else if (Float.compare(bmi, 18.5f) >= 0  &&  Float.compare(bmi, 25f) < 0) {
            bmiLabel = getString(R.string.normal);
            bmiDesc = getString(R.string.descNormal);
            resultImg.setBackgroundResource(R.drawable.normal);
        } else if (Float.compare(bmi, 25f) >= 0  &&  Float.compare(bmi, 30f) < 0) {
            bmiLabel = getString(R.string.overweight);
            bmiDesc = getString(R.string.descOverweight);
            resultImg.setBackgroundResource(R.drawable.overweight);
        } else if (Float.compare(bmi, 30f) >= 0) {
            bmiLabel = getString(R.string.obesity);
            bmiDesc = getString(R.string.descObesity);
            resultImg.setBackgroundResource(R.drawable.obese);
        }

        bmiLabel = bmi + "\n\n" + bmiLabel;
        result.setText(bmiLabel);
        resultDesc.setText(bmiDesc);
        resultImg.setVisibility(View.VISIBLE);

    }
}
