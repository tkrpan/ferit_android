package com.example.halloffame;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String Name[] = {"Oliver Kahn", "Klaas Jan Huntelaar", "Luka Modrić"};
    String DateOfBirth[] = {"15 June 1969", "12 August 1983", "9 September 1985"};
    String Description[] = {"German former football goalkeeper.\n" +
            "Kahn is one of the most successful German players in recent history",
            "Dutch professional footballer who plays as a striker\n" +
                    "Huntelaar was named Dutch Football Talent of the Year and Ajax \"Player of the Year\" in 2006",
            "Croatian professional footballer who plays as a midfielder\n" +
                    "Modrić is widely regarded as one of the best midfielders of his generation and the greatest Croatian footballer of all time"};
    int images[] = {R.drawable.kahn, R.drawable.huntelaar, R.drawable.modric};

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        String rName[];
        String rDateOfBirth[];
        String rDescription[];
        int rImages[];

        MyAdapter(Context c, String name[], String dateOfBirth[], String description[], int images[]) {
            super(c, R.layout.row, R.id.textView1, name);
            this.context = c;
            this.rName = name;
            this.rDateOfBirth = dateOfBirth;
            this.rDescription = description;
            this.rImages = images;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row, parent, false);
            ImageView images = row.findViewById(R.id.image);
            TextView myName = row.findViewById(R.id.textView1);
            TextView myDateOfBirth = row.findViewById(R.id.textView2);
            TextView myDescription = row.findViewById(R.id.textView3);

            // now set our resources on views
            images.setImageResource(rImages[position]);
            myName.setText(rName[position]);
            myDateOfBirth.setText(rDateOfBirth[position]);
            myDescription.setText(rDescription[position]);

            return row;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);


        MyAdapter adapter = new MyAdapter(this, Name, DateOfBirth, Description, images);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Toast.makeText(MainActivity.this, "FC Bayern München", Toast.LENGTH_SHORT).show();
                }
                if (position == 1) {
                    Toast.makeText(MainActivity.this, "FC Ajax", Toast.LENGTH_SHORT).show();
                }
                if (position == 2) {
                    Toast.makeText(MainActivity.this, "FC Real Madrid", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}

