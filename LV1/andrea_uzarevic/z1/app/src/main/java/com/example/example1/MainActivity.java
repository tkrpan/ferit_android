package com.example.example1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText height, weight;
    private TextView result;
    private Button button;
    private ImageView image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        height = (EditText) findViewById(R.id.height);
        weight = (EditText) findViewById(R.id.weight);
        result = (TextView) findViewById(R.id.result);
    }

    public void calculateBMI(View v) {
        String heightStr = height.getText().toString();
        String weightStr = weight.getText().toString();

        if(heightStr != null && !"".equals(heightStr) && weightStr != null && !"".equals(weightStr)){
            double heightValue = Double.parseDouble(heightStr) / 100;
            double weightValue = Double.parseDouble(weightStr);

            double bmi = weightValue / (heightValue * heightValue);
            displayBMI(bmi);
        }
    }

    private void displayBMI(double bmi) {
        String doubleBMI = "";

        if (Double.compare(bmi, 15d) <= 0 && Double.compare(bmi, 18.5d) <= 0) {

            doubleBMI = "Underweight";
            ImageView simpleImageView=(ImageView) findViewById(R.id.imageView2);
            simpleImageView.setImageResource(R.drawable.underweight);
        } else if (Double.compare(bmi, 18.5d) > 0 && Double.compare(bmi, 25d) <= 0){

            doubleBMI = "Normal";
            ImageView simpleImageView=(ImageView) findViewById(R.id.imageView2);
            simpleImageView.setImageResource(R.drawable.normal);
        } else if (Double.compare(bmi, 25d) > 0 && Double.compare(bmi, 30d) <= 0) {

            doubleBMI = "Overweight";
            ImageView simpleImageView=(ImageView) findViewById(R.id.imageView2);
            simpleImageView.setImageResource(R.drawable.overweight);
        } else if (Double.compare(bmi, 30d) > 0 && Double.compare(bmi, 40d) <= 0) {

            doubleBMI = "Obese";
            ImageView simpleImageView=(ImageView) findViewById(R.id.imageView2);
            simpleImageView.setImageResource(R.drawable.obese);
        }

        doubleBMI = bmi + "\n\n" + doubleBMI;
        result.setText(doubleBMI);
    }
}
