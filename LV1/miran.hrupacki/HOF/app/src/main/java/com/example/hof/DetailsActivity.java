package com.example.hof;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DetailsActivity extends AppCompatActivity {

    ImageView PlayerImage;
    TextView details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initializeUI();
    }

    private void initializeUI(){
        String info, image;
        info  ="";
        image ="";
        this.details = (TextView) findViewById(R.id.info);
        this.PlayerImage = (ImageView) findViewById(R.id.image);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            info = bundle.get("info").toString();
            image = bundle.get("image").toString();
        }

        switch(image){
            case ("Messi"):
                PlayerImage.setImageResource(R.drawable.messi);
                details.setText(info);
                break;

            case("Neymar"):
                PlayerImage.setImageResource(R.drawable.neymar);
                details.setText(info);
                break;

            case("Suarez"):
                PlayerImage.setImageResource(R.drawable.suarez);
                details.setText(info);
                break;
        }
    }
}
