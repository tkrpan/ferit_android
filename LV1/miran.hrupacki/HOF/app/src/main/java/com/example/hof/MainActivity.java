package com.example.hof;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    CardView cardMessi, cardNeymar, cardSuarez;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }


    private void initializeUI(){
        this.cardMessi  = (CardView) findViewById(R.id.cardMessi);
        this.cardNeymar = (CardView) findViewById(R.id.cardNeymar);
        this.cardSuarez = (CardView) findViewById(R.id.cardSuarez);

        this.cardMessi.setOnClickListener(this);
        this.cardNeymar.setOnClickListener(this);
        this.cardSuarez.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
        Bundle bundle = new Bundle();

        switch(v.getId()){
            case R.id.cardMessi:
                bundle.putString("info", getString(R.string.MessiInfo));
                bundle.putString("image", "Messi");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.cardNeymar:
                bundle.putString("info", getString(R.string.NeymarInfo));
                bundle.putString("image", "Neymar");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.cardSuarez:
                bundle.putString("info", getString(R.string.SuarezInfo));
                bundle.putString("image", "Suarez");
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }
}
