package com.example.sportasi;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String Title[]={"Wtipe miočić", "Jon Jones", "Conor McGregor"};
    String Description[] = {
            "19. kolovoza 1982., Ukupno borbi: 22, Pobjeda: 19, Poraz: 3, Neriješeno: 0",
            "19. srpnja 1987., Ukupno borbi: 28, Pobjeda: 26, Poraz: 2, Neriješeno: 0",
            "14. srpnja 1988., Ukupno borbi: 25, Pobjeda: 21, Poraz: 4, Neriješeno: 0"
    };
    int images[] = {R.drawable.stipe, R.drawable.jon, R.drawable.conor};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);

        class MyAdapter extends ArrayAdapter<String> {
            Context context;
            String Title[];
            String Description[];
            int Imgs[];


            MyAdapter (Context c, String title[], String description[], int imgs[]) {
                super(c, R.layout.row, R.id.textView1, title);
                this.context = c;
                this.Title = title;
                this.Description = description;
                this.Imgs = imgs;
            }

            @NonNull
            @Override
            public View getView(int position, @NonNull View convertView, @NonNull ViewGroup parent) {
                LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View row = layoutInflater.inflate(R.layout.row, parent, false);
                ImageView images = row.findViewById(R.id.image);
                TextView myTitle = row.findViewById(R.id.textView1);
                TextView myDescription = row.findViewById(R.id.textView2);

                images.setImageResource(Imgs[position]);
                myTitle.setText(Title[position]);
                myDescription.setText(Description[position]);
                return row;
            }
        }





        MyAdapter adapter = new MyAdapter(this, Title, Description, images);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position ==  0) {
                    Toast.makeText(MainActivity.this, "I've done things that weren't good, you know, but I made up for my mistakes.", Toast.LENGTH_SHORT).show();
                }
                if (position ==  1) {
                    Toast.makeText(MainActivity.this, "I don't care about history; all I care about is winning.", Toast.LENGTH_SHORT).show();
                }
                if (position ==  2) {
                    Toast.makeText(MainActivity.this, "I’m just going to keep doing what I’m doing. Keep proving people wrong and proving myself right.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
