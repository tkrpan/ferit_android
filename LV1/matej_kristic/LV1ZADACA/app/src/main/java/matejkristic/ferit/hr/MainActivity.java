package matejkristic.ferit.hr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    // For list view
    int[] images = {R.drawable.mario, R.drawable.luka, R.drawable.srna};
    String[] name = {"Mario Mandukić", "Luka Modrić", "Dario Srna"};
    String[] description = {"Mario Mandžukić odlazi tijekom rata s obitelji iz Odžaka u Bosni u Njemačku u grad Ditzingen blizu Stuttgarta i tamo počinje trenirati nogomet u klubu u kojemu igra njegov otac.[2] Obitelj se nakon rata vraća iz Njemačke u Slavonski Brod gdje Mario od 1997. do 2003. godine igra za NK Marsoniju. Sljedeće godine, igrao je u gradskom niželigašu Željezničaru, a nakon toga, ponovno je jednu sezonu 2004./05. odigrao za Marsoniju. U lipnju 2005. prešao je u NK Zagreb.\n" +
            "\n" +
            "U dresu Zagreba, na Dvoranskom prvenstvu HNL, u sezoni 2005./06. izabran je za najboljeg igrača prvenstva, a na istom natjecanju u sezoni 2006./07., bio je najbolji strijelac.\n" +
            "\n", "Luka Modrić, hrvatski je profesionalni nogometaš, hrvatski nogometni reprezentativac i kapetan hrvatske nogometne reprezentacije koji trenutačno igra za španjolski nogometni klub Real Madrid. Većinom igra kao razigravač na poziciji navalnog veznog ili na poziciji lijevog krila. Za sezonu 2017.\n" +
            "\n", "Darijo Srna, bivši kapetan hrvatske nogometne reprezentacije i bivši igrač Hajduka, Šahtara i Cagliarija. Sin nogometnog vratara Uzeira, koji je pored ostalih klubova, branio u Sarajevu i Čeliku. S 134 nastupom na čelu je ljestvice hrvatskih reprezentativaca po broju nastupa.\n" +
            "\n"};
    String[] quotes = {"Tvoja snaga je u tome da nikada ne odustaneš od sebe.","Podcijenili su Hrvatsku večeras " +
            "i to je bila velika greška", " Jedan igrač nemože sve učiniti sam,nogomet je timska igra."};
    ListView lView;
    ListAdapter lAdapter;

    // For activities
    private Button nav1,nav2,nav3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // For List View
        lView = findViewById(R.id.androidList);
        lAdapter = new ListAdapter(MainActivity.this, name, description, images);
        lView.setAdapter(lAdapter);
        lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Toast.makeText(MainActivity.this, quotes[i], Toast.LENGTH_LONG).show();

            }
        });

        nav1 =  findViewById(R.id.nav1);
        nav1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity1();
            }
        });

        nav2 =  findViewById(R.id.nav2);
        nav2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity2();
            }
        });

        nav3 =  findViewById(R.id.nav3);
        nav3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity3();
            }
        });
    }


    public void openActivity1() {
        Intent intent = new Intent(this, Activity1.class);
        startActivity(intent);
    }

    public void openActivity2() {
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }

    public void openActivity3() {
        Intent intent = new Intent(this, Activity3.class);
        startActivity(intent);
    }
}
