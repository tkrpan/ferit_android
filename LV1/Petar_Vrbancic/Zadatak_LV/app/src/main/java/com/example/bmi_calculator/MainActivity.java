package com.example.bmi_calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView result1,result2,result3;
    private EditText meters,kilograms;
    private Button calculateBMI;
    private ImageView result4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();
        calculateBMI();
    }

    private void setView() {
        this.meters = findViewById(R.id.meters);
        this.kilograms = findViewById(R.id.kilograms);
        this.result1 = findViewById(R.id.result1);
        this.result2 = findViewById(R.id.result2);
        this.result3 = findViewById(R.id.result3);
        this.result4 = findViewById(R.id.result4);
        this.calculateBMI = findViewById(R.id.calculateBMI);
    }

    private void calculateBMI() {

        this.calculateBMI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Calculate BMI
                double height =Double.parseDouble(meters.getText().toString());
                double weight =Double.parseDouble(kilograms.getText().toString());
                double res = weight / (height*height);
                double rounded = Math.round(res * 10) / 10.0;
                result1.setText("BMI: " + Double.toString(rounded));
                result1.setVisibility(View.VISIBLE);


                // Set status
                if (rounded < 18.5) { result2.setText("\"Underweight\""); }
                else if (rounded >= 18.5 && rounded < 23) { result2.setText("\"Healty range\""); }
                else if (rounded >= 23 && rounded < 27.5) { result2.setText("\"Over weight\"");}
                else if (rounded >= 27.5) { result2.setText("\"Obese\"");}
                result2.setVisibility(View.VISIBLE);


                // Set description
                if (rounded < 18.5) { result3.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit." +
                        " Praesent pharetra metus risus, sollicitudin pharetra ex gravida posuere. " +
                        "Morbi tempor lectus."); }
                else if (rounded >= 18.5 && rounded < 23) { result3.setText("Lorem ipsum dolor sit amet," +
                        " consectetur adipiscing elit. Nullam sagittis ex et metus ullamcorper, " +
                        "at efficitur lacus imperdiet. Ut cursus."); }
                else if (rounded >= 23 && rounded < 27.5) { result3.setText("Lorem ipsum dolor sit amet, " +
                        "consectetur adipiscing elit. Nulla rhoncus dapibus euismod. " +
                        "Donec nunc sapien, aliquam aliquam eros non, gravida");}
                else if (rounded >= 27.5) { result3.setText("Lorem ipsum dolor sit amet, " +
                        "consectetur adipiscing elit. Vestibulum commodo consequat lorem, " +
                        "id auctor enim elementum ac. Aenean lacinia semper.");}
                result3.setVisibility(View.VISIBLE);

                // Set Image
                if (rounded < 18.5) {  result4.setBackgroundResource(R.drawable.underweight); }
                else if (rounded >= 18.5 && rounded < 23) { result4.setBackgroundResource(R.drawable.healty_range); }
                else if (rounded >= 23 && rounded < 27.5) { result4.setBackgroundResource(R.drawable.over_weight); }
                else if (rounded >= 27.5) { result4.setBackgroundResource(R.drawable.obese); }
                result4.setVisibility(View.VISIBLE);


            }

        });


    }


    }


