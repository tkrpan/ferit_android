package com.example.fan_app_calisthenics;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    // For list view
    int[] images = {R.drawable.cristiano_ronaldo_2018, R.drawable.lm, R.drawable.mario_mandzukic_in_2018};
    String[] name = {"Cristiano Ronaldo", "Luka Modrić", "Mario Mandžukić"};
    String[] description = {"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet porta libero. Ut " +
            "non aliquam neque, a vestibulum augue.\n" +
            "\n", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet porta libero. Ut non aliquam neque, a vestibulum augue.\n" +
            "\n", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet porta libero. Ut non aliquam neque, a vestibulum augue.\n" +
            "\n"};
    String[] quotes = {"Work hard.","Train hard.", "The results will be."};
    ListView lView;
    ListAdapter lAdapter;

    // For activities
    private Button nav1,nav2,nav3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // For List View
        lView = findViewById(R.id.androidList);
        lAdapter = new ListAdapter(MainActivity.this, name, description, images);
        lView.setAdapter(lAdapter);
        lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Toast.makeText(MainActivity.this, quotes[i], Toast.LENGTH_LONG).show();

            }
        });

        nav1 =  findViewById(R.id.nav1);
        nav1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity1();
            }
        });

        nav2 =  findViewById(R.id.nav2);
        nav2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity2();
            }
        });

        nav3 =  findViewById(R.id.nav3);
        nav3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity3();
            }
        });
    }


    public void openActivity1() {
        Intent intent = new Intent(this, Activity1.class);
        startActivity(intent);
    }

    public void openActivity2() {
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }

    public void openActivity3() {
        Intent intent = new Intent(this, Activity3.class);
        startActivity(intent);
    }
}
