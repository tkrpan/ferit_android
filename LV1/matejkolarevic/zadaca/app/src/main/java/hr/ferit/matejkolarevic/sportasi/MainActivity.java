package hr.ferit.matejkolarevic.sportasi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void DisplayToast_crocop(View view) {
        Toast.makeText(MainActivity.this, "Cro Cop Squad Gym", Toast.LENGTH_SHORT).show();
    }

    public void DisplayToast_kostelic(View view) {
        Toast.makeText(MainActivity.this, "Ski club Zagreb", Toast.LENGTH_SHORT).show();
    }

    public void DisplayToast_modric(View view) {
        Toast.makeText(MainActivity.this, "Dinamo Zagreb", Toast.LENGTH_SHORT).show();
    }
}
