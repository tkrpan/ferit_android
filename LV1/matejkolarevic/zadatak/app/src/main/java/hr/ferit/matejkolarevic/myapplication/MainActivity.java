package hr.ferit.matejkolarevic.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText weight, height;
    TextView resultText, classText, descriptionText;
    String resultValue, classValue, descriptionValue;
    ImageView weightClassImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        weight = findViewById(R.id.weightText);
        height = findViewById(R.id.heightText);
        resultText = findViewById(R.id.TextViewBMI);
        classText = findViewById(R.id.textViewWeightClass);
        weightClassImage = findViewById(R.id.weightClassImage);
        descriptionText = findViewById(R.id.textViewDescription);
    }

    public void CalculateBMI(View view) {
        if(weight.getText().length()==0){
            resultValue = "enter your weight";
        } else if(height.getText().length()==0){
            resultValue = "enter your height";
        } else {
            float weightValue = Float.parseFloat(weight.getText().toString());
            float heightValue = Float.parseFloat(height.getText().toString()) / 100;
            if(weightValue <= 0 || weightValue > 350){
                resultValue = "weight should be between 0kg and 350kg";
            } else if(heightValue <= 0 || heightValue > 350){
                resultValue = "height should be between 0m and 2.5m";
            } else {
                float bmi = weightValue / (heightValue * heightValue);
                resultValue = "BMI: " + bmi;

                if(bmi < 18.5){
                    classValue = "underweight";
                    descriptionValue = getString(R.string.textViewDescription_underweight);
                    weightClassImage.setImageResource(R.mipmap.wc1);
                } else if(bmi < 25){
                    classValue = "normal";
                    descriptionValue = getString(R.string.textViewDescription_normal);
                    weightClassImage.setImageResource(R.mipmap.wc2);
                } else if(bmi < 30){
                    classValue = "overweight";
                    descriptionValue = getString(R.string.textViewDescription_overweight);
                    weightClassImage.setImageResource(R.mipmap.wc3);
                } else {
                    classValue = "obese";
                    descriptionValue = getString(R.string.textViewDescription_obese);
                    weightClassImage.setImageResource(R.mipmap.wc4);
                }
                descriptionText.setText(descriptionValue);
                descriptionText.setVisibility(View.VISIBLE);
                classText.setText(classValue);
                classText.setVisibility(View.VISIBLE);
                weightClassImage.setVisibility(View.VISIBLE);
            }
        }
        resultText.setText(resultValue);
        resultText.setVisibility(View.VISIBLE);
    }
}
