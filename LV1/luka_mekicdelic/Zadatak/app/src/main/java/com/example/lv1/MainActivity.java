package com.example.lv1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private static final String LOG_TAG = "LV1";

    private EditText heightEditText;
    private EditText weightEditText;
    private Button calcButton;
    private TextView resultTextView;
    private TextView titleTextView;
    private TextView descTextView;
    private ImageView imageView;

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(LOG_TAG,"MainActivity onCreate");

        setView();
    }

    //Helpers

    private void setView()
    {
        Log.d(LOG_TAG,"MainActivity setView");

        heightEditText = findViewById(R.id.heightEditText);
        weightEditText = findViewById(R.id.weightEditText);
        calcButton = findViewById(R.id.calcButton);
        resultTextView = findViewById(R.id.resultTextView);
        titleTextView = findViewById(R.id.titleTextView);
        descTextView = findViewById(R.id.descTextView);
        imageView = findViewById(R.id.imageView);

        calcButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        Log.d(LOG_TAG, "MainActivity onClick");

        String heightString = heightEditText.getText().toString();
        String weightString = weightEditText.getText().toString();

        Log.d(LOG_TAG, "MainActivity onClick heightString " + heightString);
        Log.d(LOG_TAG, "MainActivity onClick weightString " + weightString);

        if(heightString != null && !"".equals(heightString)
                && !"".equals(weightString) && weightString != null)
        {
            double height = Double.parseDouble(heightString);
            double weight = Double.parseDouble(weightString);
            //checkInputValues(height, weight)
            double resultBMI = calculateBMI(height,weight);
            Log.d(LOG_TAG, "MainActivity onClick resultBMI " + resultBMI);
            showResults(resultBMI);
        }

        hideKeyboard(this);
    }

    private double calculateBMI(double height, double weight)
    {
        return weight/(height*height);
    }

    private void showResults(double resultBMI)
    {
        resultTextView.setText(String.format("%.2f", resultBMI));
        String title = "", desc = "";

        if(resultBMI<18.5)
        {
            title=getString(R.string.title1);
            desc=getString(R.string.desc1);
        }
        else if(resultBMI>=18.5 && resultBMI<25)
        {
            title=getString(R.string.title2);
            desc=getString(R.string.desc2);
        }
        else if(resultBMI>=25 && resultBMI<30)
        {
            title=getString(R.string.title3);
            desc=getString(R.string.desc3);
        }
        else if(resultBMI>=30)
        {
            title=getString(R.string.title4);
            desc=getString(R.string.desc4);
        }

        titleTextView.setText(title);
        descTextView.setText(desc);
        imageView.setImageDrawable(getResources()
                .getDrawable(R.drawable.ic_launcher_background));

        resultTextView.setVisibility(View.VISIBLE);
        titleTextView.setVisibility(View.VISIBLE);
        descTextView.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.VISIBLE);
    }

    public static void hideKeyboard(Activity activity)
    {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onDestroy()
    {
        Log.d(LOG_TAG, "MainActivity onDestroy");
        super.onDestroy();
    }
}
