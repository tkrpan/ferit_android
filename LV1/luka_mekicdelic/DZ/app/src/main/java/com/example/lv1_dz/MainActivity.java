package com.example.lv1_dz;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity
{
    private static final String LOG_TAG = "LV1_DZ";

    private ImageView imageView1;
    private TextView textViewName1;
    private TextView textViewBirthday1;
    private TextView textViewBiography1;

    private ImageView imageView2;
    private TextView textViewName2;
    private TextView textViewBirthday2;
    private TextView textViewBiography2;

    private ImageView imageView3;
    private TextView textViewName3;
    private TextView textViewBirthday3;
    private TextView textViewBiography3;

    public class HallOfFame
    {
        private String name = "";
        private Drawable picture;
        private Calendar birthday;
        private String biography = "";
        private String club = "";

        void setName(String name) { this.name = name; }
        String getName() { return this.name; }

        void setPicture(Drawable picture) { this.picture = picture; }
        Drawable getPicture() { return this.picture; }

        void setBirthday(int day, int month, int year)
        {
            this.birthday = Calendar.getInstance();
            this.birthday.set(Calendar.DAY_OF_MONTH, day);
            this.birthday.set(Calendar.MONTH, month-1); //-1 because month indexing starts with 0
            this.birthday.set(Calendar.YEAR, year);
        }
        Calendar getBirthday() { return this.birthday; }

        void setBiography(String biography) { this.biography = biography; }
        String getBiography() { return this.biography; }

        void setClub(String club) { this.club = club; }
        String getClub() { return this.club; }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView1 = findViewById(R.id.imageView1);
        textViewName1 = findViewById(R.id.textViewName1);
        textViewBirthday1 = findViewById(R.id.textViewBirthday1);
        textViewBiography1 = findViewById(R.id.textViewBiography1);

        imageView2 = findViewById(R.id.imageView2);
        textViewName2 = findViewById(R.id.textViewName2);
        textViewBirthday2 = findViewById(R.id.textViewBirthday2);
        textViewBiography2 = findViewById(R.id.textViewBiography2);

        imageView3 = findViewById(R.id.imageView3);
        textViewName3 = findViewById(R.id.textViewName3);
        textViewBirthday3 = findViewById(R.id.textViewBirthday3);
        textViewBiography3 = findViewById(R.id.textViewBiography3);

        Log.d(LOG_TAG, "onCreate()");

        final HallOfFame lukaModric = new HallOfFame();
        final HallOfFame marioMandzukic = new HallOfFame();
        final HallOfFame ivanPerisic = new HallOfFame();

        java.text.DateFormat df = DateFormat.getDateFormat(this); //For getting the current locale's date formatting

        lukaModric.setName(getString(R.string.luka_modric_name));
        lukaModric.setPicture(getResources().getDrawable(R.drawable.luka_modric));
        lukaModric.setBirthday(9,9,1985);
        lukaModric.setBiography(getString(R.string.luka_modric_biography));
        lukaModric.setClub(getString(R.string.luka_modric_club));

        marioMandzukic.setName(getString(R.string.mario_mandzukic_name));
        marioMandzukic.setPicture(getResources().getDrawable(R.drawable.mario_mandzukic));
        marioMandzukic.setBirthday(21,5,1986);
        marioMandzukic.setBiography(getString(R.string.mario_mandzukic_biography));
        marioMandzukic.setClub(getString(R.string.mario_mandzukic_club));

        ivanPerisic.setName(getString(R.string.ivan_perisic_name));
        ivanPerisic.setPicture(getResources().getDrawable(R.drawable.ivan_perisic));
        ivanPerisic.setBirthday(2,2,1989);
        ivanPerisic.setBiography(getString(R.string.ivan_perisic_biography));
        ivanPerisic.setClub(getString(R.string.ivan_perisic_club));



        imageView1.setImageDrawable(lukaModric.getPicture());
        textViewName1.setText(lukaModric.getName());
        textViewBirthday1.setText(df.format(lukaModric.getBirthday().getTime()));
        textViewBiography1.setText(lukaModric.getBiography());

        imageView2.setImageDrawable(marioMandzukic.getPicture());
        textViewName2.setText(marioMandzukic.getName());
        textViewBirthday2.setText(df.format(marioMandzukic.getBirthday().getTime()));
        textViewBiography2.setText(marioMandzukic.getBiography());

        imageView3.setImageDrawable(ivanPerisic.getPicture());
        textViewName3.setText(ivanPerisic.getName());
        textViewBirthday3.setText(df.format(ivanPerisic.getBirthday().getTime()));
        textViewBiography3.setText(ivanPerisic.getBiography());

        imageView1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Log.v(LOG_TAG, "imageView1 clicked");
                Toast toast = Toast.makeText(getApplicationContext(), lukaModric.getClub(), Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        imageView2.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Log.v(LOG_TAG, "imageView2 clicked");
                Toast toast = Toast.makeText(getApplicationContext(), marioMandzukic.getClub(), Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        imageView3.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Log.v(LOG_TAG, "imageView3 clicked");
                Toast toast = Toast.makeText(getApplicationContext(), ivanPerisic.getClub(), Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }
}
