package com.example.halloffame;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    CardView cardMarioMandzukic, cardDomagojVida, cardIvanPerisic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }


    private void initializeUI(){
        this.cardMarioMandzukic  = (CardView) findViewById(R.id.cardMarioMandzukic);
        this.cardDomagojVida = (CardView) findViewById(R.id.cardDomagojVida);
        this.cardIvanPerisic = (CardView) findViewById(R.id.cardIvanPerisic);

        this.cardMarioMandzukic.setOnClickListener(this);
        this.cardDomagojVida.setOnClickListener(this);
        this.cardIvanPerisic.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
        Bundle bundle = new Bundle();

        switch(v.getId()){
            case R.id.cardMarioMandzukic:
                bundle.putString("info", getString(R.string.marioMandzukicInfo));
                bundle.putString("image", "MarioMandzukic");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.cardDomagojVida:
                bundle.putString("info", getString(R.string.domagojVidaInfo));
                bundle.putString("image", "DomagojVida");
                intent.putExtras(bundle);
                //intent.putExtra("info", getResources().getString(R.string.rafaelNadalInfo));
                //intent.putExtra("image", "RafaelNadal");
                startActivity(intent);
                break;

            case R.id.cardIvanPerisic:
                bundle.putString("info", getString(R.string.ivanPerisicInfo));
                bundle.putString("image", "IvanPerisic");
                intent.putExtras(bundle);
                //intent.putExtra("info", getResources().getString(R.string.novakDjokovicInfo));
                //intent.putExtra("image", "NovakDjokovic");
                startActivity(intent);
                break;
        }
    }
}