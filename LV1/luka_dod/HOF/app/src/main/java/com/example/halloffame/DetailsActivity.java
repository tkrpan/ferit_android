package com.example.halloffame;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DetailsActivity extends AppCompatActivity {

    ImageView PlayerImage;
    TextView details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initializeUI();
    }

    private void initializeUI(){
        String info, image;
        info  ="";
        image ="";
        this.details = (TextView) findViewById(R.id.info);
        this.PlayerImage = (ImageView) findViewById(R.id.image);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            info = bundle.get("info").toString();
            image = bundle.get("image").toString();
        }

        switch(image){
            case ("MarioMandzukic"):
                PlayerImage.setImageResource(R.drawable.mario_mandzukic);
                details.setText(info);
                break;

            case("DomagojVida"):
                PlayerImage.setImageResource(R.drawable.domagoj_vida);
                details.setText(info);
                break;

            case("IvanPerisic"):
                PlayerImage.setImageResource(R.drawable.ivan_perisic);
                details.setText(info);
                break;
        }
    }
}