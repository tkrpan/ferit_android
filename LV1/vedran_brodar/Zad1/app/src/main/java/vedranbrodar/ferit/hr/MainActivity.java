package vedranbrodar.ferit.hr;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText height;
    private EditText weight;
    private TextView result;
    private ImageView image;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        height = (EditText) findViewById(R.id.height);
        weight = (EditText) findViewById(R.id.weight);
        result = (TextView) findViewById(R.id.result);

    }
    public void calculateBMI(View v){
        String heightStr = height.getText().toString();
        String weightStr = weight.getText().toString();

        if (heightStr != null && !"".equals(heightStr) && weightStr !=null && !"".equals(weightStr)){
            float heightValue = Float.parseFloat(heightStr) / 100;
            float weightValue = Float.parseFloat(weightStr);

            float bmi = weightValue / (heightValue * heightValue);

            displayBMI(bmi);

        }
    }
    private void displayBMI(float bmi){
        String bmiLabel="";

        if (Float.compare(bmi, 15f) <= 0){
            bmiLabel = getString(R.string.very_severely_underweight);
        } else if (Float.compare(bmi,15f) >0 && Float.compare(bmi,18.5f) <= 0 ){
            bmiLabel = getString(R.string.underweight);
            ImageView simpleImageView=(ImageView) findViewById(R.id.imageView2);
            simpleImageView.setImageResource(R.drawable.underweight);
        }else if (Float.compare(bmi,18.5f) >0 && Float.compare(bmi, 25f) <= 0 ){
            bmiLabel = getString(R.string.normal);
            ImageView simpleImageView=(ImageView) findViewById(R.id.imageView2);
            simpleImageView.setImageResource(R.drawable.normal);
        }else if (Float.compare(bmi,25f) >0 && Float.compare(bmi,30f) <= 0 ){
            bmiLabel = getString(R.string.overweight);
            ImageView simpleImageView=(ImageView) findViewById(R.id.imageView2);
            simpleImageView.setImageResource(R.drawable.overweight);
        }else if (Float.compare(bmi,30f) >0 && Float.compare(bmi,35f) <= 0 ){
            bmiLabel = getString(R.string.obese_class_1);
            ImageView simpleImageView=(ImageView) findViewById(R.id.imageView2);
            simpleImageView.setImageResource(R.drawable.obese);
        }else if (Float.compare(bmi,35f) >0 && Float.compare(bmi,40f) <= 0 ){
            bmiLabel = getString(R.string.obese_class_2);
        } else if(Float.compare(bmi,40f) >0 ) {
            bmiLabel = getString(R.string.obese_class_3);
            ImageView simpleImageView=(ImageView) findViewById(R.id.imageView2);
            simpleImageView.setImageResource(R.drawable.exobese);
        }

        bmiLabel = bmi + "\n\n" + bmiLabel;
        result.setText(bmiLabel);

    }
}
