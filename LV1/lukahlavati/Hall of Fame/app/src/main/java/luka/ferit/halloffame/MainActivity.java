package luka.ferit.halloffame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout layoutEmilia, layoutWill, layoutThompson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }


    private void initializeUI(){
        this.layoutEmilia  = (RelativeLayout) findViewById(R.id.Emilia);
        this.layoutWill   = (RelativeLayout) findViewById(R.id.Will);
        this.layoutThompson = (RelativeLayout) findViewById(R.id.Thompson);

        this.layoutEmilia.setOnClickListener(this);
        this.layoutWill.setOnClickListener(this);
        this.layoutThompson.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        Intent intent = new Intent(MainActivity.this, AboutActivity.class);
        Bundle bundle = new Bundle();

        switch(v.getId()){
            case R.id.Emilia:
                bundle.putString("info", getString(R.string.EmiliaDesc));
                bundle.putString("image", "emilia");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.Will:
                bundle.putString("info", getString(R.string.WillDesc));
                bundle.putString("image", "willsmith");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.Thompson:
                bundle.putString("info", getString(R.string.ThompsonDesc));
                bundle.putString("image", "thompson");
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }
}