package luka.ferit.halloffame;

import android.widget.ImageView;
import android.widget.TextView;
import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;

public class AboutActivity extends AppCompatActivity {

    ImageView PlayerImage;
    TextView details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        initializeUI();
    }

    private void initializeUI(){
        String info  ="";
        String image ="";
        this.details = (TextView) findViewById(R.id.info);
        this.PlayerImage = (ImageView) findViewById(R.id.image);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            info = bundle.get("info").toString();
            image = bundle.get("image").toString();
        }

        switch(image){
            case "Emilia":
                PlayerImage.setImageResource(R.drawable.emilia);
                details.setText(info);
                break;

            case "Willsmith":
                PlayerImage.setImageResource(R.drawable.willsmith);
                details.setText(info);
                break;

            case "Thompson":
                PlayerImage.setImageResource(R.drawable.thompson);
                details.setText(info);
                break;
        }
    }
}
