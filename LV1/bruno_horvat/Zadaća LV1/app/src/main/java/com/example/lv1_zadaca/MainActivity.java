package com.example.lv1_zadaca;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    int[] images = {R.drawable.federer, R.drawable.schumacher, R.drawable.messi};
    String[] Names = {"Roger Federer", "Michael Schumacher", "Lionel Messi"};
    String[] Date = {"August 8, 1981.", "January 3, 1969.", "June 24, 1987"};
    String[] Description = {"Has won a record-breaking 20 single Grand Slam titles. Held no.1 world ranking for 310 weeks overall. Many consider him to be the greatest tennis player of all time.", "Seven times World Champion holds many Formula One records for most wins and most podium placings.", "One of the most spectacular footballers of the modern game. Messi has led Barcelona FC to unprecedented domestic success and has been voted footballer of the year (FIFA Ballon d’Or) five times."};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);
        MyAdapter adapter = new MyAdapter(this, Names, Date, Description, images);
        listView.setAdapter(adapter);
        // now set item click on list view
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position ==  0) {
                    Toast.makeText(MainActivity.this, "Tenis", Toast.LENGTH_SHORT).show();
                }
                if (position ==  1) {
                    Toast.makeText(MainActivity.this, "Formula 1", Toast.LENGTH_SHORT).show();
                }
                if (position ==  2) {
                    Toast.makeText(MainActivity.this, "Football", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        String title1[];
        String description1[];
        String date1[];
        int images1[];

        MyAdapter (Context c, String title[], String date[], String description[], int imgs[]) {
            super(c, R.layout.layout2, R.id.textView1, title);
            this.context = c;
            this.title1 = title;
            this.date1 = date;
            this.description1 = description;
            this.images1 = imgs;
        }

        @Override
        public View getView(int position, @Nullable View convertView, @Nullable ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = getLayoutInflater().inflate(R.layout.layout2, null);

            ImageView images = (ImageView) view.findViewById(R.id.imageView);
            TextView title = (TextView) view.findViewById(R.id.textView1);
            TextView date = (TextView) view.findViewById(R.id.date);
            TextView description = view.findViewById(R.id.textView2);

            images.setImageResource(images1[position]);
            title.setText(title1[position]);
            date.setText(date1[position]);
            description.setText(description1[position]);

            return view;
        }
    }
}
