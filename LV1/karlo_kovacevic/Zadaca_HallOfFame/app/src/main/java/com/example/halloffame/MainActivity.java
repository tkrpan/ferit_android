package com.example.halloffame;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    String mTitle[] = {"Lionel Messi", "Michael Schumacher", "Michael Jordan"};
    String mYears[] = {"1987.", "1969.", "1963."};
    String mDesciption[] = {"Lionel Andrés Messi argentinski je nogometaš koji trenutačno igra za španjolsku Barcelonu i Argentinsku nogometnu reprezentaciju. Messi se smatra najboljim nogometašem na svijetu, također i svih vremena.", "Michael Schumacher, njemački vozač Formule 1 iz Kerpena u Porajnju. Michael je stariji brat Ralfa Schumachera, koji je također bio vozač Formule 1. Najuspješniji vozač Formule 1 svih vremena.", "Michael Jeffrey Jordan umirovljeni je američki profesionalni košarkaš. Igrao je na poziciji bek šutera, a izabran je u 1. krugu NBA drafta 1984. od strane Chicago Bullsa."};
    int images[] = {R.drawable.messi, R.drawable.schumacher, R.drawable.jordan};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        MyAdapter adapter = new MyAdapter(this, mTitle, mYears, mDesciption, images);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    Toast.makeText(MainActivity.this, "Barcelona", Toast.LENGTH_LONG).show();
                }
                if (position == 1){
                    Toast.makeText(MainActivity.this, "Ferrari", Toast.LENGTH_LONG).show();
                }
                if (position == 2){
                    Toast.makeText(MainActivity.this, "Chicago Bulls", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    class MyAdapter extends ArrayAdapter<String>{

        Context context;
        String rTitle[];
        String rYears[];
        String rDesciption[];
        int rImages[];

        MyAdapter(Context c, String title[], String years[], String description[], int imgs[]){
            super(c, R.layout.row, R.id.textView1, title);
            this.context = c;
            this.rTitle = title;
            this.rYears = years;
            this.rDesciption = description;
            this.rImages = imgs;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row, parent, false);
            ImageView images = row.findViewById(R.id.image);
            TextView myTitle = row.findViewById(R.id.textView1);
            TextView myYears = row.findViewById(R.id.textView2);
            TextView myDescription = row.findViewById(R.id.textView3);

            images.setImageResource(rImages[position]);
            myTitle.setText(rTitle[position]);
            myYears.setText(rYears[position]);
            myDescription.setText(rDesciption[position]);


            return row;
        }
    }
}

