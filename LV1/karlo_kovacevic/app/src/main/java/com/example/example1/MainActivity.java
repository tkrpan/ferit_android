package com.example.example1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;


import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText heightEditText;
    private EditText weightEditText;
    private TextView resultNumTextView, resultTxtTextView;
    private Button calcButton;
    private ImageView imageImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setView();
        calculateBMI(null);
    }

    private void setView(){
        heightEditText = findViewById(R.id.height);
        weightEditText = findViewById(R.id.weight);
        calcButton = findViewById(R.id.calc);
        resultNumTextView = findViewById(R.id.resultNum);
        resultTxtTextView = findViewById(R.id.resultTxt);
        imageImageView = findViewById(R.id.image);
    }

    public void calculateBMI(View v){
        String heightString = heightEditText.getText().toString();
        String weightString = weightEditText.getText().toString();
        String bmiLabel = "";

        if(heightString != null && !"".equals(heightString) && weightString != null && !"".equals(weightString)){
            double height = Double.parseDouble(heightString);
            double weight = Double.parseDouble(weightString);

            if(height < 2.51 && weight < 351) {
                double bmi = (weight / (height * height));
                bmi = Math.round(bmi * 10) / 10.0;

                if (bmi <= 18.0) {
                    bmiLabel = getString(R.string.underweight);
                    imageImageView.setBackgroundResource(R.drawable.underweight);
                } else if (bmi > 18 && bmi <= 23) {
                    bmiLabel = getString(R.string.healthy);
                    imageImageView.setBackgroundResource(R.drawable.healthy);
                } else if (bmi > 23 && bmi <= 28) {
                    bmiLabel = getString(R.string.overweight);
                    imageImageView.setBackgroundResource(R.drawable.overweight);
                } else if (bmi > 28) {
                    bmiLabel = getString(R.string.obese);
                    imageImageView.setBackgroundResource(R.drawable.obese);
                }

                resultNumTextView.setText(String.valueOf(bmi));
                resultTxtTextView.setText(bmiLabel);

                resultNumTextView.setVisibility(View.VISIBLE);
                resultTxtTextView.setVisibility(View.VISIBLE);
                imageImageView.setVisibility(View.VISIBLE);
            }
        }
    }
}

