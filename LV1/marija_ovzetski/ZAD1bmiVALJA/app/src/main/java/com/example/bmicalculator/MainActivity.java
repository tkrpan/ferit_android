package com.example.bmicalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


//Main activity class start here
public class MainActivity extends AppCompatActivity {

    EditText heightEditText, weightEditText;
    Button calcButton;
    TextView resultTextView, titleTextView, descTextView;
    ImageView descImageView;
    String resultValue, classValue, descriptionValue;

    //Define layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        heightEditText = findViewById(R.id.heightEditText);
        weightEditText = findViewById(R.id.weightEditText);
        resultTextView = findViewById(R.id.resultTextView);
        titleTextView = findViewById(R.id.titleTextView);
        descTextView = findViewById(R.id.descTextView);
        descImageView = findViewById(R.id.descImageView);
        calcButton = findViewById(R.id.calcButton);
    }

    public void calcBMI(View view){
        if(weightEditText.getText().length()==0){
            resultValue = "Enter your weight";
        }
        else if(heightEditText.getText().length()==0){
            resultValue = "Enter your height";
        }
        else {
            float weight = Float.parseFloat(weightEditText.getText().toString());
            float height = Float.parseFloat(heightEditText.getText().toString()) / 100;
            if(weight <= 0 || weight > 350){
                resultValue = "weight should be between 0kg and 350kg";
            }
            else if(height <= 0 || height > 350){
                resultValue = "height should be between 0cm and 250cm";
            }
            else {
                float bmi = weight / (height * height);
                resultValue = "BMI: " + bmi;

                if(bmi < 18.5){
                    classValue = "underweight";
                    descriptionValue = "Nutritional deficiencies: if you're underweight, it's likely that you're not consuming a healthy, balanced diet, which can lead to you lacking nutrients that your body needs to work properly.";
                    descImageView.setImageResource(R.mipmap.wc1);
                } else if(bmi < 25){
                    classValue = "normal";
                    descriptionValue = "Your BMI is normal. Keep it!";
                    descImageView.setImageResource(R.mipmap.wc2);
                }else if(bmi < 30){
                    classValue = "overweight";
                    descriptionValue = "Overweight. ... Being overweight or fat is having more body fat than is optimally healthy. Being overweight is especially common where food supplies are plentiful and lifestyles are sedentary.";
                    descImageView.setImageResource(R.mipmap.wc3);
                }else {
                    classValue = "obese";
                    descriptionValue = "Obesity is a medical condition in which excess body fat has accumulated to an extent that it may have a negative effect on health.";
                    descImageView.setImageResource(R.mipmap.wc4);
                }
                descTextView.setText(descriptionValue);
                descTextView.setVisibility(View.VISIBLE);
                titleTextView.setText(classValue);
                titleTextView.setVisibility(View.VISIBLE);
                descImageView.setVisibility(View.VISIBLE);

            }
        }
        resultTextView.setText(resultValue);
        resultTextView.setVisibility(View.VISIBLE);
    }
}