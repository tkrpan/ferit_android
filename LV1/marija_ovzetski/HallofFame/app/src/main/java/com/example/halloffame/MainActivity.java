package com.example.halloffame;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgSindhu;
    ImageView imgNehwal;
    ImageView imgWei;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intializeUI();
    }

    private void intializeUI() {
        this.imgSindhu=(ImageView)findViewById(R.id.imgSindhu);
        this.imgSindhu.setOnClickListener(this);
        this.imgNehwal=(ImageView)findViewById(R.id.imgNehwal);
        this.imgNehwal.setOnClickListener(this);
        this.imgWei=(ImageView)findViewById(R.id.imgWei);
        this.imgWei.setOnClickListener(this);
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.imgSindhu:
                Toast.makeText(this, R.string.tSindhu, Toast.LENGTH_SHORT).show();
                break;
            case R.id.imgNehwal:
                Toast.makeText(this, R.string.tNehwal, Toast.LENGTH_SHORT).show();
                break;
            case R.id.imgWei:
                Toast.makeText(this, R.string.tWei, Toast.LENGTH_SHORT).show();
                break;
        }
    }
}


