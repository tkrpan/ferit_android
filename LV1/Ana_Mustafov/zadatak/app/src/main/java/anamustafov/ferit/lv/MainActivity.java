package anamustafov.ferit.lv;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    CardView cardYuji, cardLeon, cardAleks;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }


    private void initializeUI(){
        this.cardYuji  = (CardView) findViewById(R.id.cardYuji);
        this.cardLeon   = (CardView) findViewById(R.id.cardLeon);
        this.cardAleks = (CardView) findViewById(R.id.cardAleks);

        this.cardYuji.setOnClickListener(this);
        this.cardLeon.setOnClickListener(this);
        this.cardAleks.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
        Bundle bundle = new Bundle();

        switch(v.getId()){
            case R.id.cardYuji:
                bundle.putString("info", getString(R.string.Yujiabo));
                bundle.putString("image", "yuji");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.cardLeon:
                bundle.putString("info", getString(R.string.Leonabo));
                bundle.putString("image", "leon");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.cardAleks:
                bundle.putString("info", getString(R.string.Aleksabo));
                bundle.putString("image", "aleks");
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }
}