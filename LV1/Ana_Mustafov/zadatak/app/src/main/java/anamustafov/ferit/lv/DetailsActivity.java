package anamustafov.ferit.lv;

import android.annotation.SuppressLint;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;


@SuppressLint("Registered")
public class DetailsActivity extends AppCompatActivity {

    ImageView PlayerImage;
    TextView details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initializeUI();
    }

    private void initializeUI(){
        String info, image;
        info  ="";
        image ="";
        this.details = (TextView) findViewById(R.id.info);
        this.PlayerImage = (ImageView) findViewById(R.id.image);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            info = bundle.get("info").toString();
            image = bundle.get("image").toString();
        }

        switch(image){
            case ("Yuji"):
                PlayerImage.setImageResource(R.drawable.yuji);
                details.setText(info);
                break;

            case("Leon"):
                PlayerImage.setImageResource(R.drawable.leon);
                details.setText(info);
                break;

            case("Aleks"):
                PlayerImage.setImageResource(R.drawable.aleks);
                details.setText(info);
                break;
        }
    }
}
