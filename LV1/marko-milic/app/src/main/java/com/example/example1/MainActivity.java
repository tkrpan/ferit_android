package com.example.example1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText weightEditText;
    private EditText heightEditText;
    private Button calcButton;
    private TextView rezTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();
    }

    private void setView(){

        weightEditText = findViewById(R.id.Weight);
        heightEditText = findViewById(R.id.Height);

        calcButton = findViewById(R.id.Calculate);
        calcButton.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view){

                        double tezina = Double.parseDouble(weightEditText.getText().toString());
                        double visina = Double.parseDouble(heightEditText.getText().toString());
                        if(visina<0 || visina>2.5){
                            heightEditText.setText("");
                            heightEditText.setHint("Height must be above 0 and below 2.5 meters");

                        }
                        else if(tezina<0 || tezina>350){
                            weightEditText.setText("");
                            weightEditText.setHint("Weight must be above 0 and below 350 kilograms");

                        } else {
                            double ukupno = tezina / Math.pow(visina, 2);
                            rezTextView = findViewById(R.id.Rezultat);
                            rezTextView.setText(Double.toString(ukupno));
                        }
                    }
                }
        );

    }

}
