package com.example.zadaca;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ImageView ritchieImageView;
    private ImageView linusImageView;
    private ImageView turingImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();
    }

    private void setView() {

        ritchieImageView = findViewById(R.id.ritchie);
        linusImageView = findViewById(R.id.linus);
        turingImageView = findViewById(R.id.turing);

        ritchieImageView.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "September 9, 1941", Toast.LENGTH_SHORT).show();
                    }
                }
        );

        linusImageView.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "December 28, 1969", Toast.LENGTH_SHORT).show();
                    }
                }
        );

        turingImageView.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "June 23, 1912", Toast.LENGTH_SHORT).show();
                    }
                }
        );

    }

}
