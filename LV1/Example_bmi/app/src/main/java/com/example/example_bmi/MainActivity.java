package com.example.example_bmi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    EditText etHeight;
    EditText etWeight;
    Button bCalculate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etHeight = (EditText) findViewById(R.id.etHeight);
        etWeight = (EditText) findViewById(R.id.etWeight);
        bCalculate = (Button) findViewById(R.id.bCalculate);

        bCalculate.setOnClickListener((View.OnClickListener) this);
    }

    @Override
    public void onClick(View v){
        double height = Double.parseDouble(etHeight.getText().toString());
        double weight = Double.parseDouble(etWeight.getText().toString());
        double bmi = weight/(height*height);

        String message= String.valueOf(bmi);
        Toast.makeText(this,message, Toast.LENGTH_LONG ).show();
        Log.d("TAG", message);
    }
}
