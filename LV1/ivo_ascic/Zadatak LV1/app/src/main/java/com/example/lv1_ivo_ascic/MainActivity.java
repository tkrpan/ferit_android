﻿package com.example.lv1_ivo_ascic;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText heightEditText;
    private EditText weightEditText;
    private Button calculateButton;
    private TextView resultTextView;
    String calculation, BMIresult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setView();
    }
   //TODO
    //value from heightEditText -> int
    //value from weightEditText -> int
    //onClick calculateButton res=h/w^2
    //set res on resultTextView

    private void setView(){

        heightEditText = findViewById(R.id.heightEditText);
        weightEditText = findViewById(R.id.weightEditText);
        calculateButton = findViewById(R.id.calculateButton);
        resultTextView = findViewById(R.id.resultTextView);
    }

    public void calculateBMI(View view){

        String s1 = heightEditText.getText().toString();
        String s2 = weightEditText.getText().toString();

        float heightValue = Float.parseFloat(s1);
        float weightValue = Float.parseFloat(s2);

        float bmi = weightValue / (heightValue * heightValue);

        if(bmi<16){
            BMIresult = "Pothranjen";
        }else if(bmi < 18.5){
            BMIresult = "Mršav";
        }else if(bmi >= 18.5 && bmi <= 24.9){
            BMIresult = "Normalna težina";
        }else if(bmi >= 25 && bmi <= 29.9){
            BMIresult = "Debeo";
        }else{
            BMIresult = "Pretil";
        }

        calculation = "Rezultat: \n\n" + bmi + "\n" + BMIresult;

        resultTextView.setText(calculation);
    }
}
