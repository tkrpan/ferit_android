package com.example.halloffame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ListView mListView;
    int[] images = {R.drawable.image1, R.drawable.image2, R.drawable.image3};

    String[] names ={"Michael M. Watkins", "Elon Musk", "Jim Bridenstine"};
    String[] desc ={"An American engineer and scientist, director of NASA's Jet Propulsion Laboratory in Pasadena, California, and a vice president of" +
            " the California Institute of Technology", "An engineer, industrial designer, technology entrepreneur, and philanthropist.", "An American " +
            "politician and the Administrator of the National Aeronautics and Space Administration (NASA)"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = findViewById(R.id.listView);

        CustomAdaptor customAdaptor = new CustomAdaptor();
        mListView.setAdapter(customAdaptor);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    Intent intent = new Intent(view.getContext(), Activity_Michael.class);
                    startActivity(intent);
                }
                if(position == 1){
                    Intent intent = new Intent(view.getContext(), Activity_Elon.class);
                    startActivity(intent);
                }
                if(position == 2){
                    Intent intent = new Intent(view.getContext(), Activity_Jim.class);
                    startActivity(intent);
                }
            }
        });
    }

    class CustomAdaptor extends BaseAdapter{


        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = getLayoutInflater().inflate(R.layout.customlayout, null);
            ImageView mImageView = view.findViewById(R.id.imageView);
            TextView mTextView = view.findViewById(R.id.textView);
            TextView mTextViewDesc = view.findViewById(R.id.textViewDesc);

            mImageView.setImageResource(images[position]);
            mTextView.setText(names[position]);
            mTextViewDesc.setText(desc[position]);
            return view;
        }

    }
}
