package com.example.example1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText visina;
    private EditText tezina;
    private TextView rezultat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        visina = (EditText) findViewById(R.id.visina);
        tezina = (EditText) findViewById(R.id.tezina);
        rezultat = (TextView) findViewById(R.id.rezultat);
    }

    public void izracunajBMI(View v) {
        String visinaStr = visina.getText().toString();
        String tezinaStr = tezina.getText().toString();

        if (visinaStr != null && !"".equals(visinaStr)
                && tezinaStr != null  &&  !"".equals(tezinaStr)) {
            float visinaValue = Float.parseFloat(visinaStr) / 100;
            float tezinaValue = Float.parseFloat(tezinaStr);

            float bmi = tezinaValue / (visinaValue * visinaValue);

            displayBMI(bmi);
        }
    }

    private void displayBMI(float bmi) {
        String bmiLabel = "";

        if (Float.compare(bmi, 15f) <= 0) {
            bmiLabel = getString(R.string.pothranjenost);
        } else if (Float.compare(bmi, 15f) > 0  &&  Float.compare(bmi, 16f) <= 0) {
            bmiLabel = getString(R.string.idealna_tezina);
        } else if (Float.compare(bmi, 16f) > 0  &&  Float.compare(bmi, 18.5f) <= 0) {
            bmiLabel = getString(R.string.prekomjerna_tjelesna_tezina);
        } else if (Float.compare(bmi, 18.5f) > 0  &&  Float.compare(bmi, 25f) <= 0) {
            bmiLabel = getString(R.string.pretilost);
        } else {
            bmiLabel = getString(R.string.jaka_pretilost);
        }

        bmiLabel = bmi + "\n\n" + bmiLabel;
        rezultat.setText(bmiLabel);
    }
}
