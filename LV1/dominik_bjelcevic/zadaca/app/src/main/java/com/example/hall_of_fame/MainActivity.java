package com.example.hall_of_fame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    // For list view
    int[] images = {R.drawable.kobe, R.drawable.lebron, R.drawable.westbrook};
    String[] name = {"Kobe Bryant", "Lebron James", "Russell Westbrook"};
    String[] description = {"1978-2020, Igrao je na poziciji bek šutera i bio je član NBA momčadi Los Angeles Lakersa kroz svoju cijelu 20-godisnju karijeru. \n" +
            "\n", "1984,  Igra na poziciji niskog krila, a od ljeta 2018. je član NBA momčadi Los Angeles Lakers. Izabran je u 1. krugu (1. ukupno).\n" +
            "\n", "1988,  Igra na poziciji razigravača, a trenutačno je član NBA momčadi Houston Rocketsa. Izabran je u 1. krugu (4. ukupno).\n" +
            "\n"};
    String[] quotes = {"Los Angeles Lakers","Cleveland Cavaliers","Oklahoma City Thunder"};

    ListView lView;
    ListAdapter lAdapter;

    // For activities
    private Button nav1,nav2,nav3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // For List View
        lView = findViewById(R.id.androidList);
        lAdapter = new ListAdapter(MainActivity.this, name, description, images);
        lView.setAdapter(lAdapter);
        lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Toast.makeText(MainActivity.this, quotes[i], Toast.LENGTH_LONG).show();

            }
        });

        nav1 =  findViewById(R.id.nav1);
        nav1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity1();
            }
        });

        nav2 =  findViewById(R.id.nav2);
        nav2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity2();
            }
        });

        nav3 =  findViewById(R.id.nav3);
        nav3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity3();
            }
        });
    }


    public void openActivity1() {
        Intent intent = new Intent(this, Activity1.class);
        startActivity(intent);
    }

    public void openActivity2() {
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }

    public void openActivity3() {
        Intent intent = new Intent(this, Activity3.class);
        startActivity(intent);
    }
}
