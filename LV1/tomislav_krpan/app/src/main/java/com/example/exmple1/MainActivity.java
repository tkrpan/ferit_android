package com.example.exmple1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG = "exmple1";

    private EditText heightEditText;
    private EditText weightEditText;
    private Button calcButton;
    private TextView resultTextView;
    private TextView titleTextView;
    private TextView descTextView;
    private ImageView descImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(LOG_TAG, "MainActivity onCreate");

        setView();
    }

    /*
    Helpers
     */
    private void setView(){
        Log.d(LOG_TAG, "MainActivity setView");
        heightEditText = findViewById(R.id.heightEditText);
        weightEditText = findViewById(R.id.weightEditText);
        calcButton = findViewById(R.id.calcButton);
        resultTextView = findViewById(R.id.resultTextView);
        titleTextView = findViewById(R.id.titleTextView);
        descTextView = findViewById(R.id.descTextView);
        descImageView = findViewById(R.id.descImageView);

        calcButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.d(LOG_TAG, "MainActivity onClick");

        String heightString = heightEditText.getText().toString();
        String weightString = weightEditText.getText().toString();

        Log.d(LOG_TAG, "MainActivity onClick heightString " + heightString);
        Log.d(LOG_TAG, "MainActivity onClick heightString " + weightString);

        if(heightString != null && !"".equals(heightString)
                && weightString != null && !"".equals(weightString)){
            double height = Double.parseDouble(heightString);
            double weight = Double.parseDouble(weightString);
            //checkInputValues(height, weight)
            double resultBMI = calculateBMI(height, weight);
            Log.d(LOG_TAG, "MainActivity onClick resultBMI " + resultBMI);
            showResults(resultBMI);
        }
        //TODO
        // set description on descTextView
    }

    private double calculateBMI(double height, double weight){
        return weight/(height*height);
    }

    private void showResults(double resultBMI){
        resultTextView.setText(String.format("%.2f", resultBMI));
        String title = "";
        if(resultBMI > 30){
            title = "Debeo/la si";
        }

        descImageView.setVisibility(View.VISIBLE);
        titleTextView.setText(title);
        descTextView.setText("Opis zasto si debeo ...");
        //descImageView.setImageDrawable(getResources()
        //        .getDrawable(R.drawable.ic_launcher_background));
    }

    @Override
    protected void onDestroy() {
        Log.d(LOG_TAG, "MainActivity onDestroy");
        super.onDestroy();
    }
}
