package com.example.bmicalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText weight, height;
    TextView resulttext, descText;
    String calculation, BMIresult;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        weight = findViewById(R.id.weight);
        height = findViewById(R.id.height);
        resulttext = findViewById(R.id.result);
        image = findViewById(R.id.descImageView);
        descText = findViewById(R.id.descTextView);
    }

    public void calculateBMI(View view) {
        String S1 = weight.getText().toString();
        String S2 = height.getText().toString();

        float weightValue = Float.parseFloat(S1);
        float heightValue = Float.parseFloat(S2) / 100;


        float bmi = weightValue/(heightValue*heightValue);
        if(bmi<16){
            BMIresult = "Premršavi ste!";
        }else if(bmi < 18.5){
            BMIresult = "Mršavi ste!";
        }else if(bmi >= 18.5 && bmi <= 24.9){
            BMIresult="Idealni ste!";
        }else if(bmi >= 25 && bmi <= 29.9){
            BMIresult = "Malo ste debeljuškasti!";
        }else{
            BMIresult = "Ovo nije baš najbolje, malo je predebelo!";
        }
        calculation = "Result: \n\n" + bmi + "\n" + BMIresult;
        resulttext.setText(calculation);

        if(bmi<16){
            descText.setText("Obratite se svome liječniku kako biste povećali tjelesnu težinu.");
        }else if(bmi < 18.5){
            descText.setText("Nije toliko strašno, ali obratite se svome liječniku kako biste povećali tjelesnu težinu.");
        }else if(bmi >= 18.5 && bmi <= 24.9){
            descText.setText("Hranite se zdravo te se bavite tjelesnom aktivnošću kako biste ju održali.");
        }else if(bmi >= 25 && bmi <= 29.9){
            descText.setText("Obratite se liječniku kako biste smanjili tjelesnu težinu te rizik za razvoj povezanih bolesti");
        }else{
            descText.setText("Pretili ste! Obratite se liječniku kako biste smanjili tjelesnu težinu te rizik za razvoj povezanih bolesti.");
        }

        if(bmi<16){
            image.setBackgroundResource(R.drawable.a);
        }else if(bmi < 18.5){
            image.setBackgroundResource(R.drawable.b);
        }else if(bmi >= 18.5 && bmi <= 24.9){
            image.setBackgroundResource(R.drawable.c);
        }else if(bmi >= 25 && bmi <= 29.9){
            image.setBackgroundResource(R.drawable.d);
        }else{
            image.setBackgroundResource(R.drawable.e);
        }
        image.setVisibility(View.VISIBLE);

    }
}
