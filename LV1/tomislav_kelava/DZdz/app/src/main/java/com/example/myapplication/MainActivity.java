package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String mIme[] = {"Dwyane Wade", "LeBron James", "Dražen Petrović"};
    String mDatum[] = {"17. siječnja 1982.", "30. prosinca 1984.", "22. listopada 1964."};
    String mOpis[] = {"Dwyane Tyrone Wade, Jr. američki je umirovljeni profesionalni košarkaš. Igrao je na poziciji bek šutera, te je bivši član NBA momčadi Miami Heata.",
                    "LeBron Raymone James američki profesionalni je košarkaš. Igra na poziciji niskog krila, a od ljeta 2018. je član NBA momčadi Los Angeles Lakers. ",
                    "Dražen Petrović, bio je hrvatski košarkaš. Bio je jedan od najvećih hrvatskih i svjetskih košarkaša te je uvršten među 50 osoba koji su najviše pridonijeli Euroligi. Spada u red najvećih hrvatskih sportaša svih vremena. Smatra se predvodnikom vala europskih košarkaša u NBA. "};
    int images[] = {R.drawable.wade, R.drawable.lebron, R.drawable.drazen};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        MyAdapter adapter = new MyAdapter(this, mIme, mDatum, mOpis, images);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    Toast.makeText(MainActivity.this, "Miami Heat", Toast.LENGTH_SHORT).show();
                }
                if (position==1){
                    Toast.makeText(MainActivity.this, "Los Angeles Lakers", Toast.LENGTH_SHORT).show();
                }
                if (position==2){
                    Toast.makeText(MainActivity.this, "New Jersey Nets", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    class MyAdapter extends ArrayAdapter<String>{
        Context context;
        String rIme[];
        String rDatum[];
        String rOpis[];
        int rImages[];

        MyAdapter(Context c, String ime[], String datum[], String opis[], int image[] ){
            super(c, R.layout.row, R.id.textView1, ime);
            this.context = c;
            this.rIme = ime;
            this.rDatum = datum;
            this.rOpis = opis;
            this.rImages = image;

        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row, parent, false);
            ImageView images = row.findViewById(R.id.image);
            TextView myName = row.findViewById(R.id.textView1);
            TextView myDate = row.findViewById(R.id.textView2);
            TextView myDescription = row.findViewById(R.id.textView3);

            images.setImageResource(rImages[position]);
            myName.setText(rIme[position]);
            myDate.setText(rDatum[position]);
            myDescription.setText(rOpis[position]);


            return row;
        }
    }
}
