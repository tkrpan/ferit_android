package hr.ferit.budano.app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private TextView bmi, status, message;
    private EditText heightInMeters, weightInKilograms;
    private Button btnCalculate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();
        calculateBMI();
    }

    private void setView() {
        this.heightInMeters = findViewById(R.id.heightInMeters);
        this.weightInKilograms = findViewById(R.id.weightInKilograms);
        this.bmi = findViewById(R.id.bmi);
        this.status = findViewById(R.id.status);
        this.message = findViewById(R.id.message);
        this.btnCalculate = findViewById(R.id.btnCalculate);
    }

    private void calculateBMI() {
        this.btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Double height = Double.parseDouble(heightInMeters.getText().toString());
                Double weight = Double.parseDouble(weightInKilograms.getText().toString());
                Double result = weight / (height * height);
                Double rounded = Math.round(result * 10) / 10.0;
                bmi.setText("YOUR BMI: " + Double.toString(rounded));
                bmi.setVisibility(View.VISIBLE);


                if (rounded < 18.5) { status.setText("UNDERWEIGHT"); }
                else if (rounded >= 18.5 && rounded < 23) { status.setText("HEALTHY"); }
                else if (rounded >= 23 && rounded < 27.5) { status.setText("OVERWEIGHT");}
                else if (rounded >= 27.5) { status.setText("OBESE");}
                status.setVisibility(View.VISIBLE);


                if (rounded < 18.5) { message.setText("You need to put on weight! :)"); }
                else if (rounded >= 18.5 && rounded < 23) { message.setText("You seem to be healthy! :D"); }
                else if (rounded >= 23 && rounded < 27.5) { message.setText("You should lose a little weight! :)"); }
                else if (rounded >= 27.5) { message.setText("You're very unhealthy, you need to lose weight! :("); }
                message.setVisibility(View.VISIBLE);
            }
        });
    }

}
