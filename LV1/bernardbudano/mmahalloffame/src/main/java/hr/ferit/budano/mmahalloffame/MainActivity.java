package hr.ferit.budano.mmahalloffame;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ImageView imgCroCop;
    private ImageView imgFedor;
    private ImageView imgConor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();
    }

    private void setView() {

        imgCroCop = findViewById(R.id.crocop);
        imgFedor = findViewById(R.id.fedor);
        imgConor = findViewById(R.id.conor);

        imgCroCop.setOnClickListener(
                new View.OnClickListener() {
            public void onClick (View view){
                Toast.makeText(getApplicationContext(), "Right leg hospital, left leg cemetery.", Toast.LENGTH_SHORT).show();
            }
        }
            );

        imgFedor.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "The one who doesn't fall, doesn't stand up.", Toast.LENGTH_SHORT).show();
                    }
                }
        );

        imgConor.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Precision beats power, timing beats speed.", Toast.LENGTH_SHORT).show();
                    }
                }
        );

    }
}
