package hr.ferit.patrikvinicki.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    CardView cardRogerFederer, cardRafaelNadal, cardNovakDjokovic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }


    private void initializeUI(){
        this.cardRogerFederer  = (CardView) findViewById(R.id.cardRogerFederer);
        this.cardRafaelNadal   = (CardView) findViewById(R.id.cardRafaelNadal);
        this.cardNovakDjokovic = (CardView) findViewById(R.id.cardNovakDjokovic);

        this.cardRogerFederer.setOnClickListener(this);
        this.cardRafaelNadal.setOnClickListener(this);
        this.cardNovakDjokovic.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
        Bundle bundle = new Bundle();

        switch(v.getId()){
            case R.id.cardRogerFederer:
                bundle.putString("info", getString(R.string.rogerFedererInfo));
                bundle.putString("image", "RogerFederer");
                intent.putExtras(bundle);
                //intent.putExtra("info", getResources().getString(R.string.rogerFedererInfo));
                //intent.putExtra("image", "RogerFederer");
                startActivity(intent);
                break;

            case R.id.cardRafaelNadal:
                bundle.putString("info", getString(R.string.rafaelNadalInfo));
                bundle.putString("image", "RafaelNadal");
                intent.putExtras(bundle);
                //intent.putExtra("info", getResources().getString(R.string.rafaelNadalInfo));
                //intent.putExtra("image", "RafaelNadal");
                startActivity(intent);
                break;

            case R.id.cardNovakDjokovic:
                bundle.putString("info", getString(R.string.novakDjokovicInfo));
                bundle.putString("image", "NovakDjokovic");
                intent.putExtras(bundle);
                //intent.putExtra("info", getResources().getString(R.string.novakDjokovicInfo));
                //intent.putExtra("image", "NovakDjokovic");
                startActivity(intent);
                break;
        }
    }
}
