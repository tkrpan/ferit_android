package hr.ferit.patrikvinicki.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    ImageView PlayerImage;
    TextView details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initializeUI();
    }

    private void initializeUI(){
        String info, image;
        info  ="";
        image ="";
        this.details = (TextView) findViewById(R.id.info);
        this.PlayerImage = (ImageView) findViewById(R.id.image);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            info = bundle.get("info").toString();
            image = bundle.get("image").toString();
        }

        switch(image){
            case ("RogerFederer"):
                PlayerImage.setImageResource(R.drawable.roger_federer);
                details.setText(info);
                break;

            case("RafaelNadal"):
                PlayerImage.setImageResource(R.drawable.rafael_nadal);
                details.setText(info);
                break;

            case("NovakDjokovic"):
                PlayerImage.setImageResource(R.drawable.novak_djokovic);
                details.setText(info);
                break;
        }
    }
}
