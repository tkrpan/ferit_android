package com.example.nesta;

import androidx.appcompat.app.AppCompatActivity;

import android.media.audiofx.DynamicsProcessing;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.sql.BatchUpdateException;

public class MainActivity extends AppCompatActivity {

    private EditText editTextheight;
    private EditText editTextweight;
    private Button buttonCalculate;
    private TextView textViewResult,textviewAbout;
    private float height,weight,bmi;
    String resultValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("TAG","MainActivity onCreate");


        //textViewResult.setText(Calculate("10","2"));
        editTextheight=findViewById(R.id.editTextheight);
        editTextweight=findViewById(R.id.editTextweight);
        buttonCalculate=findViewById(R.id.buttonCalculate);
        textViewResult=findViewById(R.id.textViewResult);
        textviewAbout=findViewById(R.id.textViewAbout);

    }






    public void Calculate(View view) {
        if(editTextweight.length()==0){
            resultValue="ur lyingW";
        } else if(editTextheight.length()==0){
            resultValue="ur lyingH";
        }else{
            height=Float.parseFloat(editTextheight.getText().toString())/100;
            weight=Float.parseFloat(editTextweight.getText().toString());
            float resultbmi = weight / (height * height);

            if(resultbmi < 18.5){
                resultValue  = "underweight (BMI):" + resultbmi;
                textviewAbout.setText(getString(R.string.underweight));
                textviewAbout.setVisibility(View.VISIBLE);
                //weightClassImage.setImageResource(R.mipmap.wc1);
            } else if(resultbmi < 25){
                resultValue  = "normal (BMI):" + resultbmi;
                textviewAbout.setText(getString(R.string.normal));
                textviewAbout.setVisibility(View.VISIBLE);
                //weightClassImage.setImageResource(R.mipmap.wc2);
            } else if(resultbmi < 30){
                resultValue  = "overweight (BMI):" + resultbmi;
                textviewAbout.setText(getString(R.string.overweight));
                textviewAbout.setVisibility(View.VISIBLE);
                //weightClassImage.setImageResource(R.mipmap.wc3);
            } else {
                resultValue  = "obese (BMI):" + resultbmi;
                textviewAbout.setText(getString(R.string.obese));
                textviewAbout.setVisibility(View.VISIBLE);
                //weightClassImage.setImageResource(R.mipmap.wc4);
            }
        }

        textViewResult.setVisibility(View.VISIBLE);
        textViewResult.setText(resultValue);
    }
}
