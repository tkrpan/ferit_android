package com.example.recyclerviewapp;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyCustomAdapter extends RecyclerView.Adapter<MyCustomAdapter.ViewHolder>{

    private String TAG = "MyCustomAdapter";
    private OnItemClickListener listener;
    private ArrayList<String> values;

    public MyCustomAdapter(OnItemClickListener listener, ArrayList<String> values) {
        this.listener = listener;
        this.values = values;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtHeader;
        public TextView txtFooter;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtHeader = itemView.findViewById(R.id.txtViewHeader);
            txtFooter = itemView.findViewById(R.id.txtViewFooter);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_adapter, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        String txtString = values.get(position);

        holder.txtHeader.setText(txtString);
        holder.txtFooter.setText("Footer: " + txtString);

        holder.txtHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "MyCustomAdapter position: " + position);
                Log.d(TAG, "MyCustomAdapter values.size(): " + values.size());
                //removeItem(position);
                listener.onItemSelected(position);
                Log.d(TAG, "MyCustomAdapter values.size(): " + values.size());
            }
        });
    }

    private void removeItem(int position){
        values.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public interface OnItemClickListener {
        void onItemSelected(int position);
    }
}
