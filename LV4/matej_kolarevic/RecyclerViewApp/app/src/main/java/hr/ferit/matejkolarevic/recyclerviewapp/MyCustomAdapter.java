package hr.ferit.matejkolarevic.recyclerviewapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyCustomAdapter extends RecyclerView.Adapter<MyCustomAdapter.ViewHolder> {

    private ArrayList<String> values;
    private OnItemClickListener listener;

    public MyCustomAdapter(MainActivity listener, ArrayList<String> values){
        this.values = values;
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtHeader;
        public TextView txtFooter;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtHeader = itemView.findViewById(R.id.firstText);
            txtFooter = itemView.findViewById(R.id.secondText);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        String txtString = values.get(position);

        holder.txtHeader.setText(txtString);
        holder.txtFooter.setText("Footer: " + txtString);

        holder.txtHeader.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //removeItem(position);
                listener.onItemSelected(position);
            }
        });
    }

    private void removeItem(int position){
        values.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public interface OnItemClickListener{
        void onItemSelected(int position);
    }
}
