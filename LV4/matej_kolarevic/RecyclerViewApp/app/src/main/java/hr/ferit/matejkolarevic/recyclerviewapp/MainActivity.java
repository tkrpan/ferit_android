package hr.ferit.matejkolarevic.recyclerviewapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MyCustomAdapter.OnItemClickListener {

    private String TAG = "MainActivity";
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<String> values;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        values = new ArrayList<>();

        for(int i = 0; i<100; i++){
            values.add("Test " + i);
        }

        myCustomAdapter = new MyCustomAdapter(this, values);
        recyclerView.setAdapter(myCustomAdapter);
    }

    @Override
    public void onItemSelected(int position) {
        values.remove(position);
        recyclerView.getAdapter().notifyDataSetChanged();
    }
}
