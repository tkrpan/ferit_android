package com.example.lv4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MyCustomAdapter{

    private String TAG = "MainActivity";
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView recyclerView;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<String> values;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);

        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        values = new ArrayList<>();
        for(int i = 0; i < 100; i++){
            values.add("Item " + i);
        }

        myCustomAdapter = new MyCustomAdapter(this, values);
        recyclerView.setAdapter(myCustomAdapter);
    }

    @Override
    public void onItemSelected(int position){
        Log.d(TAG, "MainActivity position: " + position);
        Log.d(TAG, "MyCustomAdapter values.size(): " + values.size());
        values.remove(position);
        Log.d(TAG, "MyCustomAdapter values.size(): " + values.size());
        recyclerView.getAdapter().notifyDataSetChanged();
    }


}