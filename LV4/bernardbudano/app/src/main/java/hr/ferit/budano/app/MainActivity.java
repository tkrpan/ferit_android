package hr.ferit.budano.app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CustomAdapter.OnItemClickListener {

    private String LOG_TAG = "MainActivity";

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<String> values;

    private CustomAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        values = new ArrayList<>();
        for(int i = 0; i < 100; i++){
            values.add("Item " + i);
        }

        customAdapter = new CustomAdapter(values, this);
        recyclerView.setAdapter(customAdapter);
    }

    @Override
    public void onItemSelected(int position) {
        Log.d(LOG_TAG, "MainActivity position: " + position);
        values.remove(position);
        recyclerView.getAdapter().notifyDataSetChanged();
    }
}
