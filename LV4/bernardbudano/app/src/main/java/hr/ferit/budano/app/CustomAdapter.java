package hr.ferit.budano.app;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {

    private String LOG_TAG = "CustomAdapter";

    private OnItemClickListener onItemClickListener;

    private ArrayList<String> values;

    public CustomAdapter(ArrayList<String> values, OnItemClickListener onItemClickListener){
        this.values = values;
        this.onItemClickListener = onItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTitle;
        public TextView tvDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.title);
            tvDescription = itemView.findViewById(R.id.description);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        String text = values.get(position);

        holder.tvTitle.setText(text);
        holder.tvDescription.setText("Description: " + text);

        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "CustomAdapter position: " + position);
                //removeItem(position);
                onItemClickListener.onItemSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public void removeItem(int position){
        values.remove(position);
        notifyItemRemoved(position);
    }

    public interface OnItemClickListener{
        void onItemSelected(int position);
    }
}
