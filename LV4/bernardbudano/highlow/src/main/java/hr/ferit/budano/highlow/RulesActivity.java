package hr.ferit.budano.highlow;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class RulesActivity extends AppCompatActivity {

    TextView tvTitle, tvRules;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);

        tvTitle = findViewById(R.id.title);
        tvRules = findViewById(R.id.rules_text);
    }
}

