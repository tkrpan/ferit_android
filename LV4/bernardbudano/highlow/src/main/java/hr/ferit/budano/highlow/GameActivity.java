package hr.ferit.budano.highlow;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;


public class GameActivity extends AppCompatActivity {

    public static final int MAX_NUMBER = 100;
    private Random random = new Random();
    private TextView tvInfo;
    private EditText etGuess, etName;
    private Button btnGuess;
    private int computersNumber, tries;
    public static final String scoreboard_basic = "Score Table";
    public static final ArrayList<String> results = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findViewsById();

        btnGuess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkGuess();
            }
        });

        startNewGame();
    }

    private void findViewsById(){
        setContentView(R.layout.activity_game);
        tvInfo = findViewById(R.id.game_info);
        etGuess = findViewById(R.id.guess);
        btnGuess = findViewById(R.id.guess_button);
        etName = findViewById(R.id.name);
    }

    private void checkGuess(){
        int n = Integer.parseInt(etGuess.getText().toString());
        tries++;

        if(n < computersNumber){
            tvInfo.setText(R.string.too_low);
        } else if(n > computersNumber){
            tvInfo.setText(R.string.too_high);
        } else {
            int score = 100 - tries;

            String name = etName.getText().toString();
            if(name.isEmpty() || name.equals("")){
                name="Anonymous";
            }

            Toast.makeText(this,
                    "Congratulations! You guessed the number I was thinking of in " + tries + " tries!", Toast.LENGTH_LONG).show();
            results.add(name + ": " + score);
            startNewGame();
        }
    }

    private void startNewGame() {
        computersNumber = random.nextInt(MAX_NUMBER) + 1;
        tvInfo.setText(R.string.game_info);
        etGuess.setText("");
        etName.setText("");
        tries = 0;
    }
}

