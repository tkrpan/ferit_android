package hr.ferit.budano.highlow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    private Button btnGame, btnRules, btnScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewsById();
        setOnClickListeners();
    }

    public void findViewsById(){
        btnGame = findViewById(R.id.game);
        btnScore = findViewById(R.id.score);
        btnRules = findViewById(R.id.rules);
    }

    public void setOnClickListeners(){
        btnGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGameActivity();
            }
        });

        btnScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startScoreActivity();
            }
        });

        btnRules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRulesActivity();
            }
        });
    }

    public void startGameActivity(){
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    public void startScoreActivity(){
        Intent intent = new Intent(this, ScoreActivity.class);
        startActivity(intent);
    }

    public void startRulesActivity(){
        Intent intent = new Intent(this, RulesActivity.class);
        startActivity(intent);
    }
}

