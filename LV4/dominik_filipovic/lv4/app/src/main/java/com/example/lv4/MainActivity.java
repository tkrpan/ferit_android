package com.example.lv4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    private Button initiate_game, initiate_rules, initiate_scores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initiate_game = findViewById(R.id.playgame);
        initiate_rules = findViewById(R.id.rules);
        initiate_scores = findViewById(R.id.viewscore);

        initiate_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity1();
            }
        });

        initiate_rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity2();
            }
        });

        initiate_scores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity3();
            }
        });

    }
    public void Activity1(){
        Intent intent = new Intent(this, InitiateGame.class);
        startActivity(intent);
    }

    public void Activity2(){
        Intent intent = new Intent(this, InitiateRules.class);
        startActivity(intent);
    }


    public void Activity3(){
        Intent intent = new Intent(this, InitiateScores.class);
        startActivity(intent);
    }
}

