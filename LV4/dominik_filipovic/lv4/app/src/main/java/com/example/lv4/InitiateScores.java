package com.example.lv4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class InitiateScores extends AppCompatActivity {

    ListView scoreboard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initiate_scores);

        scoreboard = findViewById(R.id.scoreboard);
        ArrayList<String> scoreboard_list = new ArrayList<>();
        scoreboard_list.add(InitiateGame.scoreboard_basic);

        scoreboard_list.addAll(InitiateGame.results);


        ArrayAdapter<String> array_adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, scoreboard_list);
        scoreboard.setAdapter(array_adapter);
    }
}
