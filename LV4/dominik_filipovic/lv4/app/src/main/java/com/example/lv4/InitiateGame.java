package com.example.lv4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class InitiateGame extends AppCompatActivity{

    public static final int MAX_NUMBER = 100;
    public static final Random RANDOM = new Random();
    private TextView msgText;
    private EditText numberEntered, name2;
    private int numberToFind, numberTries;
    public static final String scoreboard_basic = "The Table Of Puny Mortals";
    public static final ArrayList<String> results = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initiate_game);
        msgText = findViewById(R.id.status);
        numberEntered = findViewById(R.id.number);
        Button submit = findViewById(R.id.submit);
        name2 = findViewById(R.id.name);
        name2.requestFocus();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });

        newGame();
    }

    private void validate(){
        int n = Integer.parseInt(numberEntered.getText().toString());
        numberTries++;

        if(n == numberToFind) {
            int score = 1000 - (numberTries*10);
            String name = name2.getText().toString();
            if(name.isEmpty()){
                name="Unknown champion";
            }
            Toast.makeText(this, "Well done! You found the number " + numberToFind + " in " + numberTries + " " +
                    "tries, you're soul is sold for "+ numberTries + " dollars.", Toast.LENGTH_LONG).show();
            results.add(name + "         " + score + " points of Divine Cowardness");
            newGame();
        }
        else if (n > numberToFind) {
            msgText.setText(R.string.high);
        }
        else {
            msgText.setText(R.string.low);
        }
    }

    private void newGame() {
        numberToFind = RANDOM.nextInt(MAX_NUMBER) + 1;
        msgText.setText(R.string.status);
        numberEntered.setText("");
        name2.setText("");
        numberTries = 0;
    }
}
