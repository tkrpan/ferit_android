package com.example.guess_the_number;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class PlayGame extends AppCompatActivity{

    public static final int MAX_NUMBER = 100;
    public static final Random RANDOM = new Random();
    private TextView msgText;
    private EditText numberEntered, name2;
    private Button submit;
    private int numberToFind, numberTries;
    public static final String scoreboard_basic = "Score Table";
    public static final ArrayList<String> results = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);
        msgText = findViewById(R.id.status);
        numberEntered = findViewById(R.id.number);
        submit = findViewById(R.id.submit);
        name2 = findViewById(R.id.name);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });

        newGame();
    }

    private void validate(){
        int n = Integer.parseInt(numberEntered.getText().toString());
        numberTries++;

        if(n == numberToFind) {
            int score = 1000 - (numberTries*10);
            String name = name2.getText().toString();
            if(name.isEmpty()){
                name="Unknown";
            }
            Toast.makeText(this, "Congratulations ! You found the number " + numberToFind + " in " + numberTries + " " +
                    "tries", Toast.LENGTH_LONG).show();
            results.add(name + "     ----->     " + score);
            newGame();
        }
        else if (n > numberToFind) {
            msgText.setText(R.string.too_high);
        }
        else if (n < numberToFind) {
            msgText.setText(R.string.too_low);
        }
    }

    private void newGame() {
        numberToFind = RANDOM.nextInt(MAX_NUMBER) + 1;
        msgText.setText(R.string.status);
        numberEntered.setText("");
        name2.setText("");
        numberTries = 0;
    }
}
