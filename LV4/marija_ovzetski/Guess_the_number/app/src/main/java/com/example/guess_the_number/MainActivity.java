package com.example.guess_the_number;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    private Button play_game, show_rules, view_scores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        play_game = findViewById(R.id.playgame);
        show_rules = findViewById(R.id.rules);
        view_scores = findViewById(R.id.viewscore);

        play_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity1();
            }
        });

        show_rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity2();
            }
        });

        view_scores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity3();
            }
        });

    }

    public void openActivity2(){
        Intent intent = new Intent(this, ShowRules.class);
        startActivity(intent);
    }

    public void openActivity1(){
        Intent intent = new Intent(this, PlayGame.class);
        startActivity(intent);
    }

    public void openActivity3(){
        Intent intent = new Intent(this, ViewScore.class);
        startActivity(intent);
    }
}
