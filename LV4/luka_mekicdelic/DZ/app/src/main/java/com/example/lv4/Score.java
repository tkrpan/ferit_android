package com.example.lv4;

public class Score
{
    String name = "";
    String city = "";
    byte number = 0;
    int tries = 0;

    public Score(String name, String city, byte number, int tries)
    {
        this.name = name;
        this.city = city;
        this.number = number;
        this.tries = tries;
    }
}
