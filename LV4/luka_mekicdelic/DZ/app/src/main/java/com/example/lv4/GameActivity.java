package com.example.lv4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static java.lang.Math.random;

public class GameActivity extends AppCompatActivity
{
    private String LOG_TAG = "LV4 GameActivity";

    private EditText editTextName;
    private EditText editTextCity;
    private EditText editTextNumber;
    private Button buttonGuess;

    private PreferencePlayer preferencePlayer;

    private int tries;
    private byte randomNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        editTextName = findViewById(R.id.editTextName);
        editTextCity = findViewById(R.id.editTextCity);
        editTextNumber = findViewById(R.id.editTextNumber);
        buttonGuess = findViewById(R.id.buttonGuess);

        preferencePlayer = new PreferencePlayer();

        tries = 0;
        randomNumber = (byte) (random() * 100);
        Log.d(LOG_TAG,"Generated number: " + randomNumber);

        buttonGuess.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //For determining which inputs are invalid so that an appropriate toast message can be displayed
                //strikes[0] is name, strikes[1] is city, strikes[2] is number
                boolean error = false;
                boolean[] strikes = {false, false, false};

                String name = "";
                String city = "";
                int number = 0;

                String editTextNameString = editTextName.getText().toString();
                String editTextCityString = editTextCity.getText().toString();
                String editTextNumberString = editTextNumber.getText().toString();

                StringBuilder toastMessage = new StringBuilder();

                if(!editTextNameString.equals(""))
                    name = editTextNameString;
                else
                {
                    error = true;
                    strikes[0] = true;
                }
                if(!editTextCityString.equals(""))
                    city = editTextCityString;
                else
                {
                    error = true;
                    strikes[1] = true;
                }
                if(!editTextNumberString.equals(""))
                {
                    number = Integer.parseInt(editTextNumberString);
                    if(!(number<=100))
                    {
                        error = true;
                        strikes[2] = true;
                    }
                }
                else
                {
                    error = true;
                    strikes[2] = true;
                }

                if(!error)
                {
                    ++tries;
                    if (number == randomNumber)
                    {
                        ScoreDBHelper.getInstance(getApplicationContext()).insertScore(name,city,(byte)number,tries);
                        toastMessage.append("You guessed! Your result has been written in the table of scores.  Number of tries: ").append(tries);
                        tries = 0;
                        randomNumber = (byte) (random() * 100);
                        Log.d(LOG_TAG,"Generated number: " + randomNumber);
                    }
                    else
                    {
                        toastMessage.append("You guessed wrong. Your number is ");
                        if (number<randomNumber)
                            toastMessage.append("smaller than the generated number.");
                        else if (number>randomNumber)
                            toastMessage.append("larger than the generated number.");
                        toastMessage.append(" Number of tries: ").append(tries);
                    }
                }
                else
                {
                    toastMessage.append("ERROR: invalid ");

                    byte count = 0;
                    for (byte i=0; i<strikes.length; i++)
                    {
                        if(strikes[i])
                        {
                            if(count>0)
                            {
                                toastMessage.append(", ");
                            }
                            switch(i)
                            {
                                case 0:
                                    toastMessage.append("name");
                                    break;
                                case 1:
                                    toastMessage.append("city");
                                    break;
                                case 2:
                                    toastMessage.append("number");
                                    break;
                            }
                            count++;
                        }
                    }

                    toastMessage.append(". Check your input.");
                }

                preferencePlayer.savePlayer(getApplicationContext(),name,city);
                showToast(toastMessage.toString());
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if(preferencePlayer != null)
        {
            editTextName.setText(preferencePlayer.retrievePlayer(this,"NAME"));
            editTextCity.setText(preferencePlayer.retrievePlayer(this,"CITY"));
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        preferencePlayer.savePlayer(getApplicationContext(),editTextName.getText().toString(),editTextCity.getText().toString());
    }

    Toast toast;
    private void showToast(String text)
    {
        if(toast != null)
            toast.cancel();
        toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG);
        toast.show();
    }

}
