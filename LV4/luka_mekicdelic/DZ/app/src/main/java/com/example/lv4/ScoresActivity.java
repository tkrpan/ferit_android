package com.example.lv4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

public class ScoresActivity extends AppCompatActivity
{
    private String LOG_TAG = "LV4 ScoresActivity";

    private RecyclerView recyclerViewScores;
    private RecyclerView.LayoutManager layoutManagerScores;
    private ArrayList<Score> scores;
    private ScoreAdapter scoreAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        recyclerViewScores = findViewById(R.id.recyclerViewScores);
        layoutManagerScores = new LinearLayoutManager(this);

        scores = ScoreDBHelper.getInstance(getApplicationContext()).getAllScores();
        scoreAdapter = new ScoreAdapter(scores, new ScoreAdapter.OnItemClickListener()
        {
            @Override
            public void onItemSelected(int position)
            {
                Log.d(LOG_TAG,"onItemSelected(), removing entry...");
                scores.remove(position);
                recyclerViewScores.getAdapter().notifyDataSetChanged();
                Log.d(LOG_TAG, "Entry removed.");

                /* WORK IN PROGRESS
                Log.d(LOG_TAG,"onItemSelected(), removing entry from DB...");
                if (ScoreDBHelper.getInstance(getApplicationContext()).removeScore(position)>0)
                {
                    Log.d(LOG_TAG, "Entry removed from DB.");
                    scores.remove(position);
                    recyclerViewScores.getAdapter().notifyDataSetChanged();
                }
                else
                {
                    Log.d(LOG_TAG, "Entry NOT removed from DB!!");
                }
                */
            }
        });

        recyclerViewScores.setLayoutManager(layoutManagerScores);
        recyclerViewScores.setAdapter(scoreAdapter);
    }
}
