package com.example.lv4;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ViewHolder>
{
    private String LOG_TAG = "LV4 ScoreAdapter";

    private ArrayList<Score> scores;
    private OnItemClickListener listener;

    public ScoreAdapter(ArrayList<Score> scores, OnItemClickListener listener)
    {
        this.scores = scores;
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public LinearLayout linearLayout;
        public TextView textViewName, textViewCity, textViewNumber, textViewTries;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);

            linearLayout = itemView.findViewById(R.id.linearLayout);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewCity = itemView.findViewById(R.id.textViewCity);
            textViewNumber = itemView.findViewById(R.id.textViewNumber);
            textViewTries = itemView.findViewById(R.id.textViewTries);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.score_adapter, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position)
    {
        Log.d(LOG_TAG,"onBindViewHolder()");

        Score score = scores.get(position);

        /*
        Log.d(LOG_TAG,score.name);
        Log.d(LOG_TAG,score.city);
        Log.d(LOG_TAG,"" + score.number);
        Log.d(LOG_TAG,"" + score.tries);
        */

        holder.textViewName.setText(score.name);
        holder.textViewCity.setText(score.city);
        holder.textViewNumber.setText(String.format("%s", score.number));
        holder.textViewTries.setText(String.format("%d", score.tries));

        holder.linearLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d(LOG_TAG,"onClick(), listener.onItemSelected()");
                listener.onItemSelected(position);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return scores.size();
    }

    public interface OnItemClickListener
    {
        void onItemSelected(int position);
    }
}
