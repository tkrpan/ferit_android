package com.example.lv4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class ScoreDBHelper extends SQLiteOpenHelper
{
    public static class Schema
    {
        private static final int SCHEMA_VERSION = 1;
        private static final String DATABASE_NAME = "scores.db";
        //A table to store scores:
        static final String TABLE_SCORES = "scores";
        static final String NAME = "name";
        static final String CITY = "city";
        static final String NUMBER = "number";
        static final String TRIES = "tries";
    }
    //SQL statements
    private static final String CREATE_TABLE_SCORES = "CREATE TABLE " + Schema.TABLE_SCORES
            + " (" + Schema.NAME + " TEXT," + Schema.CITY + " TEXT," + Schema.NUMBER + " INTEGER," + Schema.TRIES + " INTEGER);";
    private static final String DROP_TABLE_SCORES = "DROP TABLE IF EXISTS " + Schema.TABLE_SCORES;
    private static final String SELECT_ALL_SCORES = "SELECT " + Schema.NAME + "," + Schema.CITY
            + "," + Schema.NUMBER + "," + Schema.TRIES + " FROM " + Schema.TABLE_SCORES;


    private static ScoreDBHelper scoreDBHelper = null;

    private ScoreDBHelper(Context context)
    {
        super(context.getApplicationContext(),Schema.DATABASE_NAME,null,Schema.SCHEMA_VERSION);
    }

    public static synchronized ScoreDBHelper getInstance(Context context)
    {
        if(scoreDBHelper == null)
        {
            scoreDBHelper = new ScoreDBHelper(context);
        }
        return scoreDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CREATE_TABLE_SCORES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL(DROP_TABLE_SCORES);
        this.onCreate(db);
    }

    // CRUD should be performed on another thread
    public void insertScore(String name, String city, byte number, int tries)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.NAME, name);
        contentValues.put(Schema.CITY, city);
        contentValues.put(Schema.NUMBER, number);
        contentValues.put(Schema.TRIES, tries);
        SQLiteDatabase writeableDatabase = this.getWritableDatabase();
        writeableDatabase.insert(Schema.TABLE_SCORES, Schema.NAME,contentValues);
        writeableDatabase.close();
    }
    public void insertScore(Score score)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.NAME, score.name);
        contentValues.put(Schema.CITY, score.city);
        contentValues.put(Schema.NUMBER, score.number);
        contentValues.put(Schema.TRIES, score.tries);
        SQLiteDatabase writeableDatabase = this.getWritableDatabase();
        writeableDatabase.insert(Schema.TABLE_SCORES, Schema.NAME, contentValues);
        writeableDatabase.close();
    }

    /* WORK IN PROGRESS
    public int removeScore(int pos)
    {
        SQLiteDatabase writeableDatabase = this.getWritableDatabase();
        return writeableDatabase.delete(Schema.TABLE_SCORES,"id=?",new String[] {Integer.toString(pos)});
    }
    */

    public ArrayList<Score> getAllScores()
    {
        SQLiteDatabase writeableDatabase = this.getWritableDatabase();
        Cursor scoreCursor = writeableDatabase.rawQuery(SELECT_ALL_SCORES,null);
        ArrayList<Score> scores = new ArrayList<>();
        if(scoreCursor.moveToFirst())
        {
            do {
                String name = scoreCursor.getString(0);
                String city = scoreCursor.getString(1);
                byte number = (byte) scoreCursor.getInt(2);
                int tries = scoreCursor.getInt(3);
                scores.add(new Score(name, city, number, tries));
            }while(scoreCursor.moveToNext());
        }
        scoreCursor.close();
        writeableDatabase.close();
        return scores;
    }

}
