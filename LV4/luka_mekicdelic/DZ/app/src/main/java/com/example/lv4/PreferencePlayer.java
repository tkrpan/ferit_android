package com.example.lv4;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencePlayer
{
    public static String FILE = "FILE";
    
    public static String NAME = "NAME";
    public static String CITY = "CITY";

    public void savePlayer(Context context, String name, String city)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(NAME, name);
        editor.putString(CITY, city);
        editor.apply();
    }

    public String retrievePlayer(Context context, String key)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(FILE, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key,"");
    }

}